import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProfileItemView extends StatelessWidget {
  final String fieldValue;
  final String fieldText;
  const ProfileItemView({Key key, this.fieldValue, this.fieldText}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: EdgeInsets.only(top: 5),
        width: MediaQuery.of(context).size.width,
        height: 56,
        decoration: new BoxDecoration(color: Color(0xffffffff)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: new Text(fieldText,
                  style: TextStyle(
                    fontFamily: 'GESSTwo',
                    color: Color(0xffdb0916),
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                  )),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: new Text(fieldValue,
                  style: TextStyle(
                    fontFamily: 'GESSTwo',
                    color: Color(0xff4f4f4f),
                    fontSize: 16,
                    fontWeight: FontWeight.w300,
                    fontStyle: FontStyle.normal,
                  )),
            ),
          ],
        ));
  }
}
