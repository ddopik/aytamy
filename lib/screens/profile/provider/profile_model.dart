

import 'dart:io';
import 'package:image_picker/image_picker.dart';
class ProfileModel {
  final picker = ImagePicker();
  var _profileImagePath;
  Future getImage({onSucess,onError}) async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    _profileImagePath = File(pickedFile.path);
    onSucess(_profileImagePath);
  }
}
