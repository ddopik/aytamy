import 'Dream.dart';

class DreamsResponse {
  bool _success;
  List<Dream> _data;
  String _message;

  bool get success => _success;

  List<Dream> get data => _data;

  String get message => _message;

  DreamsResponse({bool success, List<Dream> data, String message}) {
    _success = success;
    _data = data;
    _message = message;
  }

  DreamsResponse.fromJson(dynamic json) {
    _success = json["success"];
    if (json["data"] != null) {
      _data = [];
      json["data"].forEach((v) {
        _data.add(Dream.fromJson(v));
      });
    }
    _message = json["message"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["success"] = _success;
    if (_data != null) {
      map["data"] = _data.map((v) => v.toJson()).toList();
    }
    map["message"] = _message;
    return map;
  }
}
