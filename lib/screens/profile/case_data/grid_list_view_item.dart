import 'package:aytamy/common/fonts.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class GridListViewItem extends StatelessWidget {
 final String fieldName;
 final String fieldValue;
 final String imagePath;

  const GridListViewItem({Key key, this.fieldName, this.fieldValue, this.imagePath}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left:30.0,right: 40.0),
      child: Column(
        children: [
          Row(children: [
            new Image.asset(
              imagePath,
              color: Colors.red,
              width: 20,
              height: 23,
            ),
            SizedBox(width: 5,),
            Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
              Text(fieldName,
                  textScaleFactor: 1.0,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'GESSTwo',
                    color: Color(0xffdb0011),
                    fontSize: Fonts.fontSize_small,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                  )),

              new Text(fieldValue,
                  textScaleFactor: 1.0,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: 'GESSTwo',
                    color: Color(0xff000000),
                    fontSize: Fonts.fontSize_small,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                  ))
            ])
          ]),

        ],
      ),
    );
  }
}
