import 'package:aytamy/common/fonts.dart';
import 'package:aytamy/common/model/User.dart';
import 'package:aytamy/generated/l10n.dart';
import 'package:aytamy/screens/profile/case_data/grid_list_view_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class DraggableListView extends StatefulWidget {
  final User user;

  const DraggableListView({
    Key key,
    this.user,
  }) : super(key: key);

  @override
  _DraggableListViewState createState() => _DraggableListViewState(user);
}

class _DraggableListViewState extends State<DraggableListView> {
  final User user;

  _DraggableListViewState(this.user);

  @override
  Widget build(BuildContext context) {
    return DraggableScrollableActuator(
      child: Stack(
        children: <Widget>[
          DraggableScrollableSheet(
            initialChildSize: .5,
            minChildSize: 0.50,
            maxChildSize: 1.0,
            builder: (BuildContext context, ScrollController scrollController) {
              return Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(16.0),
                    topRight: Radius.circular(16.0),
                  ),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey,
                        offset: Offset(1.0, -2.0),
                        blurRadius: 4.0,
                        spreadRadius: 2.0)
                  ],
                ),
                child: ListView.builder(
                  controller: scrollController,
                  itemCount: 1,
                  itemBuilder: (BuildContext context, int index) {
                    return getViewOne(context, user);
                  },
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}

getViewOne(BuildContext context, User user) {
  return Container(
    alignment: Alignment.topCenter,
    child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        // Line
        Container(
            width: 30,
            height: 8,
            margin: EdgeInsets.all(8),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(100)),
                color: const Color(0xff4f4f4f))),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 15.0, bottom: 15.0),
              child: new Text(S.current.orphenData,
                  textScaleFactor: 1.0,
                  style: TextStyle(
                    fontFamily: Fonts.fontfamily,
                    color: Color(0xffdb0011),
                    fontSize: Fonts.itemFontSize,
                    fontWeight: FontWeight.w700,
                    fontStyle: FontStyle.normal,
                  )),
            ),
            GridView.count(
                primary: false,
                crossAxisCount: 2,
                childAspectRatio: 5,
                mainAxisSpacing: 1.0,
                crossAxisSpacing: 1.0,
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                // You won't see infinite size error
                children: <Widget>[
                  GridListViewItem(
                    fieldName: S.current.userName,
                    fieldValue: user.name,
                    imagePath: "assets/icons/user.png",
                  ),
                  GridListViewItem(
                    fieldName: S.current.birthDate,
                    fieldValue: user.dateBirth != null
                        ? DateTime.parse(user.dateBirth).day.toString() +
                            "-" +
                            DateTime.parse(user.dateBirth).month.toString() +
                            "-" +
                            DateTime.parse(user.dateBirth).year.toString()
                        : "9-3-1997",
                    imagePath: "assets/icons/cake.png",
                  ),
                ]),
            Divider(
              thickness: 2,
              indent: MediaQuery.of(context).size.width * 0.10,
              color: Color(0x1f000000),
              endIndent: MediaQuery.of(context).size.width * 0.18,
            ),
            GridView.count(
                primary: false,
                crossAxisCount: 2,
                childAspectRatio: 5,
                mainAxisSpacing: 1.0,
                crossAxisSpacing: 1.0,
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                // You won't see infinite size error
                children: <Widget>[
                  GridListViewItem(
                    fieldName: S.current.eduSys,
                    fieldValue: "ابتدائي",
                    imagePath: "assets/icons/star.png",
                  ),
                  GridListViewItem(
                    fieldName: S.current.dream,
                    fieldValue: "الحلم",
                    imagePath: "assets/icons/cup.png",
                  ),
                ]),
            Divider(
              thickness: 2,
              indent: MediaQuery.of(context).size.width * 0.10,
              color: Color(0x1f000000),
              endIndent: MediaQuery.of(context).size.width * 0.18,
            ),
            GridView.count(
                primary: false,
                crossAxisCount: 2,
                childAspectRatio: 5,
                mainAxisSpacing: 1.0,
                crossAxisSpacing: 1.0,
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                // You won't see infinite size error
                children: <Widget>[
                  GridListViewItem(
                    fieldName: S.current.city,
                    fieldValue: user.name,
                    imagePath: "assets/icons/location.png",
                  ),
                  GridListViewItem(
                    fieldName: S.current.spareTime,
                    fieldValue: user.name,
                    imagePath: "assets/icons/hom.png",
                  ),
                ]),
            Divider(
              thickness: 2,
              indent: MediaQuery.of(context).size.width * 0.10,
              color: Color(0x1f000000),
              endIndent: MediaQuery.of(context).size.width * 0.18,
            ),
            GridView.count(
                primary: false,
                crossAxisCount: 2,
                childAspectRatio: 5,
                mainAxisSpacing: 1.0,
                crossAxisSpacing: 1.0,
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                // You won't see infinite size error
                children: <Widget>[
                  GridListViewItem(
                    fieldName: S.current.phoneNumber,
                    fieldValue: user.name,
                    imagePath: "assets/icons/phone.png",
                  ),
                  GridListViewItem(
                    fieldName: S.current.personalProof,
                    fieldValue: user.name,
                    imagePath: "assets/icons/pdf.png",
                  ),
                ]),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: new Container(
                width: MediaQuery.of(context).size.width * 0.80,
                height: 165,
                decoration: new BoxDecoration(
                  border:
                      Border.all(color: const Color(0xff4f4f4f), width: 1),
                  color: Color(0xffffffff),
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                        color: Color(0x29000000),
                        offset: Offset(1, 3),
                        blurRadius: 6,
                        spreadRadius: 0)
                  ],
                ),
                child: TextFormField(
                  cursorColor: Colors.black,
                  decoration: new InputDecoration(
                      border: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      contentPadding: EdgeInsets.only(
                          left: 15, bottom: 11, top: 11, right: 15),
                      hintText: "الوصف",
                      hintStyle: TextStyle(color: Colors.black)),
                  style:
                      TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
                ),
              ),
            )
          ],
        )
      ],
    ),
  );
}
