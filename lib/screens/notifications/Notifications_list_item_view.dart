import 'package:aytamy/common/model/User.dart';
import 'package:aytamy/generated/l10n.dart';
import 'package:aytamy/screens/likes/delete_dialog_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class NotificationsListItemView extends StatelessWidget {
  final String body;
  final String title;
  final int notificationId;
  final Function DeleteNotificationCallBack;
  const NotificationsListItemView({Key key, this.body, this.DeleteNotificationCallBack, this.notificationId, this.title}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return
      new Container(
          width: MediaQuery.of(context).size.width*0.80,
          height: 80,
          decoration: new BoxDecoration(
              color: Color(0xffe4e4e4)
          ),
        child:
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                "assets/logo.png",
                height: 35,
              ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
              new Text(title,
                  style: TextStyle(
                    fontFamily: 'GESSTwo',
                    color: Color(0xff000000),
                    fontSize: 16,
                    fontWeight: FontWeight.w700,
                    fontStyle: FontStyle.normal,


                  )
              ),
              Flexible(
                child: new Text(body.toString(),
                    style: TextStyle(
                      fontFamily: 'GESSTwo',
                      color: Color(0xff4f4f4f),
                      fontSize: 12,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0.12,

                    )
                ),
              )
            ],),
            InkWell(
            onTap: (){
              deleteDialogView(
                  context: context,
                  DeleteCallBack: DeleteNotificationCallBack,
                  id: notificationId);
            }
            ,

            child: Icon(Icons.offline_bolt_rounded))
          ],)
      );
  }
}
