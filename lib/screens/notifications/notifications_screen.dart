import 'package:aytamy/common/stats_widgets.dart';
import 'package:aytamy/generated/l10n.dart';
import 'package:aytamy/screens/notifications/Notifications_list_item_view.dart';
import 'package:aytamy/screens/notifications/model/notification.dart';
import 'package:aytamy/screens/notifications/provider/notification_model.dart';
import 'package:flutter/material.dart';

class NotificationsScreen extends StatefulWidget {
  @override
  _NotificationsScreenState createState() => _NotificationsScreenState();
}

class _NotificationsScreenState extends State<NotificationsScreen> {
  NotificationModel _notificationModel = NotificationModel();
  List<notification> notifications;
  @override
  void initState() {
    showLoading(context);
    _notificationModel.getUserNotifications(onSuccess: (response) {
      dismissLoading();
      setState(() {
        notifications = response;
      });
    }, onError: (error) {
      dismissLoading();
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return
      notifications != null
          ?
      Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: new Text(S.current.mostRecentNotification,
              style: TextStyle(
                fontFamily: 'GESSTwo',
                color: Color(0xff000000),
                fontSize: 16,
                fontWeight: FontWeight.w700,
                fontStyle: FontStyle.normal,
              )),
        ),
        ListView.builder(
          shrinkWrap: true,
          itemCount: notifications.length,
          itemBuilder: (BuildContext context, int index) {
            return NotificationsListItemView(body: notifications[index].body,notificationId: notifications[index].id,DeleteNotificationCallBack: (){
              showLoading(context);
              _notificationModel.deleteNotification(
                  notificationId: notifications[index].id,
                  onSuccess: () {
                    dismissLoading();
                    setState(() {
                      notifications.removeAt(index);
                    });
                    showSuccesses(context, S.current.deleteItemSuccess);
                    Navigator.of(context).pop();
                  },
                  onError: (error) {
                    dismissLoading();
                  });
            },title:notifications[index].title ,);
          },
        )
      ],
    ):Text("");
  }
}
