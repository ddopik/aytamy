import 'package:aytamy/common/model/User.dart';
import 'package:aytamy/network/dio_manager.dart';
import 'package:aytamy/screens/home/model/response.dart';
import 'package:aytamy/screens/likes/model/orphen.dart';
import 'package:aytamy/screens/likes/model/likes_response.dart';
import 'package:aytamy/screens/notifications/model/notification.dart';
import 'package:aytamy/screens/notifications/model/notificationResponse.dart';

class NotificationModel {
  List<notification> notifications;

  void getUserNotifications({onSuccess, onError}) {
    notifications = [notification(), notification(), notification(), notification()];
    return DIOManager().getNotifications(onSuccess: (response) {
      print("getNotifications onSuccess ---->" + response.toString());
      NotificationResponse notificationResponse = NotificationResponse.fromJson(response);
      notifications = notificationResponse.data;
      onSuccess(notifications);
    }, onError: (error) {
      onError(error.toString());
      print("getNotifications onError ---->" + error.toString());
    });
  }

  deleteNotification({int notificationId,onSuccess, onError}) {
    DIOManager().deleteNotification(notificationId:notificationId,onSuccess: (response) {
      print("deleteNotification onSuccess ---->" + response.toString());
      onSuccess();
    }, onError: (error) {
      onError(error.toString());
      print("deleteNotification onError ---->" + error.toString());
    });
  }
}
