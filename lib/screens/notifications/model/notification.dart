import 'package:aytamy/common/model/User.dart';

class notification {
  int _id;
  String _title;
  String _body;
  dynamic _customer;

  int get id => _id;

  String get title => _title;

  dynamic get customer => _customer;

  String get body => _body;

  notification({int id, String title, String body, User customer}) {
    _id = id;
    _title = title;
    _body = body;
    _customer = customer;
  }

  notification.fromJson(dynamic json) {
    _id = json["id"];
    _title = json["title"];
    _body = json["body"];
    if (json["customer"] != null) {
      _customer = User.fromJson(json["customer"]);
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["title"] = _title;
    map["body"] = _body;
    if (_customer != null) {
      map["customer"] = _customer.toJson();
    }
    return map;
  }
}
