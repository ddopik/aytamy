import 'package:aytamy/common/dimen.dart';
import 'package:aytamy/generated/l10n.dart';
import 'package:flutter/material.dart';

class PrivacyAndPolicyScreen extends StatefulWidget {
  @override
  _PrivacyAndPolicyScreenState createState() => _PrivacyAndPolicyScreenState();
}

class _PrivacyAndPolicyScreenState extends State<PrivacyAndPolicyScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: Container(),
          centerTitle: true,
          title: Text(
            S.current.privacyAndPoliceTitle,
            style: TextStyle(
              fontFamily: 'GESSTwo',
              color: Colors.white,
              fontSize: 24.0,
              fontWeight: FontWeight.bold,
              fontStyle: FontStyle.normal,
            ),
          ),
          actions: [
            IconButton(
                icon: Icon(Icons.arrow_forward_ios_outlined),
                onPressed: () {
                  Navigator.of(context).pop();
                })
          ],
        ),
        body: SingleChildScrollView(
            child: Container(
          padding: EdgeInsets.all(12),
          child: Column(
            children: [
              Text(
                S.current.privacyAndPoliceSubTitle,
                style: TextStyle(
                  fontFamily: 'GESSTwo',
                  color: Color(0xff4f4f4f),
                  fontSize: 22,
                  fontWeight: FontWeight.w700,
                  fontStyle: FontStyle.normal,
                  letterSpacing: -0.19285714530944825,
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: form_field_space_min,),
              Text(
                S.current.privacyAndPoliceSubContent,
                style: TextStyle(
                  fontFamily: 'GESSTwo',
                  color: Color(0xff4f4f4f),
                  fontSize: 18,
                  fontWeight: FontWeight.w300,
                  fontStyle: FontStyle.normal,
                  letterSpacing: -0.19285714530944825,
                ),
                textAlign: TextAlign.center,
              ),
              // Rectangle 49
            ],
          ),
        )));
  }
}
