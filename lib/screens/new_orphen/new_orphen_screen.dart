import 'dart:io';

import 'package:aytamy/common/exception_indicators/error_indicator.dart';
import 'package:aytamy/common/fonts.dart';
import 'package:aytamy/common/model/user_type.dart';
import 'package:aytamy/common/stats_widgets.dart';
import 'package:aytamy/generated/l10n.dart';
import 'package:aytamy/screens/profile/model/Dream.dart';
import 'package:aytamy/screens/profile/model/EduSystem.dart';
import 'package:aytamy/screens/profile/provider/profile_model.dart';
import 'package:aytamy/screens/signup/model/city.dart';
import 'package:aytamy/screens/signup/model/job.dart';
import 'package:aytamy/screens/signup/model/nationality.dart';
import 'package:aytamy/screens/signup/provider/registrationModel.dart';
import 'package:aytamy/storage/pref_manager.dart';
import 'package:date_format/date_format.dart' as Format;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';

class NewOrphenScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _NewOrphenScreen();
  }
}

class _NewOrphenScreen extends State<NewOrphenScreen> {
  DateTime selectedDate;
  RegistrationModel _registrationModel = RegistrationModel();
  DateFormat dateFormat = DateFormat("MM/dd/yyyy");
  List<Job> dropdownJobList = [];
  List<City> dropdownCityList = [];
  List<EduSystem> dropdownEduSystemsList = [];
  List<Dream> dropdownDreamsList = [];
  List<Nationality> dropdownNationalityList = [];
  City _selectedDropdownCityValue;
  EduSystem _selectedDropdownEduSystemValue;
  Dream _selectedDropdownDreamValue;
  Nationality _selectedDropdownNationalityValue;
  File _profileImagePath;
  File _provenessImagePath;
  File _parentPaperImagePath;
  File _motherPaperImagePath;
  File _LearnPaperImagePath;
  final picker = ImagePicker();
  ProfileModel _profileModel = ProfileModel();
  String personalId = "";
  String parentCertificate = "";
  String motherCertificate = "";
  String educationalCertificate = "";
  final GlobalKey<FormState> _profileFormState = GlobalKey<FormState>();
  final TextEditingController _userNameController = TextEditingController();
  final TextEditingController _userTimeController = TextEditingController();
  final TextEditingController _userDescriptionController =
      TextEditingController();
  final TextEditingController _userParentMobileController =
      TextEditingController();
  final TextEditingController _userMailController = TextEditingController();
  final TextEditingController _userPasswordController = TextEditingController();

  @override
  void initState() {
    showLoading(context);
    RegistrationModel().getCities(onSuccess: (response) {
      setState(() {
        dropdownCityList.addAll(response);
      });
      dismissLoading();
    }, onError: (error) {
      dismissLoading();
    });
    showLoading(context);
    RegistrationModel().getEduSystems(onSuccess: (response) {
      setState(() {
        dropdownEduSystemsList.addAll(response);
      });
      dismissLoading();
    }, onError: (error) {
      dismissLoading();
    });
    showLoading(context);
    RegistrationModel().getDreams(onSuccess: (response) {
      setState(() {
        dropdownDreamsList.addAll(response);
      });
      dismissLoading();
    }, onError: (error) {
      dismissLoading();
    });
    showLoading(context);
    RegistrationModel().getNationalities(onSuccess: (response) {
      setState(() {
        dropdownNationalityList.addAll(response);
      });
      dismissLoading();
    }, onError: (error) {
      dismissLoading();
    });
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
        body: dropdownEduSystemsList.isNotEmpty &&
                dropdownDreamsList.isNotEmpty &&
                dropdownCityList.isNotEmpty
            ? getMainView()
            : Container(
                alignment: Alignment.center,
                child: Container(
                  height: 500,
                  child: ErrorIndicator(
                    error: S.current.unExpectedError,
                  ),
                ),
              ));
  }

  Widget getMainView() {
    return ListView(
      shrinkWrap: true,
      children: [
        new Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: new BoxDecoration(
              color: Color(0xfff5f5f5),
              borderRadius: BorderRadius.circular(10)),
          child: Form(
              key: _profileFormState,
              child: ListView(
                padding: EdgeInsets.zero,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        InkWell(
                          onTap: () {
                            String BirthDate;
                            if (selectedDate == null) {
                              BirthDate = "1997-5-15";
                            } else {
                              BirthDate = Format.formatDate(selectedDate, [
                                Format.yyyy,
                                '-',
                                Format.mm,
                                '-',
                                Format.dd
                              ]);
                            }
                            showLoading(context);
                            _registrationModel.updateUserProfileInfo(
                                profileImagePath: _profileImagePath,
                                email: _userMailController.text.toString(),
                                birthDate: BirthDate.toString(),
                                cityId: _selectedDropdownCityValue?.id,
                                countryId: _selectedDropdownCityValue?.id,
                                description: _userDescriptionController.text,
                                dreamId: _selectedDropdownDreamValue?.id,
                                eduSystemId:
                                    _selectedDropdownEduSystemValue?.id,
                                parentMobile: _userParentMobileController.text,
                                password:
                                    _userPasswordController.text.toString(),
                                spareTime: _userTimeController.text.toString(),
                                userName: _userNameController.text.toString(),
                                personalIdImagePath: _provenessImagePath,
                                eduCertificateImagePath: _LearnPaperImagePath,
                                motherCertificateImagePath:
                                    _motherPaperImagePath,
                                parentCertificateImagePath:
                                    _parentPaperImagePath,
                                onSuccess: (user) {
                                  dismissLoading();
                                  print("done---->");
                                },
                                onError: (error) {
                                  dismissLoading();
                                  print("errror" + error.toString());
                                  showError(error.toString());
                                });
                          },
                          child: new Text("حفظ",
                              textScaleFactor: 1.0,
                              style: TextStyle(
                                fontFamily: 'GESSTwo',
                                color: Color(0xffdb0011),
                                fontSize: Fonts.fontSize_small,
                                fontWeight: FontWeight.w500,
                                fontStyle: FontStyle.normal,
                              )),
                        ),
                        new Text("يتيم جديد",
                            textScaleFactor: 1.0,
                            style: TextStyle(
                              fontFamily: 'GESSTwo',
                              color: Color(0xffdb0011),
                              fontSize: Fonts.fontSize_small,
                              fontWeight: FontWeight.w700,
                              fontStyle: FontStyle.normal,
                              letterSpacing: -0.19285714530944825,
                            )),
                        InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: new Text("الغاء",
                              textScaleFactor: 1.0,
                              style: TextStyle(
                                fontFamily: 'GESSTwo',
                                color: Color(0xff9e9e9e),
                                fontSize: Fonts.fontSize_small,
                                fontWeight: FontWeight.w500,
                                fontStyle: FontStyle.normal,
                              )),
                        ),
                      ],
                    ),
                  ),
                  Stack(
                    children: [
                      (_profileImagePath?.path != null)
                          ? Image.file(
                              _profileImagePath,
                              height: MediaQuery.of(context).size.height * .35,
                              width: MediaQuery.of(context).size.width,
                              fit: BoxFit.fill,
                            )
                          : Container(
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height * .35,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  image: new DecorationImage(
                                    image: AssetImage(
                                        "assets/images/shutterstock_107332775.png"),
                                    fit: BoxFit.cover,
                                  ),
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(10),
                                      topRight: Radius.circular(10))),
                            ),
                      Align(
                          alignment: Alignment.topLeft,
                          child: Column(
                            children: [
                              IconButton(
                                icon: Icon(
                                  Icons.camera_alt,
                                  color: Colors.red,
                                ),
                                onPressed: () {
                                  _profileModel.getImage(onSucess: (path) {
                                    setState(() {
                                      _profileImagePath = path;
                                    });
                                  });
                                },
                              ),
                              new Text("ألتقط صورة",
                                  textScaleFactor: 1.0,
                                  style: TextStyle(
                                    fontFamily: 'GESSTwo',
                                    color: Color(0xffdb0916),
                                    fontSize: Fonts.fontSize_small,
                                    fontWeight: FontWeight.w500,
                                    fontStyle: FontStyle.normal,
                                  ))
                            ],
                          )),
                    ],
                  ),
                  new Container(
                      margin: EdgeInsets.only(top: 5),
                      width: MediaQuery.of(context).size.width,
                      height: 56,
                      decoration: new BoxDecoration(color: Color(0xffffffff)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Flexible(
                              child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: new Text("الاسم",
                                      textScaleFactor: 1.0,
                                      style: TextStyle(
                                        fontFamily: 'GESSTwo',
                                        color: Color(0xffdb0916),
                                        fontSize: Fonts.fontSize_small,
                                        fontWeight: FontWeight.w500,
                                        fontStyle: FontStyle.normal,
                                      )))),
                          Flexible(
                            child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: new TextFormField(
                                  controller: _userNameController,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontFamily: 'GESSTwo',
                                    color: Color(0xff4f4f4f),
                                    fontSize: Fonts.fontSize_small,
                                    fontWeight: FontWeight.w300,
                                    fontStyle: FontStyle.normal,
                                  ),
                                  decoration: InputDecoration(
                                    disabledBorder: InputBorder.none,
                                    border: InputBorder.none,
                                  ),
                                )),
                          ),
                        ],
                      )),
                  getDropDownCountriesView(),
                  getDropDownCitiesView(),
                  getBirthDateView(),
                  getDropDownEduSystemsView(),
                  getDropDownDreamsView(),
                  new Container(
                      margin: EdgeInsets.only(top: 5),
                      width: MediaQuery.of(context).size.width,
                      height: 56,
                      decoration: new BoxDecoration(color: Color(0xffffffff)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: new Text("وقت الفراغ",
                                  textScaleFactor: 1.0,
                                  style: TextStyle(
                                    fontFamily: 'GESSTwo',
                                    color: Color(0xffdb0916),
                                    fontSize: Fonts.fontSize_small,
                                    fontWeight: FontWeight.w500,
                                    fontStyle: FontStyle.normal,
                                  ))),
                          Flexible(
                            child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: new TextFormField(
                                  controller: _userTimeController,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontFamily: 'GESSTwo',
                                    color: Color(0xff4f4f4f),
                                    fontSize: Fonts.fontSize_small,
                                    fontWeight: FontWeight.w300,
                                    fontStyle: FontStyle.normal,
                                  ),
                                  decoration: InputDecoration(
                                    disabledBorder: InputBorder.none,
                                    border: InputBorder.none,
                                  ),
                                )),
                          ),
                        ],
                      )),
                  int.parse(PrefManager().getUserType() ?? "6") ==
                          UserType.Orphen
                      ? new Container(
                          margin: EdgeInsets.only(top: 5),
                          width: MediaQuery.of(context).size.width,
                          height: 56,
                          decoration:
                              new BoxDecoration(color: Color(0xffffffff)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: new Text("اثبات الشخصية",
                                      textScaleFactor: 1.0,
                                      style: TextStyle(
                                        fontFamily: 'GESSTwo',
                                        color: Color(0xffdb0916),
                                        fontSize: Fonts.fontSize_small,
                                        fontWeight: FontWeight.w500,
                                        fontStyle: FontStyle.normal,
                                      ))),
                              Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: InkWell(
                                    onTap: () {
                                      _profileModel.getImage(onSucess: (path) {
                                        setState(() {
                                          _provenessImagePath = path;
                                        });
                                      });
                                    },
                                    child: Row(children: [
                                      new Text(
                                          (_provenessImagePath?.path != null)
                                              ? _provenessImagePath.path
                                                  .split("image_picker")
                                                  .last
                                              : personalId.split('picker').last,
                                          textScaleFactor: 1.0,
                                          style: TextStyle(
                                            fontFamily: 'GESSTwo',
                                            color: Color(0xff4f4f4f),
                                            fontSize: Fonts.fontSize_small,
                                            fontWeight: FontWeight.w300,
                                            fontStyle: FontStyle.normal,
                                          )),
                                      Icon(
                                        Icons.camera_alt,
                                        color: Color(0xff4f4f4f),
                                      ),
                                    ]),
                                  )),
                            ],
                          ))
                      : SizedBox(),
                  int.parse(PrefManager().getUserType() ?? "6") ==
                          UserType.Orphen
                      ? new Container(
                          margin: EdgeInsets.only(top: 5),
                          width: MediaQuery.of(context).size.width,
                          height: 56,
                          decoration:
                              new BoxDecoration(color: Color(0xffffffff)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: new Text(S.current.fatherCertificate,
                                      textScaleFactor: 1.0,
                                      style: TextStyle(
                                        fontFamily: 'GESSTwo',
                                        color: Color(0xffdb0916),
                                        fontSize: Fonts.fontSize_small,
                                        fontWeight: FontWeight.w500,
                                        fontStyle: FontStyle.normal,
                                      ))),
                              Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: InkWell(
                                    onTap: () {
                                      _profileModel.getImage(onSucess: (path) {
                                        setState(() {
                                          _parentPaperImagePath = path;
                                        });
                                      });
                                    },
                                    child: Row(children: [
                                      new Text(
                                          (_parentPaperImagePath?.path != null)
                                              ? _parentPaperImagePath.path
                                                  .split("image_picker")
                                                  .last
                                              : parentCertificate
                                                  .split('picker')
                                                  .last,
                                          textScaleFactor: 1.0,
                                          style: TextStyle(
                                            fontFamily: 'GESSTwo',
                                            color: Color(0xff4f4f4f),
                                            fontSize: Fonts.fontSize_small,
                                            fontWeight: FontWeight.w300,
                                            fontStyle: FontStyle.normal,
                                          )),
                                      Icon(
                                        Icons.camera_alt,
                                        color: Color(0xff4f4f4f),
                                      ),
                                    ]),
                                  )),
                            ],
                          ))
                      : SizedBox(),
                  int.parse(PrefManager().getUserType() ?? "6") ==
                          UserType.Orphen
                      ? new Container(
                          margin: EdgeInsets.only(top: 5),
                          width: MediaQuery.of(context).size.width,
                          height: 56,
                          decoration:
                              new BoxDecoration(color: Color(0xffffffff)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: new Text(S.current.motherCertificate,
                                      textScaleFactor: 1.0,
                                      style: TextStyle(
                                        fontFamily: 'GESSTwo',
                                        color: Color(0xffdb0916),
                                        fontSize: Fonts.fontSize_small,
                                        fontWeight: FontWeight.w500,
                                        fontStyle: FontStyle.normal,
                                      ))),
                              Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: InkWell(
                                    onTap: () {
                                      _profileModel.getImage(onSucess: (path) {
                                        setState(() {
                                          _motherPaperImagePath = path;
                                        });
                                      });
                                    },
                                    child: Row(children: [
                                      new Text(
                                          (_motherPaperImagePath?.path != null)
                                              ? _motherPaperImagePath.path
                                                  .split("image_picker")
                                                  .last
                                              : motherCertificate
                                                  .split('picker')
                                                  .last,
                                          textScaleFactor: 1.0,
                                          style: TextStyle(
                                            fontFamily: 'GESSTwo',
                                            color: Color(0xff4f4f4f),
                                            fontSize: Fonts.fontSize_small,
                                            fontWeight: FontWeight.w300,
                                            fontStyle: FontStyle.normal,
                                          )),
                                      Icon(
                                        Icons.camera_alt,
                                        color: Color(0xff4f4f4f),
                                      ),
                                    ]),
                                  )),
                            ],
                          ))
                      : SizedBox(),
                  int.parse(PrefManager().getUserType() ?? "6") ==
                          UserType.Orphen
                      ? new Container(
                          margin: EdgeInsets.only(top: 5),
                          width: MediaQuery.of(context).size.width,
                          height: 56,
                          decoration:
                              new BoxDecoration(color: Color(0xffffffff)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: new Text(S.current.eduCertificate,
                                      textScaleFactor: 1.0,
                                      style: TextStyle(
                                        fontFamily: 'GESSTwo',
                                        color: Color(0xffdb0916),
                                        fontSize: Fonts.fontSize_small,
                                        fontWeight: FontWeight.w500,
                                        fontStyle: FontStyle.normal,
                                      ))),
                              Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: InkWell(
                                    onTap: () {
                                      _profileModel.getImage(onSucess: (path) {
                                        setState(() {
                                          _LearnPaperImagePath = path;
                                        });
                                      });
                                    },
                                    child: Row(children: [
                                      new Text(
                                          (_LearnPaperImagePath?.path != null)
                                              ? _LearnPaperImagePath.path
                                                  .split("image_picker")
                                                  .last
                                              : educationalCertificate
                                                  .split('picker')
                                                  .last,
                                          textScaleFactor: 1.0,
                                          style: TextStyle(
                                            fontFamily: 'GESSTwo',
                                            color: Color(0xff4f4f4f),
                                            fontSize: Fonts.fontSize_small,
                                            fontWeight: FontWeight.w300,
                                            fontStyle: FontStyle.normal,
                                          )),
                                      Icon(
                                        Icons.camera_alt,
                                        color: Color(0xff4f4f4f),
                                      ),
                                    ]),
                                  )),
                            ],
                          ))
                      : SizedBox(),
                  new Container(
                      margin: EdgeInsets.only(top: 5),
                      width: MediaQuery.of(context).size.width,
                      height: 56,
                      decoration: new BoxDecoration(color: Color(0xffffffff)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: new Text(S.current.description,
                                  textScaleFactor: 1.0,
                                  style: TextStyle(
                                    fontFamily: 'GESSTwo',
                                    color: Color(0xffdb0916),
                                    fontSize: Fonts.fontSize_small,
                                    fontWeight: FontWeight.w500,
                                    fontStyle: FontStyle.normal,
                                  ))),
                          Flexible(
                            child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: new TextFormField(
                                  controller: _userDescriptionController,
                                  style: TextStyle(
                                    fontFamily: 'GESSTwo',
                                    color: Color(0xff4f4f4f),
                                    fontSize: Fonts.fontSize_small,
                                    fontWeight: FontWeight.w300,
                                    fontStyle: FontStyle.normal,
                                  ),
                                  textAlign: TextAlign.center,
                                  decoration: InputDecoration(
                                    disabledBorder: InputBorder.none,
                                    border: InputBorder.none,
                                  ),
                                )),
                          ),
                        ],
                      )),
                  new Container(
                      margin: EdgeInsets.only(top: 5),
                      height: 56,
                      decoration: new BoxDecoration(color: Color(0xffffffff)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: new Text(S.current.parentMobile,
                                  textScaleFactor: 1.0,
                                  style: TextStyle(
                                    fontFamily: 'GESSTwo',
                                    color: Color(0xffdb0916),
                                    fontSize: Fonts.fontSize_small,
                                    fontWeight: FontWeight.w500,
                                    fontStyle: FontStyle.normal,
                                  ))),
                          Flexible(
                            child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: new TextFormField(
                                  textAlign: TextAlign.center,
                                  keyboardType: TextInputType.number,
                                  controller: _userParentMobileController,
                                  style: TextStyle(
                                    fontFamily: 'GESSTwo',
                                    color: Color(0xff4f4f4f),
                                    fontSize: Fonts.fontSize_small,
                                    fontWeight: FontWeight.w300,
                                    fontStyle: FontStyle.normal,
                                  ),
                                  decoration: InputDecoration(
                                    disabledBorder: InputBorder.none,
                                    border: InputBorder.none,
                                  ),
                                )),
                          ),
                        ],
                      )),
                  new Container(
                      margin: EdgeInsets.only(top: 5),
                      width: MediaQuery.of(context).size.width,
                      height: 56,
                      decoration: new BoxDecoration(color: Color(0xffffffff)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: new Text(S.current.email,
                                  textScaleFactor: 1.0,
                                  style: TextStyle(
                                    fontFamily: 'GESSTwo',
                                    color: Color(0xffdb0916),
                                    fontSize: Fonts.fontSize_small,
                                    fontWeight: FontWeight.w500,
                                    fontStyle: FontStyle.normal,
                                  ))),
                          Flexible(
                            child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: new TextFormField(
                                  textAlign: TextAlign.center,
                                  controller: _userMailController,
                                  style: TextStyle(
                                    fontFamily: 'GESSTwo',
                                    color: Color(0xff4f4f4f),
                                    fontSize: Fonts.fontSize_small,
                                    fontWeight: FontWeight.w300,
                                    fontStyle: FontStyle.normal,
                                  ),
                                  decoration: InputDecoration(
                                    disabledBorder: InputBorder.none,
                                    border: InputBorder.none,
                                  ),
                                )),
                          ),
                        ],
                      )),
                  new Container(
                      margin: EdgeInsets.only(top: 5),
                      width: MediaQuery.of(context).size.width,
                      height: 56,
                      decoration: new BoxDecoration(color: Color(0xffffffff)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: new Text(S.current.changePassword,
                                  textScaleFactor: 1.0,
                                  style: TextStyle(
                                    fontFamily: 'GESSTwo',
                                    color: Color(0xffdb0916),
                                    fontSize: Fonts.fontSize_small,
                                    fontWeight: FontWeight.w500,
                                    fontStyle: FontStyle.normal,
                                  ))),
                          Flexible(
                            child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: new TextFormField(
                                  textAlign: TextAlign.center,
                                  obscureText: true,
                                  controller: _userPasswordController,
                                  style: TextStyle(
                                    fontFamily: 'GESSTwo',
                                    color: Color(0xff4f4f4f),
                                    fontSize: Fonts.fontSize_small,
                                    fontWeight: FontWeight.w300,
                                    fontStyle: FontStyle.normal,
                                  ),
                                  decoration: InputDecoration(
                                    disabledBorder: InputBorder.none,
                                    border: InputBorder.none,
                                  ),
                                )),
                          ),
                        ],
                      )),
                ],
              )),
        )
      ],
    );
  }

  Widget getDropDownCitiesView() {
    return Container(
      margin: EdgeInsets.only(top: 5),
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
              padding: const EdgeInsets.all(8.0),
              child: new Text(S.current.city,
                  textScaleFactor: 1.0,
                  style: TextStyle(
                    fontFamily: 'GESSTwo',
                    color: Color(0xffdb0916),
                    fontSize: Fonts.fontSize_small,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                  ))),
          Container(
              height: 50,
              child: DropdownButton<City>(
                value: _selectedDropdownCityValue,
                underline: Container(
                  height: 2,
                ),
                onChanged: (City newValue) {
                  setState(() {
                    _selectedDropdownCityValue = newValue;
                  });
                },
                items:
                    dropdownCityList?.map<DropdownMenuItem<City>>((City value) {
                  return DropdownMenuItem<City>(
                    value: value,
                    child: Container(
                        width: MediaQuery.of(context).size.width * .2,
                        child: new Text(value.name,
                            textScaleFactor: 1.0,
                            style: TextStyle(
                              fontFamily: 'GESSTwo',
                              color: Color(0xff4f4f4f),
                              fontSize: Fonts.fontSize_small,
                              fontWeight: FontWeight.w300,
                              fontStyle: FontStyle.normal,
                            ))),
                  );
                })?.toList(),
              )),
        ],
      ),
    );
  }

  Widget getDropDownEduSystemsView() {
    return Container(
      margin: EdgeInsets.only(top: 5),
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
              padding: const EdgeInsets.all(8.0),
              child: new Text(S.current.eduSys,
                  textScaleFactor: 1.0,
                  style: TextStyle(
                    fontFamily: 'GESSTwo',
                    color: Color(0xffdb0916),
                    fontSize: Fonts.fontSize_small,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                  ))),
          Container(
              height: 50,
              child: DropdownButton<EduSystem>(
                value: _selectedDropdownEduSystemValue,
                underline: Container(
                  height: 2,
                ),
                onChanged: (EduSystem newValue) {
                  setState(() {
                    _selectedDropdownEduSystemValue = newValue;
                  });
                },
                items: dropdownEduSystemsList
                    ?.map<DropdownMenuItem<EduSystem>>((EduSystem value) {
                  return DropdownMenuItem<EduSystem>(
                    value: value,
                    child: Container(
                        width: MediaQuery.of(context).size.width * .2,
                        child: new Text(value.name,
                            textScaleFactor: 1.0,
                            style: TextStyle(
                              fontFamily: 'GESSTwo',
                              color: Color(0xff4f4f4f),
                              fontSize: Fonts.fontSize_small,
                              fontWeight: FontWeight.w300,
                              fontStyle: FontStyle.normal,
                            ))),
                  );
                })?.toList(),
              )),
        ],
      ),
    );
  }

  Widget getDropDownDreamsView() {
    return Container(
      margin: EdgeInsets.only(top: 5),
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
              padding: const EdgeInsets.all(8.0),
              child: new Text(S.current.dream,
                  textScaleFactor: 1.0,
                  style: TextStyle(
                    fontFamily: 'GESSTwo',
                    color: Color(0xffdb0916),
                    fontSize: Fonts.fontSize_small,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                  ))),
          Container(
              height: 50,
              child: DropdownButton<Dream>(
                value: _selectedDropdownDreamValue,
                underline: Container(
                  height: 2,
                ),
                onChanged: (Dream newValue) {
                  setState(() {
                    _selectedDropdownDreamValue = newValue;
                  });
                },
                items: dropdownDreamsList
                    ?.map<DropdownMenuItem<Dream>>((Dream value) {
                  return DropdownMenuItem<Dream>(
                    value: value,
                    child: Container(
                        width: MediaQuery.of(context).size.width * .2,
                        child: new Text(value.name,
                            textScaleFactor: 1.0,
                            style: TextStyle(
                              fontFamily: 'GESSTwo',
                              color: Color(0xff4f4f4f),
                              fontSize: Fonts.fontSize_small,
                              fontWeight: FontWeight.w300,
                              fontStyle: FontStyle.normal,
                            ))),
                  );
                })?.toList(),
              )),
        ],
      ),
    );
  }

  Widget getDropDownCountriesView() {
    return Container(
      margin: EdgeInsets.only(top: 5),
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
              padding: const EdgeInsets.all(8.0),
              child: new Text(S.current.country,
                  textScaleFactor: 1.0,
                  style: TextStyle(
                    fontFamily: 'GESSTwo',
                    color: Color(0xffdb0916),
                    fontSize: Fonts.fontSize_small,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                  ))),
          Container(
              height: 50,
              child: DropdownButton<City>(
                value: _selectedDropdownCityValue,
                underline: Container(
                  height: 2,
                ),
                onChanged: (City newValue) {
                  setState(() {
                    _selectedDropdownCityValue = newValue;
                  });
                },
                items:
                    dropdownCityList?.map<DropdownMenuItem<City>>((City value) {
                  return DropdownMenuItem<City>(
                    value: value,
                    child: Container(
                        width: MediaQuery.of(context).size.width * .2,
                        child: new Text(value.name,
                            textScaleFactor: 1.0,
                            style: TextStyle(
                              fontFamily: 'GESSTwo',
                              color: Color(0xff4f4f4f),
                              fontSize: Fonts.fontSize_small,
                              fontWeight: FontWeight.w300,
                              fontStyle: FontStyle.normal,
                            ))),
                  );
                })?.toList(),
              )),
        ],
      ),
    );
  }

  Widget getBirthDateView() {
    return GestureDetector(
      child: Container(
        margin: EdgeInsets.only(top: 5),
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: new Text(S.current.birthDate,
                  textScaleFactor: 1.0,
                  style: TextStyle(
                    fontFamily: 'GESSTwo',
                    color: Color(0xffdb0916),
                    fontSize: Fonts.fontSize_small,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                  )),
            ),
            Container(
                alignment: Alignment.center,
                height: 50,
                padding: EdgeInsets.symmetric(horizontal: 12),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      (selectedDate != null)
                          ? dateFormat.format(selectedDate).toString()
                          : S.current.birthDate,
                      textScaleFactor: 1.0,
                      style: TextStyle(
                        fontFamily: 'GESSTwo',
                        color: Color(0xff4f4f4f),
                        fontSize: Fonts.fontSize_small,
                        fontWeight: FontWeight.w300,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                  ],
                ))
          ],
        ),
      ),
      onTap: () {
        _selectDate(context);
      },
    );
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate ?? DateTime.now(),
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }
}
