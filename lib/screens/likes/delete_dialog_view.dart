import 'package:aytamy/common/fonts.dart';
import 'package:aytamy/common/widgets/slide_dialog.dart';
import 'package:aytamy/generated/l10n.dart';
import 'package:aytamy/screens/likes/provider/likes_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

deleteDialogView<T>(
    {@required BuildContext context,
    Color barrierColor,
    bool barrierDismissible = false,
    Duration transitionDuration = const Duration(milliseconds: 300),
    Color pillColor,
    String message,
    Color backgroundColor,
    Function onChangePasswordClick,
    int id,
    Function DeleteCallBack}) {
  assert(context != null);

  return showGeneralDialog(
    context: context,
    pageBuilder: (context, animation1, animation2) {},
    barrierColor: barrierColor ?? Colors.black.withOpacity(0.7),
    barrierDismissible: true,
    barrierLabel: "Dismiss",
    transitionDuration: transitionDuration,
    transitionBuilder: (context, animation1, animation2, widget) {
      final curvedValue = Curves.easeInOut.transform(animation1.value) - 1.0;
      return GestureDetector(
        child: Transform(
          transform: Matrix4.translationValues(0.0, curvedValue * -300, 0.0),
          child: Opacity(
            opacity: animation1.value,
            child: SingleChildScrollView(
              child: SlideDialog(
                  heightRatio: 1.45,
                  pillColor: pillColor ?? Colors.blueGrey[200],
                  backgroundColor:
                      backgroundColor ?? Theme.of(context).canvasColor,
                  child: DeleteDialogView(
                    id: id,
                    DeleteCallBack: DeleteCallBack,
                  )),
            ),
          ),
        ),
        onTap: () {
          Navigator.of(context).pop();
        },
      );
    },
  );
}

class DeleteDialogView extends StatefulWidget {
  final int id;
  final Function DeleteCallBack;

  const DeleteDialogView({Key key, this.id, this.DeleteCallBack})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _DeleteDialogViewState(id, DeleteCallBack);
  }
}

class _DeleteDialogViewState extends State<DeleteDialogView> {
  final int id;
  int currentSum = 10;
  int currentQuantity = 1;
  LikesModel _likesModel = LikesModel();
  final Function DeleteCallBack;

  _DeleteDialogViewState(this.id, this.DeleteCallBack);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          new Container(
            width: 60.240234375,
            height: 60.24017333984375,
            decoration: new BoxDecoration(
              color: const Color(0xff7c94b6),
              image: new DecorationImage(
                image: AssetImage("assets/images/img_profile.jpeg"),
                fit: BoxFit.cover,
              ),
              borderRadius: new BorderRadius.all(new Radius.circular(50.0)),
            ),
          ),
          SizedBox(
            height: 5,
          ),
          RichText(
            textScaleFactor: 1.0,
            textAlign: TextAlign.start,
            text: TextSpan(children: [
              TextSpan(
                  text: " نويت ان",
                  style: TextStyle(
                    fontFamily: 'GESSTwo',
                    color: Color(0xff4f4f4f),
                    fontSize: Fonts.fontSize_small,
                    fontWeight: FontWeight.w300,
                    fontStyle: FontStyle.normal,
                  )),
              new TextSpan(
                  text: "أكفلك",
                  style: TextStyle(
                    fontFamily: 'GESSTwo',
                    color: Color(0xff000000),
                    fontSize: Fonts.fontSize_small,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                  )),
              TextSpan(
                  text: " وستصلك رسالة فور وصول",
                  style: TextStyle(
                    fontFamily: 'GESSTwo',
                    color: Color(0xff4f4f4f),
                    fontSize: Fonts.fontSize_small,
                    fontWeight: FontWeight.w300,
                    fontStyle: FontStyle.normal,
                  )),
            ]),
          ),

          // Rectangle 49
          Column(children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: InkWell(
                onTap: () {
                  DeleteCallBack();
                },
                child: Container(
                    width: MediaQuery.of(context).size.width * .6,
                    height: 35,
                    alignment: Alignment.center,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                      child: Row(
                        children: [
                          Icon(Icons.four_k),
                          SizedBox(
                            width: 5,
                          ),
                          Text(S.current.delete + " ",
                              textScaleFactor: 1.0,
                              style: const TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w700,
                                  fontFamily: Fonts.fontfamily,
                                  fontStyle: FontStyle.normal,
                                  fontSize: Fonts.fontSize_small),
                              textAlign: TextAlign.center)
                        ],
                      ),
                    ),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(16))),
              ),
            ),
            Container(
                width: MediaQuery.of(context).size.width * .6,
                height: 35,
                alignment: Alignment.center,
                child: Padding(
                  padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Icon(Icons.four_k),
                      SizedBox(
                        width: 5,
                      ),
                      new Text("ابلاغ عن عطل او شكوى",
                          textScaleFactor: 1.0,
                          style: TextStyle(
                            fontFamily: Fonts.fontfamily,
                            color: Color(0xff000000),
                            fontSize: Fonts.fontSize_small,
                            fontWeight: FontWeight.w500,
                            fontStyle: FontStyle.normal,
                          ))
                    ],
                  ),
                ),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(16)))
          ])
        ],
      ),
    );
  }
}
