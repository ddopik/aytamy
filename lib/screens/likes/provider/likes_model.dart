import 'package:aytamy/common/model/User.dart';
import 'package:aytamy/network/dio_manager.dart';
import 'package:aytamy/screens/home/model/response.dart';
import 'package:aytamy/screens/likes/model/orphen.dart';
import 'package:aytamy/screens/likes/model/likes_response.dart';

class LikesModel {
  List<Orphen> likes;

  void getUserLikes({onSuccess, onError}) {
    likes = [Orphen(), Orphen(), Orphen(), Orphen()];
    return DIOManager().getLikes(onSuccess: (response) {
      print("getLikes onSuccess ---->" + response.toString());
      LikesResponse likesResponse = LikesResponse.fromJson(response);
      likes = likesResponse.data;
      onSuccess(likes);
    }, onError: (error) {
      onError(error.toString());
      print("getLikes onError ---->" + error.toString());
    });
  }

  deleteOrphenFromLikes({int orphenId,onSuccess, onError}) {
    DIOManager().deleteOrphen(orphinId: orphenId,onSuccess: (response) {
      print("deleteLike onSuccess ---->" + response.toString());
      onSuccess();
    }, onError: (error) {
      onError(error.toString());
      print("deleteLike onError ---->" + error.toString());
    });
  }
}
