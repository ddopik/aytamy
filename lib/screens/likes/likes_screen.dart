import 'package:aytamy/common/stats_widgets.dart';
import 'package:aytamy/generated/l10n.dart';
import 'package:flutter/cupertino.dart';
import 'likews_list_item_view.dart';
import 'package:aytamy/screens/likes/provider/likes_model.dart';
import 'package:aytamy/screens/likes/model/orphen.dart';

class likesScreen extends StatefulWidget {
  @override
  _likesScreenState createState() => _likesScreenState();
}

class _likesScreenState extends State<likesScreen> {
  LikesModel _likesModel = LikesModel();
  List<Orphen> likes;

  @override
  void initState() {
    showLoading(context);
    _likesModel.getUserLikes(onSuccess: (response) {
      dismissLoading();
      setState(() {
        likes = response;
      });
    }, onError: (error) {
      dismissLoading();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return likes != null
        ? Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: new Text(S.current.mostRecentLikes,
                    style: TextStyle(
                      fontFamily: 'GESSTwo',
                      color: Color(0xff000000),
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                    )),
              ),
              ListView.builder(
                shrinkWrap: true,
                itemCount: likes.length,
                itemBuilder: (BuildContext context, int index) {
                  return LikesListItemView(
                    user: likes[index].data_to,
                    DeleteOrphenCallBack: () {
                      showLoading(context);
                      _likesModel.deleteOrphenFromLikes(
                          orphenId: likes[index].data_to.id,
                          onSuccess: () {
                            dismissLoading();
                            setState(() {
                              likes.removeAt(index);
                            });
                            showSuccesses(context, S.current.deleteItemSuccess);
                            Navigator.of(context).pop();
                          },
                          onError: (error) {
                            dismissLoading();
                          });
                    },
                  );
                },
              )
            ],
          )
        : Text("");
  }
}
