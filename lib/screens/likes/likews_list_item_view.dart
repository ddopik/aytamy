import 'package:aytamy/common/fonts.dart';
import 'package:aytamy/common/model/User.dart';
import 'package:aytamy/screens/likes/delete_dialog_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class LikesListItemView extends StatelessWidget {
  final User user;
  final Function DeleteOrphenCallBack;
  const LikesListItemView({Key key, this.user, this.DeleteOrphenCallBack}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: new Container(
          width: MediaQuery.of(context).size.width,
          height: 80,
          decoration: new BoxDecoration(color: Color(0xffe4e4e4)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                "assets/logo.png",
                height: 35,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  new Text(user.name.toString(),
                      textScaleFactor: 1.0,
                      style: TextStyle(
                        fontFamily: 'GESSTwo',
                        color: Color(0xff000000),
                        fontSize: Fonts.fontSize_small,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                      )),
                  new Text("أنت ضمن اولويات الكفيل ",
                      textScaleFactor: 1.0,
                      style: TextStyle(
                        fontFamily: 'GESSTwo',
                        color: Color(0xff4f4f4f),
                        fontSize: Fonts.fontSize_small,
                        fontWeight: FontWeight.w500,
                        fontStyle: FontStyle.normal,
                        letterSpacing: 0.12,
                      ))
                ],
              ),
              SizedBox(
                width: 40,
              ),
              InkWell(onTap:(){
                deleteDialogView(
                    context: context,
                    DeleteCallBack: DeleteOrphenCallBack,
                    id: user.id);
              },child: Icon(Icons.offline_bolt_rounded))
            ],
          )),
    );
  }
}
