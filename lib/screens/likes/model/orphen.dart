import 'package:aytamy/common/model/User.dart';

class Orphen {
  int _id;
  int _from;
  int _to;
  dynamic _data_to;

  int get id => _id;

  int get from => _from;

  int get to => _to;

  User get data_to => _data_to;

  Orphen({int id, int from, int to, User data_to}) {
    _id = id;
    _from = from;
    _to = to;
    _data_to = data_to;
  }

  Orphen.fromJson(dynamic json) {
    _id = json["id"];
    _from = json["from"];
    _to = json["to"];
    if (json["data_to"] != null) {
      _data_to = User.fromJson(json["data_to"]);
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["to"] = _to;
    map["from"] = _from;
    if (_data_to != null) {
      map["data_to"] = _data_to.toJson();
    }
    return map;
  }
}