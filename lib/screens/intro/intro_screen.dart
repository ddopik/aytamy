import 'package:aytamy/app/route.dart';
import 'package:aytamy/common/fonts.dart';
import 'package:aytamy/generated/l10n.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class IntroScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print("IntroScreen ---> Build()");
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        body: Container(
            alignment: Alignment.center,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height * .1,
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 20),
                  height: 128.00436401367188,
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  child: Image.asset(
                    "assets/logo.png",
                    height: MediaQuery.of(context).size.height * .35,
                  ),
                ),
                Column(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * .80,
                      height: 45,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(6),
                          border: Border.all(
                              color: const Color(0xff4f4f4f), width: 1.5)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Text(S.current.soon,
                              textScaleFactor: 1.0,
                              style: const TextStyle(
                                  color: const Color(0xffdb0011),
                                  fontWeight: FontWeight.w700,
                                  fontFamily: Fonts.fontfamily,
                                  fontStyle: FontStyle.normal,
                                  fontSize: Fonts.fontSize_small)),
                          Text(S.current.text_1,
                              textScaleFactor: 1.0,
                              style: TextStyle(
                                  color: const Color(0xff4f4f4f),
                                  fontWeight: FontWeight.w700,
                                  fontFamily: Fonts.fontfamily,
                                  fontStyle: FontStyle.normal,
                                  fontSize: Fonts.fontSize_small)),
                          new Image.asset(
                            "assets/icons/intro.png",
                            height: 24,
                            color: Color(0xff4f4f4f),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * .80,
                      height: 45,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            flex: 1,
                            child: GestureDetector(
                              child: Container(
                                height: 50,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5)),
                                    border: Border.all(
                                        color: const Color(0xffdb0011),
                                        width: 1.5)),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    S.of(context).login,
                                    textScaleFactor: 1.0,
                                    style: const TextStyle(
                                        color: const Color(0xffdb0011),
                                        fontWeight: FontWeight.w700,
                                        fontFamily: Fonts.fontfamily,
                                        fontStyle: FontStyle.normal,
                                        fontSize: Fonts.fontSize_small),
                                  ),
                                ),
                              ),
                              onTap: () {
                                Navigator.of(context).pushNamed(Routes.LOGIN);
                              },
                            ),
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          // Rectangle
                          Expanded(
                            flex: 1,
                            child: GestureDetector(
                              child: Container(
                                height: 50,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5)),
                                    color: const Color(0xffdb0011)),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    S.current.signUp,
                                    textScaleFactor: 1.0,
                                    style: const TextStyle(
                                        color: const Color(0xffffffff),
                                        fontWeight: FontWeight.w700,
                                        fontFamily: Fonts.fontfamily,
                                        fontStyle: FontStyle.normal,
                                        fontSize: Fonts.fontSize_small),
                                  ),
                                ),
                              ),
                              onTap: () {
                                Navigator.of(context).pushNamed(Routes.SIGN_UP);
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 22,
                    ),
                    InkWell(
                      child: Container(
                        width: MediaQuery.of(context).size.width * .80,
                        height: 45,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: const Color(0xffdb0011)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            SizedBox(
                              width: 22,
                            ),
                            Text(S.current.explore,
                                textScaleFactor: 1.0,
                                style: const TextStyle(
                                    color: const Color(0xffffffff),
                                    fontWeight: FontWeight.w700,
                                    fontFamily: Fonts.fontfamily,
                                    fontStyle: FontStyle.normal,
                                    fontSize: Fonts.fontSize_meduim),
                                textAlign: TextAlign.left),
                            Container(
                              margin: EdgeInsets.only(left: 18),
                              child: Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.white,
                                size: 28,
                              ),
                            ),
                          ],
                        ),
                      ),
                      onTap: () {
                        Navigator.of(context).pushNamed(Routes.HOME);
                      },
                    )
                  ],
                ),
                SizedBox(
                  height: 42,
                ),
                Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      new Image.asset(
                        "assets/icons/tag.png",
                        height: 24,
                        color: Color(0xff4f4f4f),
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      GestureDetector(
                        child: Text(
                          S.current.privacyAndPoliceTitle,
                          textScaleFactor: 1.0,
                          style: const TextStyle(
                              color: const Color(0xff4f4f4f),
                              fontWeight: FontWeight.w700,
                              fontFamily: Fonts.fontfamily,
                              fontStyle: FontStyle.normal,
                              fontSize: Fonts.fontSize_meduim),
                        ),
                        onTap: () {
                          Navigator.of(context)
                              .pushNamed(Routes.PRIVACY_POLICY);
                        },
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      GestureDetector(
                        child: Text(
                          S.current.term_of_use,
                          textScaleFactor: 1.0,
                          style: const TextStyle(
                              color: const Color(0xff4f4f4f),
                              fontWeight: FontWeight.w700,
                              fontFamily: Fonts.fontfamily,
                              fontStyle: FontStyle.normal,
                              fontSize: Fonts.fontSize_meduim),
                        ),
                        onTap: () {
                          Navigator.of(context).pushNamed(Routes.TERM_OF_USE);
                        },
                      ),
                      // Forgot password?
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(S.current.copyRight_2 + "@",
                          textScaleFactor: 1.0,
                          style: const TextStyle(
                              color: const Color(0xff4f4f4f),
                              fontWeight: FontWeight.w300,
                              fontFamily: Fonts.fontfamily,
                              fontStyle: FontStyle.normal,
                              fontSize: 10),
                          textAlign: TextAlign.center)
                    ],
                  ),
                ),
              ],
            )),
      ),
    );
  }
}
