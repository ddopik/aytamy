import 'package:aytamy/app/route.dart';
import 'package:aytamy/common/colors.dart';
import 'package:aytamy/common/fonts.dart';
import 'package:aytamy/common/stats_widgets.dart';
import 'package:aytamy/common/tools.dart';
import 'package:aytamy/generated/l10n.dart';
import 'package:aytamy/screens/signup/provider/registrationModel.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class SignUpScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SignUpScreenState();
  }
}

class _SignUpScreenState extends State<SignUpScreen> {
  final GlobalKey<FormState> _signUpFormState = GlobalKey<FormState>();
  final TextEditingController _userNameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  RegistrationModel _registrationModel = RegistrationModel();

  @override
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    print("SignUpScreen ---> Build()");
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.red),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios, size: 22, color: rustRed),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _signUpFormState,
          child: Container(
              alignment: Alignment.topCenter,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Container(
                    margin: EdgeInsets.only(bottom: 20),
                    height: 128.00436401367188,
                    decoration: BoxDecoration(
                      color: Colors.white,
                    ),
                    child: Image.asset(
                      "assets/logo.png",
                      height: MediaQuery.of(context).size.height * .35,
                    ),
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                          width: MediaQuery.of(context).size.width * .80,
                          height: 50,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6),
                              border:
                                  Border.all(color: Colors.red, width: 1.7)),
                          child: TextFormField(
                            controller: _userNameController,
                            style: TextStyle(
                                color: const Color(0xffdb0011),
                                fontWeight: FontWeight.w700,
                                // fontFamily: Fonts.fontfamily,
                                fontStyle: FontStyle.normal,
                                fontSize: Fonts.fontSize_small),
                            decoration: InputDecoration(
                              contentPadding:
                                  EdgeInsets.symmetric(horizontal: 14),
                              suffixIcon: Icon(
                                Icons.person,
                                size: 21,
                                color: Colors.red,
                              ),
                              // hintText: 'What do people call you?',
                              labelText: S.current.userName,
                            ),
                          )),
                      SizedBox(
                        height: 22,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width * .80,
                          height: 50,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6),
                              border:
                                  Border.all(color: Colors.red, width: 1.7)),
                          child: TextFormField(
                            controller: _emailController,
                            style: TextStyle(
                                color: Color(0xffdb0011),
                                fontWeight: FontWeight.w700,
                                // fontFamily: Fonts.fontfamily,
                                fontStyle: FontStyle.normal,
                                fontSize: Fonts.fontSize_small),
                            decoration: InputDecoration(
                              contentPadding:
                                  EdgeInsets.symmetric(horizontal: 14),
                              suffixIcon: Icon(
                                Icons.email,
                                size: 21,
                                color: Colors.red,
                              ),
                              // hintText: 'What do people call you?',
                              labelText: S.current.email,
                            ),
                          )),
                      SizedBox(
                        height: 22,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width * .80,
                          height: 50,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6),
                              border:
                                  Border.all(color: Colors.red, width: 1.7)),
                          child: TextFormField(
                            controller: _passwordController,
                            obscureText: true,
                            style: TextStyle(
                                color: const Color(0xffdb0011),
                                fontWeight: FontWeight.w700,
                                // fontFamily: Fonts.fontfamily,
                                fontStyle: FontStyle.normal,
                                fontSize: Fonts.fontSize_small),
                            decoration: InputDecoration(
                              contentPadding:
                                  EdgeInsets.symmetric(horizontal: 14),
                              suffixIcon: Icon(
                                Icons.lock,
                                size: 21,
                                color: Colors.red,
                              ),
                              // hintText: 'What do people call you?',
                              labelText: S.of(context).password,
                            ),
                          )),

                      SizedBox(
                        height: 22,
                      ),
                      // Rectangle
                      InkWell(
                          child: Container(
                            width: MediaQuery.of(context).size.width * .80,
                            height: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: const Color(0xffdb0011)),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                SizedBox(
                                  width: 22,
                                ),
                                Text(S.current.signUp,
                                    textScaleFactor: 1.0,
                                    style: const TextStyle(
                                        color: const Color(0xffffffff),
                                        fontWeight: FontWeight.w700,
                                        // fontFamily: Fonts.fontfamily,
                                        fontStyle: FontStyle.normal,
                                        fontSize: Fonts.fontSize_small),
                                    textAlign: TextAlign.left),
                                Container(
                                  margin: EdgeInsets.only(left: 18),
                                  child: Icon(
                                    Icons.arrow_forward_ios,
                                    color: Colors.white,
                                    size: 21,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          onTap: () {
                            if (validateForm()) {
                              showLoading(context);
                              _registrationModel.signUpUser(
                                  userName: _userNameController.value.text,
                                  email: _emailController.value.text,
                                  password: _passwordController.value.text,
                                  onSuccess: () {
                                    Navigator.of(context).pushNamed(Routes
                                        .REGISTRATION_INFO_FIRST_STEP);
                                    dismissLoading();
                                  },
                                  onError: (errorResponse) {
                                    showError(errorResponse);
                                    dismissLoading();
                                  });
                            }
                            // Navigator.of(context)
                            //     .pushNamed(Routes.SIGN_UP_STEP_ONE);
                          }),
                      SizedBox(
                        height: 22,
                      ),
                      // Rectangle
                      InkWell(
                        onTap: () {
                          showLoading(context);
                          _registrationModel.signInWithFacebook(
                              onSucces: () {
                                dismissLoading();
                            Navigator.of(context).pushNamed(
                                Routes.REGISTRATION_INFO_FIRST_STEP);

                          }, onError: (errorResponse) {
                            dismissLoading();
                            showError(errorResponse);
                          });
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width * .80,
                          height: 50,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                  color: Color(0xff0078d7), width: 1.7)),
                          child: Padding(
                            padding: const EdgeInsets.all(2.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Text(
                                  S.current.signUpFaceBook,
                                  textScaleFactor: 1.0,
                                  style: const TextStyle(
                                      color: const Color(0xff0078d7),
                                      fontWeight: FontWeight.w700,
                                      // fontFamily: Fonts.fontfamily,
                                      fontStyle: FontStyle.normal,
                                      fontSize: Fonts.fontSize_small),
                                ),
                                SizedBox(),
                                Container(
                                  child: new Image.asset(
                                    "assets/icons/facebook.png",
                                    height: 30,
                                    width: 30,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 22,
                      ),
                      // Rectangle
                      InkWell(
                        onTap: () {
                          showLoading(context);
                          _registrationModel.signInWithGoogle(onSucces: () {
                            Navigator.of(context).pushNamed(
                                Routes.REGISTRATION_INFO_FIRST_STEP);
                            dismissLoading();
                          }, onError: (errorResponse) {
                            showError(S.current.applicationError);
                            dismissLoading();
                          });
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width * .80,
                          height: 50,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                  color: Color(0xffdb0011), width: 1.7)),
                          child: Padding(
                            padding: const EdgeInsets.all(2.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Text(
                                  S.current.signUpGoogle,
                                  textScaleFactor: 1.0,
                                  style: TextStyle(
                                      color: Color(0xffdb0011),
                                      fontWeight: FontWeight.w700,
                                      // fontFamily: Fonts.fontfamily,
                                      fontStyle: FontStyle.normal,
                                      fontSize: Fonts.fontSize_small),
                                ),
                                SizedBox(),
                                Container(
                                  child: new Image.asset(
                                    "assets/icons/google.png",
                                    height: 30,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  RichText(
                      textScaleFactor: 1.0,
                      text: TextSpan(children: [
                    TextSpan(
                        style: const TextStyle(
                                color: const Color(0xffff6a00),
                                fontWeight: FontWeight.w500,
                                // fontFamily: Fonts.fontfamily,
                                fontStyle: FontStyle.normal,
                                fontSize: Fonts.fontSize_small),
                            text: S.current.alreadyHaveAccount + " "),
                    TextSpan(
                        style: const TextStyle(
                                color: const Color(0xff4f4f4f),
                                fontWeight: FontWeight.w500,
                                // fontFamily: Fonts.fontfamily,
                                fontStyle: FontStyle.normal,
                                fontSize: Fonts.fontSize_small),
                            text: S.current.login,
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                Navigator.of(context).pushNamed(Routes.LOGIN);
                              })
                  ])),
                  SizedBox(
                    height: 24,
                  ),
                ],
              )),
        ),
      ),
    );
  }

  validateForm() {
    if (!isEmail(_emailController.value.text)) {
      showError(S.current.invalidEmail);
      return false;
    }
    if (_passwordController.value.text.isEmpty) {
      showError(S.current.invalidPassword);
    } else if (_passwordController.value.text.length < 6) {
      showError(S.current.passwordErrorLength);
      return false;
    }

    if (_userNameController.value.text.isEmpty) {
      showError(S.current.invalidUserName);
      return false;
    }

    return true;
  }
}
