import 'package:aytamy/common/dimen.dart';
import 'package:aytamy/common/fonts.dart';
import 'package:aytamy/generated/l10n.dart';
import 'package:aytamy/screens/payment/payment_history_view.dart';
import 'package:aytamy/screens/profile/case_data/case_data_slide_dialog_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class PaymentScreen extends StatefulWidget {
  final int index;

  PaymentScreen({this.index});

  @override
  State<StatefulWidget> createState() {
    return _PaymentScreenState();
  }
}

class _PaymentScreenState extends State<PaymentScreen> {


  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return getMainView();
  }

  Widget getMainView() {
    return SafeArea(
      child: Scaffold(
        appBar:  AppBar(
            elevation: 0.0,
            centerTitle: false,
            title: Text(S.current.payment2,
                textScaleFactor: 1.0,
                style: const TextStyle(
                    color: const Color(0xffffffff),
                    fontWeight: FontWeight.w700,
                    fontFamily: Fonts.fontfamily,
                    fontStyle: FontStyle.normal,
                    fontSize: Fonts.fontSize_small),
                textAlign: TextAlign.center),
            actions: [
              IconButton(
                  icon: Icon(Icons.arrow_forward_ios_outlined),
                  onPressed: () {
                    Navigator.of(context).pop();
                  })
            ],
            leading: Container(),
          ),
        body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Stack(
            children: [
              getOrphanHeaderView(),
              Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  child: DraggablePaymentListView()),
            ],
          ),
        ),
      ),),
    );
  }

  Widget getOrphanHeaderView() {
    return

      Column(children: [
        SizedBox(height: inner_boundary_field_space,),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Image.asset(
                    "assets/icons/refresh.png",
                    height: 40,
                    width: 40,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      new Text("قيمة الكفالة الدورية",
                          textScaleFactor: 1.0,
                          style: TextStyle(
                            fontFamily: Fonts.fontfamily,
                            color: Color(0xff4f4f4f),
                            fontSize: Fonts.fontSize_meduim,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.normal,
                          )),
                      SizedBox(height: 6,),
                      new Text("\$500",
                          textScaleFactor: 1.0,
                          style: TextStyle(
                            fontFamily: 'Arial',
                            color: Color(0xff4f4f4f),
                            fontSize: 34,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.normal,
                          )),
                      SizedBox(height: 6,),
                      Row(
                        children: [
                          new Text("الدفع غير متاح",
                              style: TextStyle(
                                fontFamily: Fonts.fontfamily,
                                color: Color(0xffdb0011),
                                fontSize: Fonts.fontSize_small,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                              )),
                          Image.asset(
                            "assets/icons/pay.png",
                            height: 15,
                            width: 15,
                            color: Color(0xffdb0011),
                          ),
                        ],
                      )
                    ],
                  )
                ],
              ),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              child: new Container(
                width: 80,
                height: 100,
                decoration: new BoxDecoration(
                  color: Color(0xffffffff),
                  boxShadow: [
                    BoxShadow(
                        color: Color(0x24000000),
                        offset: Offset(0, 8),
                        blurRadius: 10,
                        spreadRadius: 0)
                  ],
                ),
                child: Image.asset(
                  "assets/icons/bank.png",
                  fit: BoxFit.cover,
                ),
              )),
                  ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(10)),child:
                  new Container(
                    width: 80,
                    height: 100,
                    decoration: new BoxDecoration(
                      color: Color(0xffffffff),
                      boxShadow: [BoxShadow(
                          color: Color(0x24000000),
                          offset: Offset(0,8),
                          blurRadius: 10,
                          spreadRadius: 0

                      ) ],
                    ),child: Image.asset(
                    "assets/icons/paypal.png",
                    fit: BoxFit.cover,
                  ),
                  )),
                  ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(10)),child:
                  new Container(
                    width: 80,
                    height: 100,
                    decoration: new BoxDecoration(
                      color: Color(0xffffffff),
                      boxShadow: [BoxShadow(
                          color: Color(0x24000000),
                          offset: Offset(0,8),
                          blurRadius: 10,
                          spreadRadius: 0

                      ) ],
                    ),child: Image.asset(
                    "assets/icons/g.png",
                    fit: BoxFit.cover,
                  ),
                  )),
                  ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(10)),child:
                  new Container(
                    width: 80,
                    height: 100,
                    decoration: new BoxDecoration(
                      color: Color(0xffffffff),
                      boxShadow: [BoxShadow(
                          color: Color(0x24000000),
                          offset: Offset(0,8),
                          blurRadius: 10,
                          spreadRadius: 0

                      ) ],
                    ),child: Image.asset(
                    "assets/icons/applepay.png",
                    fit: BoxFit.cover,
                  ),
                  ))
                ],
              ),
              SizedBox(height: 20,),
              new Container(
                  width: 328,
                  height: 72,
                  decoration: new BoxDecoration(
                      color: Color(0xffdb0011),
                      borderRadius: BorderRadius.circular(10)
                  ),
                child: Center(
                  child: new Text("تحويل",
                      textAlign: TextAlign.center,
                      textScaleFactor: 1.0,
                      style: TextStyle(
                        fontFamily: Fonts.fontfamily,
                        color: Color(0xffffffff),
                        fontSize: Fonts.fontSize_large,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,


                      )
                  ),
                ),
              ),
            ]);
  }
}














// import 'package:aytamy/common/fonts.dart';
// import 'package:aytamy/screens/payment/payment_history_view.dart';
// import 'package:aytamy/screens/profile/case_data/case_data_slide_dialog_view.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/rendering.dart';
//
// class PaymentScreen extends StatefulWidget {
//   @override
//   _PaymentScreenState createState() => _PaymentScreenState();
// }
//
// class _PaymentScreenState extends State<PaymentScreen> {
//   @override
//   Widget build(BuildContext context) {
//     return SafeArea(
//         child: SingleChildScrollView(
//           child: Padding(
//       padding: const EdgeInsets.all(8.0),
//       child:
//       Column(children: [
//       Container(
//             height: MediaQuery.of(context).size.height/2,
//             width: MediaQuery.of(context).size.width,
//             child: Column(children: [
//               Row(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: [
//                   Image.asset(
//                     "assets/icons/refresh.png",
//                     height: 40,
//                     width: 40,
//                   ),
//                   Column(
//                     children: [
//                       new Text("قيمة الكفالة الدورية",
//                           textScaleFactor: 1.0,
//                           style: TextStyle(
//                             fontFamily: Fonts.fontfamily,
//                             color: Color(0xff4f4f4f),
//                             fontSize: Fonts.fontSize_meduim,
//                             fontWeight: FontWeight.w700,
//                             fontStyle: FontStyle.normal,
//                           )),
//                       new Text("\$500",
//                           textScaleFactor: 1.0,
//                           style: TextStyle(
//                             fontFamily: 'Arial',
//                             color: Color(0xff4f4f4f),
//                             fontSize: Fonts.fontSize_large,
//                             fontWeight: FontWeight.w700,
//                             fontStyle: FontStyle.normal,
//                           )),
//                       Row(
//                         children: [
//                           new Text("الدفع غير متاح",
//                               style: TextStyle(
//                                 fontFamily: Fonts.fontfamily,
//                                 color: Color(0xffdb0011),
//                                 fontSize: Fonts.fontSize_small,
//                                 fontWeight: FontWeight.w700,
//                                 fontStyle: FontStyle.normal,
//                               )),
//                           Image.asset(
//                             "assets/icons/pay.png",
//                             height: 15,
//                             width: 15,
//                             color: Color(0xffdb0011),
//                           ),
//                         ],
//                       )
//                     ],
//                   )
//                 ],
//               ),
//               SizedBox(height: 20,),
//               Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceAround,
//                 children: [
//                   ClipRRect(
//                     borderRadius: BorderRadius.all(Radius.circular(10)),child:
//                   new Container(
//                       width: 80,
//                       height: 100,
//                       decoration: new BoxDecoration(
//                         color: Color(0xffffffff),
//                         boxShadow: [BoxShadow(
//                             color: Color(0x24000000),
//                             offset: Offset(0,8),
//                             blurRadius: 10,
//                             spreadRadius: 0
//
//                         ) ],
//                       ),child: Image.asset(
//                           "assets/icons/applepay.png",
//                    fit: BoxFit.cover,
//                         ),
//                       )),
//                   ClipRRect(
//                       borderRadius: BorderRadius.all(Radius.circular(10)),child:
//                   new Container(
//                     width: 80,
//                     height: 100,
//                     decoration: new BoxDecoration(
//                       color: Color(0xffffffff),
//                       boxShadow: [BoxShadow(
//                           color: Color(0x24000000),
//                           offset: Offset(0,8),
//                           blurRadius: 10,
//                           spreadRadius: 0
//
//                       ) ],
//                     ),child: Image.asset(
//                     "assets/icons/applepay.png",
//                     fit: BoxFit.cover,
//                   ),
//                   )),
//                   ClipRRect(
//                       borderRadius: BorderRadius.all(Radius.circular(10)),child:
//                   new Container(
//                     width: 80,
//                     height: 100,
//                     decoration: new BoxDecoration(
//                       color: Color(0xffffffff),
//                       boxShadow: [BoxShadow(
//                           color: Color(0x24000000),
//                           offset: Offset(0,8),
//                           blurRadius: 10,
//                           spreadRadius: 0
//
//                       ) ],
//                     ),child: Image.asset(
//                     "assets/icons/applepay.png",
//                     fit: BoxFit.cover,
//                   ),
//                   )),
//                   ClipRRect(
//                       borderRadius: BorderRadius.all(Radius.circular(10)),child:
//                   new Container(
//                     width: 80,
//                     height: 100,
//                     decoration: new BoxDecoration(
//                       color: Color(0xffffffff),
//                       boxShadow: [BoxShadow(
//                           color: Color(0x24000000),
//                           offset: Offset(0,8),
//                           blurRadius: 10,
//                           spreadRadius: 0
//
//                       ) ],
//                     ),child: Image.asset(
//                     "assets/icons/applepay.png",
//                     fit: BoxFit.cover,
//                   ),
//                   ))
//                 ],
//               ),
//               SizedBox(height: 20,),
//               new Container(
//                   width: 328,
//                   height: 72,
//                   decoration: new BoxDecoration(
//                       color: Color(0xffdb0011),
//                       borderRadius: BorderRadius.circular(10)
//                   ),
//                 child: Center(
//                   child: new Text("تحويل",
//                       textAlign: TextAlign.center,
//                       textScaleFactor: 1.0,
//                       style: TextStyle(
//                         fontFamily: Fonts.fontfamily,
//                         color: Color(0xffffffff),
//                         fontSize: Fonts.fontSize_large,
//                         fontWeight: FontWeight.w700,
//                         fontStyle: FontStyle.normal,
//
//
//                       )
//                   ),
//                 ),
//               ),
//             ])),
//
//           Container(
//               width: MediaQuery.of(context).size.width,
//               height: MediaQuery.of(context).size.height/2,
//               child:    DraggablePaymentListView()),
//       ])
//     ),
//         ));
//   }
// }
