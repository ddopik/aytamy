import 'package:aytamy/common/fonts.dart';
import 'package:aytamy/common/model/User.dart';
import 'package:aytamy/generated/l10n.dart';
import 'package:aytamy/screens/profile/case_data/grid_list_view_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class DraggablePaymentListView extends StatefulWidget {
  const DraggablePaymentListView({
    Key key,
  }) : super(key: key);

  @override
  __DraggablePaymentListViewState createState() =>
      __DraggablePaymentListViewState();
}

class __DraggablePaymentListViewState extends State<DraggablePaymentListView> {
  __DraggablePaymentListViewState();

  @override
  Widget build(BuildContext context) {
    return DraggableScrollableActuator(
      child: Stack(
        children: <Widget>[
          DraggableScrollableSheet(
            initialChildSize: .5,
            minChildSize: 0.50,
            maxChildSize: 1.0,
            builder: (BuildContext context, ScrollController scrollController) {
              return Container(
                  width: 360,
                  height: 232,
                  decoration: new BoxDecoration(
                    color: Color(0xfff5f5f5),
                    boxShadow: [
                      BoxShadow(
                          color: Color(0x29000000),
                          offset: Offset(1, -3),
                          blurRadius: 6,
                          spreadRadius: 0)
                    ],
                  ),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: new Container(
                              width: 30,
                              height: 8,
                              decoration: new BoxDecoration(
                                  color: Color(0xff9e9e9e),
                                  borderRadius: BorderRadius.circular(100))),
                        ),
                        Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              new Text("اخر حوالات",
                                  textScaleFactor: 1.0,
                                  style: TextStyle(
                                    fontFamily: 'GESSTwo',
                                    color: Color(0xffdb0011),
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                    fontStyle: FontStyle.normal,
                                  )),
                              ListView.builder(
                                shrinkWrap: true,
                                controller: scrollController,
                                itemCount: 3,
                                itemBuilder:
                                    (BuildContext context, int index) {
                                  return getViewOne(context);
                                },
                              ),
                            ])
                      ]));
            },
          ),
        ],
      ),
    );
  }
}

getViewOne(BuildContext context) {
  return Container(
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  new Text("\$500",
                      textScaleFactor: 1.0,
                      style: TextStyle(
                        fontFamily: 'Arial',
                        color: Color(0xffdb0916),
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                      )),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.35,
                  ),
                  new Text("محمد مسعود",
                      textScaleFactor: 1.0,
                      style: TextStyle(
                        fontFamily: 'GESSTwo',
                        color: Color(0xff000000),
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        fontStyle: FontStyle.normal,
                      )),
                ],
              ),
              new Text("رفضت عملية التحويل بتاريخ 2020/11/17م الساعة 9:30",
                  textScaleFactor: 1.0,
                  style: TextStyle(
                    fontFamily: 'GESSTwo',
                    color: Color(0xff000000),
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                  ))
            ],
          ),
          Image.asset(
            "assets/icons/applepay.png",
            fit: BoxFit.cover,
          ),
        ],
      ));
}
