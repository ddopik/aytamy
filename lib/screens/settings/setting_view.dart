import 'package:aytamy/common/fonts.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SettingViewItem extends StatelessWidget {
  final String fieldValue;
  final String fieldImagePath;
  final Function onClick;
  const SettingViewItem(
      {Key key, this.fieldValue, this.fieldImagePath, this.onClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onClick,
      child: Padding(
        padding: const EdgeInsets.only(top: 5,bottom: 5),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(60),
                        child: new Image.asset(
                          fieldImagePath,
                          height: 35,
                          width: 30,
                          color: Color(0xff4f4f4f),
                        ),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      new Text(fieldValue,
                          textScaleFactor: 1.0,
                          style: TextStyle(
                            fontFamily: Fonts.fontfamily,
                            color: Color(0xff4f4f4f),
                            fontSize: Fonts.fontSize_small,
                            fontWeight: FontWeight.w500,
                            fontStyle: FontStyle.normal,
                          )),
                    ]),
                new Icon(Icons.arrow_forward_ios,
                    size: 25, color: Color(0xff4f4f4f)),
              ],
            ),
            Divider(thickness: 2,indent:MediaQuery.of(context).size.width*0.1,)
          ],
        ),
      ),
    );
  }
}
