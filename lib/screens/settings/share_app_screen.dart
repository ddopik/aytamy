import 'package:aytamy/app/app_model.dart';
import 'package:aytamy/app/route.dart';
import 'package:aytamy/common/fonts.dart';
import 'package:aytamy/generated/l10n.dart';
import 'package:aytamy/screens/settings/setting_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:social_share_plugin/social_share_plugin.dart';

class ShareAppScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ShareAppScreenState();
  }
}

class _ShareAppScreenState extends State<ShareAppScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        centerTitle: false,
        title: Text(S.current.setting,
            textScaleFactor: 1.0,
            style: const TextStyle(
                color: const Color(0xffffffff),
                fontWeight: FontWeight.w700,
                fontFamily: Fonts.fontfamily,
                fontStyle: FontStyle.normal,
                fontSize: Fonts.fontSize_small),
            textAlign: TextAlign.center),
        actions: [
          IconButton(
              icon: Icon(Icons.arrow_forward_ios_outlined),
              onPressed: () {
                Navigator.of(context).pop();
              })
        ],
        leading: Container(),
      ),
      body: Container(
          margin: EdgeInsets.only(top: 2.0),
          alignment: Alignment.topCenter,
          child: renderMainView()),
    );
  }

  Widget renderMainView() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [

          SettingViewItem(
            fieldValue: "FaceBook",
            fieldImagePath: "assets/icons/facebook.png",
            onClick: (){
               SocialSharePlugin.shareToFeedFacebookLink(url: "www.Aytam.com");
            },
          ),
          SettingViewItem(
            fieldValue: "Twitter",
            fieldImagePath: "assets/icons/lang.png",onClick: (){
            SocialSharePlugin.shareToTwitterLink(url: "www.Aytam.com");
          },
          ),
          SettingViewItem(
            fieldValue: "Instagram",
            fieldImagePath: "assets/icons/lang.png",onClick: (){
            SocialSharePlugin.shareToFeedInstagram(path: "assets/icons/home.png");
          },
          ),

        ],
      ),
    );
  }


}
