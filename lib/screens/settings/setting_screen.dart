import 'dart:io';

import 'package:aytamy/app/app_model.dart';
import 'package:aytamy/app/route.dart';
import 'package:aytamy/common/config.dart';
import 'package:aytamy/common/fonts.dart';
import 'package:aytamy/common/stats_widgets.dart';
import 'package:aytamy/generated/l10n.dart';
import 'package:aytamy/screens/home/home_navigation_drawer.dart';
import 'package:aytamy/screens/settings/setting_view.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class SettingScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SettingScreenState();
  }
}

class _SettingScreenState extends State<SettingScreen> {
  CurrentHomeSelection _currentDrawerSelection;
  final _app = AppModel();
  Languages selectedLanguage;

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      setState(() {
        _currentDrawerSelection =
            ModalRoute.of(context).settings.arguments as CurrentHomeSelection;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        centerTitle: false,
        title:
            Text(S.current.main,
                textScaleFactor: 1.0,
                style: const TextStyle(
                    color: const Color(0xffffffff),
                    fontWeight: FontWeight.w700,
                    fontFamily: Fonts.fontfamily,
                    fontStyle: FontStyle.normal,
                    fontSize: Fonts.fontSize_small),
                textAlign: TextAlign.center),

          actions: [
            IconButton(
                icon: Icon(Icons.arrow_forward_ios_outlined),
                onPressed: () {
                  Navigator.of(context).pop();
                })

        ],
        leading: Container(),
      ),
      drawer: HomeNavigationDrawer(
        onNavigateClick: (selectedNav) {
          setState(() {
            _currentDrawerSelection = selectedNav;
          });
        },
      ),
      body: Container(
          margin: EdgeInsets.only(top: 2.0),
          alignment: Alignment.topCenter,
          child: renderMainView()),
    );
  }

  showMyAlertDialog(BuildContext context) {
    var Arabic = Languages("Arabic", "Arabic");
    var English = Languages("English", "English");

    // Create SimpleDialog
    SimpleDialog dialog = SimpleDialog(
      title: Text(S.current.selectLang),
      children: <Widget>[
        SimpleDialogOption(
            onPressed: () {
              Provider.of<AppModel>(context, listen: false)
                  .changeLanguage("ar", context);
              Navigator.pop(context, Arabic);
            },
            child: Text(Arabic.Arabic)),
        SimpleDialogOption(
          onPressed: () {
            Provider.of<AppModel>(context, listen: false)
                .changeLanguage("en", context);
            Navigator.pop(context, English);
          },
          child: Text(English.English),
        )
      ],
    );

    // Call showDialog function to show dialog.
    Future<Languages> futureValue = showDialog(
        context: context,
        builder: (BuildContext context) {
          return dialog;
        });

    futureValue.then((language) => {
          this.setState(() {
            this.selectedLanguage = language;
          })
        });
  }

  Widget renderMainView() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          _app.getUserName() !=null &&  _app.getUserName() !=''?    InkWell(
            onTap: () {
              Navigator.of(context).pushNamed(Routes.PROFILE_SCREEN);
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(60),
                        child: (_app.getUserProfileImage() != null &&
                            _app.getUserProfileImage() != "null")  ||_app.getUserProfileImage().contains("http")
                            ? CachedNetworkImage(
                            imageUrl:_app.getUserProfileImage().contains("http")?_app.getUserProfileImage().toString(): IMAGE_BASE_URL +
                                _app.getUserProfileImage().toString(),
                            height: 40)
                            : new Image.asset(
                          "assets/images/img_profile.jpeg",
                          height: 40,
                        ),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          new Text(
                              Provider.of<AppModel>(context, listen: false)
                                  .getUserName(),
                              textScaleFactor: 1.0,
                              style: TextStyle(
                                fontFamily: Fonts.fontfamily,
                                color: Color(0xffdb0011),
                                fontSize: Fonts.fontSize_small,
                                fontWeight: FontWeight.w500,
                                fontStyle: FontStyle.normal,
                              )),
                          new Text(
                              Provider.of<AppModel>(context, listen: false)
                                  .getUserMail(),
                              textScaleFactor: 1.0,
                              style: TextStyle(
                                fontFamily: Fonts.fontfamily,
                                color: Color(0xff000000),
                                fontSize: Fonts.fontSize_small,
                                fontWeight: FontWeight.w500,
                                fontStyle: FontStyle.normal,
                              ))
                        ],
                      ),
                    ]),
                InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: new Icon(Icons.arrow_forward_ios,
                      size: 25, color: Color(0xff4f4f4f)),
                ),
              ],
            ),
          ):Container(),
          Divider(
            thickness: 2,
          ),
          SettingViewItem(
            fieldValue: S.current.SwitchLanguage,
            fieldImagePath: "assets/icons/lang.png",
            onClick: () {
              showMyAlertDialog(context);
            },
          ),
          SettingViewItem(
            fieldValue: S.current.methods,
            fieldImagePath: "assets/icons/wallet.png",
          ),
          SettingViewItem(
            fieldValue: S.current.share,
            fieldImagePath: "assets/icons/shar.png",
            onClick: () {
              Navigator.of(context).pushNamed(Routes.SHARE_APP);
            },
          ),
          SettingViewItem(
            fieldValue: S.current.Inquiries,
            fieldImagePath: "assets/icons/Help.png",
            onClick: () {
              launchWhatsApp(phone: "+2001125362598", message: "");
              // Navigator.of(context).pushNamed(Routes.PRIVACY_POLICY);
            },
          )
        ],
      ),
    );
  }

  void launchWhatsApp({
    @required String phone,
    @required String message,
  }) async {
    String url() {
      if (Platform.isIOS) {
        return "whatsapp://wa.me/$phone/?text=${Uri.parse(message)}";
      } else {
        return "whatsapp://send?phone=$phone&text=${Uri.parse(message)}";
      }
    }

    if (await canLaunch(url())) {
      await launch(url());
    } else {
      showInfo("Please Check WhatsApp Installation");
      throw 'Could not launch ${url()}';
    }
  }
}

class Languages {
  String Arabic;
  String English;

  Languages(this.Arabic, this.English);
}
