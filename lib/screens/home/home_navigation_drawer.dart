import 'package:aytamy/app/app_model.dart';
import 'package:aytamy/app/route.dart';
import 'package:aytamy/common/fonts.dart';
import 'package:aytamy/common/widgets/custom_image_loader.dart';
import 'package:aytamy/generated/l10n.dart';
import 'package:aytamy/screens/home/provider/drawer_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomeNavigationDrawer extends StatefulWidget {
  final Function onNavigateClick;

  HomeNavigationDrawer({this.onNavigateClick});

  @override
  State<StatefulWidget> createState() {
    return _HomeNavigationDrawer();
  }
}

DrawerModel _drawerModel = DrawerModel();

class _HomeNavigationDrawer extends State<HomeNavigationDrawer> {
  CurrentHomeSelection _currentHomeSelection;
  final _app = AppModel();

  @override
  void initState() {
    _currentHomeSelection = CurrentHomeSelection.HOME;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: Color(0xffe4e4e4),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height * .4,
              width: MediaQuery.of(context).size.width,
              color: Color(0xffffffff),
              child: createDrawerHeader(),
            ),
            Divider(
              color: Color(0xffe4e4e4),
            ),
            Expanded(
              child: Container(
                color: Colors.white,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(children: [
                    Container(
                      color: _drawerModel.NewOrphenselectedColor,
                      child: GestureDetector(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: 24,
                            ),
                            Icon(
                              Icons.add_circle,
                              color: _drawerModel.NewOrphenselectedColor !=
                                      Colors.white
                                  ? Colors.white
                                  : Color(0xff4f4f4f),
                            ),
                            Container(
                              margin: EdgeInsets.all(14.0),
                              child: // يتيم جديد
                                  Text(
                                S.current.NewOrphan,
                                textScaleFactor: 1.0,
                                style: TextStyle(
                                    color:
                                        _drawerModel.NewOrphenselectedColor !=
                                                Colors.white
                                            ? Colors.white
                                            : Color(0xff4f4f4f),
                                    fontWeight: FontWeight.w500,
                                    fontFamily: Fonts.fontfamily,
                                    fontStyle: FontStyle.normal,
                                    fontSize: Fonts.fontSize_small),
                              ),
                            )
                          ],
                        ),
                        onTap: () {
                          _drawerModel.SelectNewOrphen();
                          widget
                              .onNavigateClick(CurrentHomeSelection.NEW_ORPHEN);
                          Navigator.pop(context);
                        },
                      ),
                    ),
                    new Container(
                        width: MediaQuery.of(context).size.width,
                        height: 2,
                        decoration: new BoxDecoration(
                            color: _drawerModel.NewOrphenselectedColor !=
                                Colors.white?Color(0xffdb0011): Colors.white
                        )
                    )]),
                    Column(children: [
                    Container(
                      color: _drawerModel.lastTurnsselectedColor,
                      child: GestureDetector(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: 24,
                            ),
                            ImageIcon(
                              AssetImage("assets/icons/turns.png"),
                              color: _drawerModel.lastTurnsselectedColor !=
                                      Colors.white
                                  ? Colors.white
                                  : Color(0xff4f4f4f),
                            ),
                            Container(
                              margin: EdgeInsets.all(14.0),
                              child: Text(
                                S.current.latestTransaction,
                                textScaleFactor: 1.0,
                                style: TextStyle(
                                    color:
                                        _drawerModel.lastTurnsselectedColor !=
                                                Colors.white
                                            ? Colors.white
                                            : Color(0xff4f4f4f),
                                    fontWeight: FontWeight.w500,
                                    fontFamily: Fonts.fontfamily,
                                    fontStyle: FontStyle.normal,
                                    fontSize: Fonts.fontSize_small),
                              ),
                            )
                          ],
                        ),
                        onTap: () {
                          _drawerModel.SelectlastTurns();
                          widget.onNavigateClick(CurrentHomeSelection.TRANSACTION);
                          Navigator.pop(context);
                        },
                      ),
                    ),
                    new Container(
                        width: MediaQuery.of(context).size.width,
                        height: 2,
                        decoration: new BoxDecoration(
                            color: _drawerModel.lastTurnsselectedColor !=
                                Colors.white?Color(0xffdb0011): Colors.white
                        )
                    )]),
                    Column(children: [
                    Container(
                      color: _drawerModel.HomedselectedColor,
                      child: GestureDetector(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: 24,
                            ),
                            Icon(
                              Icons.home,
                              color: _drawerModel.HomedselectedColor !=
                                      Colors.white
                                  ? Colors.white
                                  : Color(0xff4f4f4f),
                            ),
                            Container(
                              margin: EdgeInsets.all(14.0),
                              child: Text(
                                S.current.main,
                                textScaleFactor: 1.0,
                                style: TextStyle(
                                    color: _drawerModel.HomedselectedColor !=
                                            Colors.white
                                        ? Colors.white
                                        : Color(0xff4f4f4f),
                                    fontWeight: FontWeight.w500,
                                    fontFamily: Fonts.fontfamily,
                                    fontStyle: FontStyle.normal,
                                    fontSize: Fonts.fontSize_small),
                              ),
                            )
                          ],
                        ),
                        onTap: () {
                          _drawerModel.SelectHome();
                          widget.onNavigateClick(CurrentHomeSelection.HOME);
                          Navigator.pop(context);
                        },
                      ),
                    ),
                    new Container(
                        width: MediaQuery.of(context).size.width,
                        height: 2,
                        decoration: new BoxDecoration(
                            color: _drawerModel.HomedselectedColor !=
                                Colors.white?Color(0xffdb0011): Colors.white
                        )
                    )]),
                    Column(children: [
                    Container(
                      color: _drawerModel.NotificationselectedColor,
                      child: GestureDetector(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: 24,
                            ),
                            Icon(
                              Icons.notifications_active,
                              color: _drawerModel.NotificationselectedColor !=
                                      Colors.white
                                  ? Colors.white
                                  : Color(0xff4f4f4f),
                            ),
                            Container(
                              margin: EdgeInsets.all(14.0),
                              child: Text(
                                S.current.notification,
                                textScaleFactor: 1.0,
                                style: TextStyle(
                                    color: _drawerModel
                                                .NotificationselectedColor !=
                                            Colors.white
                                        ? Colors.white
                                        : Color(0xff4f4f4f),
                                    fontWeight: FontWeight.w500,
                                    fontFamily: Fonts.fontfamily,
                                    fontStyle: FontStyle.normal,
                                    fontSize: Fonts.fontSize_small),
                              ),
                            )
                          ],
                        ),
                        onTap: () {
                          _drawerModel.SelectNotification();
                          widget.onNavigateClick(
                              CurrentHomeSelection.NOTIFICATION);
                          Navigator.pop(context);
                        },
                      ),
                    ),
                    new Container(
                        width: MediaQuery.of(context).size.width,
                        height: 2,
                        decoration: new BoxDecoration(
                            color: _drawerModel.NotificationselectedColor !=
                                Colors.white?Color(0xffdb0011): Colors.white
                        )
                    )]),
                    Column(children: [
                    Container(
                      color: _drawerModel.PrioritiesselectedColor,
                      child: GestureDetector(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: 24,
                            ),
                            Icon(
                              Icons.favorite_outlined,
                              color: _drawerModel.PrioritiesselectedColor !=
                                      Colors.white
                                  ? Colors.white
                                  : Color(0xff4f4f4f),
                            ),
                            Container(
                              margin: EdgeInsets.all(14.0),
                              child: Text(
                                S.current.myPriority,
                                textScaleFactor: 1.0,
                                style: TextStyle(
                                    color:
                                        _drawerModel.PrioritiesselectedColor !=
                                                Colors.white
                                            ? Colors.white
                                            : Color(0xff4f4f4f),
                                    fontWeight: FontWeight.w500,
                                    fontFamily: Fonts.fontfamily,
                                    fontStyle: FontStyle.normal,
                                    fontSize: Fonts.fontSize_small),
                              ),
                            )
                          ],
                        ),
                        onTap: () {
                          _drawerModel.SelectPriorities();
                          widget.onNavigateClick(CurrentHomeSelection.LIKES);
                          Navigator.pop(context);
                        },
                      ),
                    ),
                      new Container(
                          width: MediaQuery.of(context).size.width,
                          height: 2,
                          decoration: new BoxDecoration(
                              color: _drawerModel.PrioritiesselectedColor !=
                                  Colors.white?Color(0xffdb0011): Colors.white
                          )
                      )
                    ],)
                  ],
                ),
              ),
            ),
            Divider(
              color: Color(0xffe4e4e4),
            ),
            Container(
              color: Color(0xffffffff),
              child: GestureDetector(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: 24,
                    ),
                    new Image.asset(
                      "assets/icons/sign-out-option.png",
                      color: Color(0xff4f4f4f),
                      height: 19,
                    ),
                    Container(
                      margin: EdgeInsets.all(24.0),
                      child: // يتيم جديد
                          Text(
                        S.current.signOut,
                        textScaleFactor: 1.0,
                        style: const TextStyle(
                            color: const Color(0xff4f4f4f),
                            fontWeight: FontWeight.w500,
                            fontFamily: Fonts.fontfamily,
                            fontStyle: FontStyle.normal,
                            fontSize: Fonts.fontSize_small),
                      ),
                    )
                  ],
                ),
                onTap: () {
                  _app.logOutUser();
                  Navigator.pop(context);
                  Navigator.of(context).pushNamed(Routes.INTO_SCREEN);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget createDrawerHeader() {
    return DrawerHeader(
      margin: EdgeInsets.zero,
      padding: EdgeInsets.zero,
      child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
                margin: EdgeInsets.only(top: 15),
                padding: EdgeInsets.symmetric(horizontal: 24),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: new Image.asset(
                        "assets/icons/cancel.png",
                        color: Colors.red,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(60),
                          child: (Provider.of<AppModel>(context,listen: false)
                                          .getUserProfileImage() !=
                                      '' &&
                                  Provider.of<AppModel>(context)
                                          .getUserProfileImage() !=
                                      'null')
                              ? CustomImageLoader.image(
                                  url: Provider.of<AppModel>(context)
                                      .getUserProfileImage(),
                              width: 60,
                              height: 60,
                                  fit: BoxFit.fill)
                              : Image(
                              width: 60,
                              height: 60,
                                  image: AssetImage(
                                      "assets/images/img_profile.jpeg"),
                                  fit: BoxFit.fill),
                        ),
                        InkWell(
                            onTap: () {
                              if(  Provider.of<AppModel>(context, listen: false)
                                  .getUserMail()!=null) {
                                Navigator.of(context)
                                    .pushNamed(Routes.SETTING_SCREEN);
                              }
                            },
                            child: Icon(Icons.edit, color: Color(0xff4f4f4f))),
                      ],
                    )
                  ],
                )),
            Flexible(
                child: (Provider.of<AppModel>(context,listen: false).getUserName().isNotEmpty)
                    ? Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Flexible(
                            child: Container(
                              padding: EdgeInsets.symmetric(horizontal: 24),
                              child: Text(
                                Provider.of<AppModel>(context,listen: false).getUserName()==null?S.current.aytamUser:Provider.of<AppModel>(context,listen: false).getUserName().toString(),
                                textScaleFactor: 1.0,
                                // Provider.of<AppModel>(context).getUserName(),
                                style: TextStyle(
                                    color: const Color(0xff4f4f4f),
                                    fontWeight: FontWeight.w700,
                                    fontFamily: Fonts.fontfamily,
                                    fontStyle: FontStyle.normal,
                                    fontSize: Fonts.fontSize_small),
                              ),
                            ),
                          ),
                          // Rectangle 85
                          SizedBox(
                            height: 10.0,
                          ),
                          Flexible(
                            child: Container(
                              padding: EdgeInsets.symmetric(horizontal: 24),
                              child: Opacity(
                                opacity: 0.4000000059604645,
                                child: Text(Provider.of<AppModel>(context,listen: false).getUserMail()==null?"mahmoud@AYTAM.COM":Provider.of<AppModel>(context,listen: false).getUserMail().toString(),
                                    textScaleFactor: 1.0,
                                    style: const TextStyle(
                                        color: const Color(0xff4f4f4f),
                                        fontWeight: FontWeight.w400,
                                        fontFamily: Fonts.fontfamily,
                                        fontStyle: FontStyle.normal,
                                        fontSize: Fonts.fontSize_small),
                                    textAlign: TextAlign.left),
                              ),
                            ),
                          ),
                        ],
                      )
                    : Container()),
            SizedBox(
              width: 8.0,
            ),
          ]),
    );
  }
}

enum CurrentHomeSelection {
  HOME,
  NOTIFICATION,
  PROFILE,
  LIKES,
  NEW_ORPHEN,
  TRANSACTION
}
