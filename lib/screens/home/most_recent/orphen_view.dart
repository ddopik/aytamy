import 'dart:io';

import 'package:aytamy/common/fonts.dart';
import 'package:aytamy/common/model/User.dart';
import 'package:aytamy/common/widgets/CustomDialog.dart';
import 'package:aytamy/generated/l10n.dart';
import 'package:aytamy/screens/home/dialog_view.dart';
import 'package:aytamy/screens/profile/case_data/case_data_slide_dialog_view.dart';
import 'package:aytamy/storage/pref_manager.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:aytamy/screens/profile/case_data/grid_list_view_item.dart';
import 'package:url_launcher/url_launcher.dart';

class OrphenView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _OrphenViewState();
  }
}

class _OrphenViewState extends State<OrphenView> {
  User user;

  @override
  void initState() {
    super.initState();
  }
  _launchCaller() async {
    var url = "tel:"+ PrefManager().getUserPhone().toString()??010;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    user = ModalRoute.of(context).settings.arguments as User;
    return SafeArea(
      child: ListView( children: [
        Padding(
          padding: const EdgeInsets.only(top: 80.0),
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(60),
                                child: new Image.asset(
                                  "assets/images/img_profile.jpeg",
                                  height: 30,
                                ),
                              )),
                          Column(
                            children: [
                              new Text(user.name,
                                  textScaleFactor: 1.0,
                                  style: TextStyle(
                                    fontFamily: Fonts.fontfamily,
                                    color: Color(0xffdb0011),
                                    fontSize: Fonts.fontSize_small,
                                    fontWeight: FontWeight.w500,
                                    fontStyle: FontStyle.normal,
                                  )),
                              new Text("مصري، 10 سنه",
                                  textScaleFactor: 1.0,
                                  style: TextStyle(
                                    fontFamily: Fonts.fontfamily,
                                    color: Color(0xff4f4f4f),
                                    fontSize: Fonts.fontSize_small,
                                    fontWeight: FontWeight.w500,
                                    fontStyle: FontStyle.normal,
                                  ))
                            ],
                          ),
                        ],
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: new Icon(Icons.arrow_forward_ios,
                            size: 22, color: Color(0xff4f4f4f)),
                      ),
                    ],
                  ),
                  Stack(children: [
                    new Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height * .45,
                        decoration: new BoxDecoration(color: Color(0xff4f4f4f))),
                    Column(children: [
                      Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: new Container(
                          width: MediaQuery.of(context).size.width,
                          height: 136,
                          decoration: new BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(color: Colors.red, width: 1.7),
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0x29000000),
                                  offset: Offset(1, 3),
                                  blurRadius: 6,
                                  spreadRadius: 0)
                            ],
                          ),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: new Image.asset(
                              "assets/images/charracter.png",
                              height: 30,
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all( 15.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            InkWell(
                              onTap: (){
                                launchWhatsApp(phone:"+20"+user?.parentMobile.toString(),message: "");
                              }
                              ,child: new Container(
                              width: 72,
                              height: 72,
                              decoration: new BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10)),
                              child: Column(
                                children: [
                                   Flexible(
                                    child: new Text(
                                     S.current.whatsapp,
                                      textScaleFactor: 1.0,
                                      style: TextStyle(
                                        fontFamily: Fonts.fontfamily,
                                          color: Color(0xff4f4f4f),
                                          fontSize: Fonts.fontSize_small,
                                          fontWeight: FontWeight.w700,
                                          fontStyle: FontStyle.normal,
                                        ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                  SizedBox(height: 7,),
                                  new Image.asset(
                                    "assets/icons/whatsapp.png",
                                    color: Colors.red,
                                    width: 18,
                                    height: 21,
                                  ),
                                ],
                              ),
                            ),
                            ),
                            InkWell(
                              onTap: (){
                                _launchCaller();
                              }
                              ,child: new Container(
                                width: 72,
                                height: 72,
                                decoration: new BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10)),
                                child: Column(
                                  children: [
                                    Flexible(
                                      child: new Text(
                                        S.current.phoneCall,
                                        textScaleFactor: 1.0,
                                        style: TextStyle(
                                          fontFamily: Fonts.fontfamily,
                                          color: Color(0xff4f4f4f),
                                          fontSize: Fonts.fontSize_small,
                                          fontWeight: FontWeight.w700,
                                          fontStyle: FontStyle.normal,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    SizedBox(height: 7,),
                                    new Image.asset(
                                      "assets/icons/phone.png",
                                      color: Colors.red,
                                      width: 18,
                                      height: 21,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            new Container(
                              width: 72,
                              height: 72,
                              decoration: new BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10)),
                              child: Column(
                                children: [
                                  Flexible(
                                    child: new Text(
                                      S.current.orphenReport,
                                      textScaleFactor: 1.0,
                                      style: TextStyle(
                                        fontFamily: Fonts.fontfamily,
                                        color: Color(0xff4f4f4f),
                                        fontSize: Fonts.fontSize_small,
                                        fontWeight: FontWeight.w700,
                                        fontStyle: FontStyle.normal,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                  SizedBox(height: 5,),
                                  new Image.asset(
                                    "assets/icons/find.png",
                                    color: Colors.red,
                                    width: 18,
                                    height: 23,
                                  ),
                                ],
                              ),
                            ),
                            new Container(
                              width: 72,
                              height: 72,
                              decoration: new BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10)),
                              child: Column(
                                children: [
                                  Flexible(
                                    child: new Text(
                                      S.current.guaranteesNumber,
                                      textScaleFactor: 1.0,
                                      style: TextStyle(
                                        fontFamily: Fonts.fontfamily,
                                        color: Color(0xff4f4f4f),
                                        fontSize: Fonts.fontSize_small,
                                        fontWeight: FontWeight.w700,
                                        fontStyle: FontStyle.normal,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                  SizedBox(height: 4,),
                                  new Text("+1",
                                      textScaleFactor: 1.0,
                                      style: TextStyle(
                                        fontFamily: Fonts.fontfamily,
                                        color: Color(0xffdb0011),
                                        fontSize: 17,
                                        fontWeight: FontWeight.w700,
                                        fontStyle: FontStyle.normal,

                                      )
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      )
                    ])
                  ]),

                  Flexible(
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      child:    DraggableListView(user: user,))),

                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: GestureDetector(
                      child: new Container(
                          width: 328,
                          height: 48,
                          decoration: new BoxDecoration(
                              color: Color(0xffdb0011),
                              borderRadius: BorderRadius.circular(10)
                          ),child: Center(
                            child: new Text(S.current.donateButton,
                                textScaleFactor: 1.0,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontFamily: Fonts.fontfamily,
                                color: Color(0xffffffff),
                                fontSize: Fonts.fontSize_small,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                              )
                      ),
                          ),
                      ),
                      onTap: () {
                        addPayDialogView(context: context);
                      },
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ]),
    );
  }

  void launchWhatsApp(
      {@required String phone,
        @required String message,
      }) async {
    String url() {
      if (Platform.isIOS) {
        return "whatsapp://wa.me/$phone/?text=${Uri.parse(message)}";
      } else {
        return "whatsapp://send?phone=$phone&text=${Uri.parse(message)}";
      }
    }

    if (await canLaunch(url())) {
      await launch(url());
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) =>
            CustomDialog(
              title: "Aytam",
              description:
              "Please Check WhatsApp Installation",
              buttonText: "Exit",
            ),
      );
      throw 'Could not launch ${url()}';
    }
  }
}
