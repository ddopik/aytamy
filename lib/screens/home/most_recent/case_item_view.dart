import 'package:auto_size_text/auto_size_text.dart';
import 'package:aytamy/app/route.dart';
import 'package:aytamy/common/colors.dart';
import 'package:aytamy/common/config.dart';
import 'package:aytamy/common/fonts.dart';
import 'package:aytamy/common/stats_widgets.dart';
import 'package:aytamy/generated/l10n.dart';
import 'package:aytamy/screens/home/provider/home_model.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CaseItemView extends StatefulWidget {
  int index;

  CaseItemView({this.index});

  @override
  State<StatefulWidget> createState() {
    return _CaseItemViewState();
  }
}

class _CaseItemViewState extends State<CaseItemView>
    with TickerProviderStateMixin {
  bool state = false;
  HomeModel homelModel;
  Color AddOrphenIconColor = Color(0xff9e9e9e);
  double scale = 3.3;
  bool shouldScaleDown = false; //
  @override
  void initState() {
    homelModel = Provider.of<HomeModel>(context, listen: false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Provider.of<HomeModel>(context).currentSelectedRecentUser ==
              homelModel.mostRecentUsers[widget.index]
              ? Border.all(color: Colors.red)
              : Border.all(color: Color(0xff9e9e9e), width: 1.6)),
      margin: EdgeInsets.only(left: 16),
      child: GestureDetector(
          child: Container(
            child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Stack(
                  children: [
                    Container(
                      child: homelModel.mostRecentUsers[widget.index].image !=
                              null
                          ? CachedNetworkImage(
                              imageUrl: IMAGE_BASE_URL +
                                  homelModel.mostRecentUsers[widget.index].image
                                      .toString(),
                              fit: BoxFit.cover,
                              width: MediaQuery.of(context).size.width,
                            )
                          : Image.asset(
                              "assets/images/hci_adventures.jpg",
                              fit: BoxFit.cover,
                              width: MediaQuery.of(context).size.width,
                            ),
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width * .4,
                          height: 35,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10)),
                          child: Container(
                            decoration: new BoxDecoration(
                                color: rustRed,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(14),
                                    bottomLeft: Radius.zero)),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                new Image.asset(
                                  "assets/icons/bookmark.png",
                                  height: 30,
                                  color: Colors.white,
                                ),
                                SizedBox(width: 5),
                                Text(
                                    homelModel.mostRecentUsers[widget.index]
                                        .warranty ==
                                        0
                                        ? S.current.materialWarranty
                                        : S.current.socialWarranty,
                                    textScaleFactor: 1.0,
                                    style: TextStyle(
                                      fontFamily: Fonts.fontfamily,
                                      color: Color(0xffffffff),
                                      fontSize: Fonts.fontSize_small,
                                      fontWeight: FontWeight.w500,
                                      fontStyle: FontStyle.normal,
                                    )),
                              ],
                            ),
                          ),
                        ),
                        Container(
                            color: Color(0xfff5f5f5),
                            height: 45,
                            padding: EdgeInsets.symmetric(horizontal: 12),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: [
                                    InkWell(
                                      onTap: () {
                                        setState(() {
                                          AddOrphenIconColor = Colors.red;
                                        });
                                        showLoading(context);
                                        homelModel.addOrphenToMyLikes(
                                            orphenId: homelModel
                                                .mostRecentUsers[widget.index]
                                                .id,
                                            onSuccess: () {
                                              dismissLoading();
                                              showSuccesses(context,
                                                  S.current.addOrphenSuccess);
                                            },
                                            onError: (error) {
                                              dismissLoading();
                                              showError(error.toString());
                                            });
                                      },
                                      child: Icon(
                                        Icons.favorite,
                                        color: AddOrphenIconColor,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Container(
                                      width: MediaQuery.of(context).size.width *
                                          .42,
                                      height: 50,
                                      child: AutoSizeText(
                                          homelModel
                                                      .mostRecentUsers[
                                                          widget.index]
                                                      .name ==
                                                  null
                                              ? ""
                                              : homelModel
                                                  .mostRecentUsers[widget.index]
                                                  .name
                                                  .toString(),
                                          textScaleFactor: 1.0,
                                          style: TextStyle(
                                            fontFamily: Fonts.fontfamily,
                                            color: Color(0xff4f4f4f),
                                            fontSize: Fonts.fontSize_meduim,
                                            fontWeight: FontWeight.w500,
                                            fontStyle: FontStyle.normal,
                                          )),
                                    ),
                                  ],
                                ),
                                new Image.asset(
                                  "assets/icons/find.png",
                                  height: 30,
                                  color: Color(0xff9e9e9e),
                                ),
                              ],
                            )),
                      ],
                    )
                  ],
                )),
          ),
          onTap: () {
            print(shouldScaleDown.toString());
            shouldScaleDown = !shouldScaleDown;
            homelModel.setCurrentSelectedRecentUser(
                homelModel.mostRecentUsers[widget.index]);
            Navigator.of(context).pushNamed(Routes.ORPHEN_SCREEN,
                arguments: homelModel.mostRecentUsers[widget.index]);
            setState(() {});
          }),
    );
  }
}
