import 'package:aytamy/common/model/User.dart';
import 'package:aytamy/generated/l10n.dart';
import 'package:aytamy/screens/home/provider/home_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'case_item_view.dart';

class MostRecentListView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MostRecentListViewState();
  }
}

class _MostRecentListViewState extends State<MostRecentListView> {
  User user;

  @override
  void initState() {
    super.initState();
    Provider.of<HomeModel>(context, listen: false).getMostRecentUsers();
  }

  // Future<void> _fetchPage(int pageKey) async {
  //   final nextPageKey = pageKey + 1;
  // }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Consumer<HomeModel>(
        builder: (context, homeModel, child) {
          return Stack(
            alignment: Alignment.topCenter,
            children: <Widget>[
              ListView(),
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Expanded(
                        child:
                            renderMostRecentUsers(homeModel.mostRecentUsers)),
                  ],
                ),
              ),
            ],
          );
        },
      ),
    );
  }

  Widget renderMostRecentUsers(List<User> mostRecentUser) {
    return Container(
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: mostRecentUser.length,
        itemBuilder: (BuildContext context, int index) {
          return mostRecentUser != null
              ? Container(
                  width: MediaQuery.of(context).size.width * .7,
                  height: MediaQuery.of(context).size.height * .2,
                  alignment: Alignment.center,
                  child: CaseItemView(
                    index: index,
                  ),
                )
              : Text(S.current.loading + "...");
        },
      ),
    );
  }
}
