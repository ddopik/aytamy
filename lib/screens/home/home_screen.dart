import 'package:aytamy/common/dimen.dart';
import 'package:aytamy/common/fonts.dart';
import 'package:aytamy/generated/l10n.dart';
import 'package:aytamy/screens/likes/likes_screen.dart';
import 'package:aytamy/screens/new_orphen/new_orphen_screen.dart';
import 'package:aytamy/screens/notifications/notifications_screen.dart';
import 'package:aytamy/screens/payment/payment_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'home_navigation_drawer.dart';
import 'home_screen_view.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomeScreenState();
  }
}

class _HomeScreenState extends State<HomeScreen> {
  CurrentHomeSelection _currentDrawerSelection;
  String currentName = S.current.main;

  @override
  void initState() {

    Future.delayed(Duration.zero, () {
      setState(() {
        _currentDrawerSelection =
            ModalRoute.of(context).settings.arguments as CurrentHomeSelection;
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        centerTitle: false,
        title: Text(currentName,
            textScaleFactor: 1.0,
            style: const TextStyle(
                color: const Color(0xffffffff),
                fontWeight: FontWeight.w700,
                fontFamily: Fonts.fontfamily,
                fontStyle: FontStyle.normal,
                fontSize: Fonts.fontSize_small),
            textAlign: TextAlign.center),
        actions: <Widget>[
          GestureDetector(
            child: new Container(
              padding: EdgeInsets.all(10),
              child: Image.asset(
                "assets/logo.png",
                color: Colors.white,
              ),
            ),
            onTap: () {
              // Navigator.of(context).pushNamed(Routes.CART);
            },
          ),
        ],
      ),
      drawer: HomeNavigationDrawer(
        onNavigateClick: (selectedNav) {
          setState(() {
            _currentDrawerSelection = selectedNav;
            switch (_currentDrawerSelection) {
              case CurrentHomeSelection.LIKES:
                currentName = S.current.myPriority;
                break;
              case CurrentHomeSelection.HOME:
                currentName = S.current.main;
                break;
              case CurrentHomeSelection.TRANSACTION:
                currentName = S.current.transactions;
                break;
              case CurrentHomeSelection.NOTIFICATION:
                currentName = S.current.notification;
                break;
              case CurrentHomeSelection.PROFILE:
                currentName = S.current.edit_profile;
                break;
              case CurrentHomeSelection.NEW_ORPHEN:
                currentName = S.current.newOrphen;
                break;
            }
          });
        },
      ),
      body: Container(
          margin: EdgeInsets.only(top: 2.0),
          padding: EdgeInsets.all(outer_boundary_field_space),
          alignment: Alignment.topCenter,
          child: renderMainView()),
    );
  }

  Widget renderMainView() {
    print("renderMainView ----> " + _currentDrawerSelection.toString());
    switch (_currentDrawerSelection) {
      case CurrentHomeSelection.HOME:
        return HomeScreenView();
      case CurrentHomeSelection.TRANSACTION:
        return PaymentScreen();
        break;
      case CurrentHomeSelection.NOTIFICATION:
        return NotificationsScreen();
        break;
      case CurrentHomeSelection.LIKES:
        return likesScreen();
        break;
      case CurrentHomeSelection.PROFILE:
      // return ProfileScreen();
        break;
      case CurrentHomeSelection.NEW_ORPHEN:
        return NewOrphenScreen();
        break;
      default:
        {
          return HomeScreenView();
        }
    }
  }
}
