import 'package:aytamy/common/dimen.dart';
import 'package:flutter/material.dart';
import 'package:aytamy/generated/l10n.dart';

class TermsOfUseScreen extends StatefulWidget {
  @override
  _TermsOfUseScreenState createState() => _TermsOfUseScreenState();
}

class _TermsOfUseScreenState extends State<TermsOfUseScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: Container(),
          centerTitle: true,
          title: Text(
            S.current.term_of_use,
            style: TextStyle(
              fontFamily: 'GESSTwo',
              color: Colors.white,
              fontSize: 24.0,
              fontWeight: FontWeight.bold,
              fontStyle: FontStyle.normal,
            ),
          ),
          actions: [
            IconButton(
                icon: Icon(Icons.arrow_forward_ios_outlined),
                onPressed: () {
                  Navigator.of(context).pop();
                })
          ],
        ),
        body: SingleChildScrollView(
            child: Container(

          padding: EdgeInsets.all(12),
              child: Column(
             children: [
              Text(
                S.current.term_of_use_SubTitle,
                style: TextStyle(
                  fontFamily: 'GESSTwo',
                  color: Color(0xff4f4f4f),
                  fontSize: 22,
                  fontWeight: FontWeight.w700,
                  fontStyle: FontStyle.normal,
                  letterSpacing: -0.19285714530944825,
                ),
                textAlign: TextAlign.center,
              ),
               SizedBox(height: form_field_space_min,),
              Text(
                S.current.term_of_use_SubContent,
                style: TextStyle(
                  fontFamily: 'GESSTwo',
                  color: Color(0xff4f4f4f),
                  fontSize: 18,
                  fontWeight: FontWeight.w300,
                  fontStyle: FontStyle.normal,
                  letterSpacing: -0.19285714530944825,
                ),
                textAlign: TextAlign.center,
              ),
              // Rectangle 49
            ],
          ),
        )));
  }
}
