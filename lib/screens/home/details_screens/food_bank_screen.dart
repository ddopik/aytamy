import 'package:auto_size_text/auto_size_text.dart';
import 'package:aytamy/common/fonts.dart';
import 'package:aytamy/generated/l10n.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../dialog_view.dart';

class FoodBankScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _FoodBankScreenState();
  }
}

class _FoodBankScreenState extends State<FoodBankScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          height: MediaQuery.of(context).size.height,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Stack(alignment: Alignment.topLeft, children: [
                Image.asset(
                  "assets/images/fresh-tomatoes-bowl copy.png",
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * .38,
                  fit: BoxFit.cover,
                ),
                Positioned(
                    top: 12,
                    left: -12,
                    child: InkWell(
                      child: Container(
                        height: MediaQuery.of(context).size.height * .11,
                        child: new Image.asset(
                          "assets/icons/cancel.png",
                          color: Colors.red,
                        ),
                      ),
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                    )),
              ]),
              Container(
                padding: EdgeInsets.all(18),
                height: MediaQuery.of(context).size.height * .6,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 12,
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Image.asset(
                              "assets/drawable-hdpi/fastfood-24px.png",
                              color: Colors.red,
                            ),
                            SizedBox(width: 12,),
                            Container(
                              width: MediaQuery.of(context).size.width * .78,
                              child: AutoSizeText(S.current.foodTitle,
                                  textScaleFactor: 1.1,
                                  style: TextStyle(
                                    fontFamily: Fonts.fontfamily,
                                    color: Color(0xffdb0011),
                                    fontSize: Fonts.fontSize_head,
                                    fontWeight: FontWeight.w700,
                                    fontStyle: FontStyle.normal,
                                    letterSpacing: -0.23571428871154784,
                                  )),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 12,
                        ),
                        Container(
                          child: SingleChildScrollView(
                            child: Column(
                              children: [
                                new Text(S.current.foodSubTitle,
                                    textScaleFactor: 1.1,
                                    style: TextStyle(
                                      fontFamily: Fonts.fontfamily,
                                      color: Color(0xff4f4f4f),
                                      fontSize: Fonts.fontSize_large,
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.normal,
                                      letterSpacing: -0.23571428871154784,
                                    )),
                                SizedBox(
                                  height: 12,
                                ),
                                new Text(S.current.foodContent,
                                    textScaleFactor: 1.1,
                                    style: TextStyle(
                                      fontFamily: Fonts.fontfamily,
                                      color: Color(0xff4f4f4f),
                                      fontSize: Fonts.fontSize_meduim,
                                      fontWeight: FontWeight.w300,
                                      fontStyle: FontStyle.normal,
                                      letterSpacing: -0.23571428871154784,
                                    )),

                                // Rectangle 49
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    GestureDetector(
                      child: Container(
                          width: MediaQuery.of(context).size.width * .9,
                          height: 55,
                          alignment: Alignment.center,
                          child: // تبرع
                              Text(S.current.donateButton,
                                  textScaleFactor: 1.5,
                                  style: const TextStyle(
                                      color: const Color(0xffffffff),
                                      fontWeight: FontWeight.w700,
                                      fontFamily: "GESSTwo",
                                      fontStyle: FontStyle.normal,
                                      fontSize: Fonts.fontSize_small),
                                  textAlign: TextAlign.center),
                          decoration: BoxDecoration(
                              color: Color(0xffdb0011),
                              borderRadius: BorderRadius.circular(16))),
                      onTap: () {
                        addPayDialogView(
                            context: context,
                            payType: S.current.dialogButtonmeal,
                            type: S.current.meal);
                      },
                    )
                  ],
                ),
              )
            ],
          )),
    );
  }
}
