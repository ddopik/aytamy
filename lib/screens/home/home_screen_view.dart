import 'package:aytamy/common/colors.dart';
import 'package:aytamy/common/dimen.dart';
import 'package:aytamy/common/fonts.dart';
import 'package:aytamy/generated/l10n.dart';
import 'package:aytamy/screens/home/provider/home_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:provider/provider.dart';

import '../../app/route.dart';
import '../../common/stats_widgets.dart';
import 'dialog_view.dart';
import 'most_recent/being_adopted_list_view.dart';
import 'most_recent/most_recent_list_view.dart';
import 'search/search_list_view.dart';

class HomeScreenView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomeScreenViewState();
  }
}

class HomeScreenViewState extends State with SingleTickerProviderStateMixin {
  TabController _tabController;
  HomeModel _homeModel;

  @override
  void initState() {
    _tabController = TabController(length: 2, vsync: this);
    _homeModel = Provider.of<HomeModel>(context, listen: false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      child: getMainView(),
    );
  }

  getMainView() {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            height: list_separator_space,
          ),
          getSearchView(),
          ...getTabBarView(),
          SizedBox(
            height: list_separator_space,
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.92,
            height: 48,
            decoration: new BoxDecoration(
                color: Color(0xffdb0011),
                borderRadius: BorderRadius.circular(10)),
            child: InkWell(
              child: Center(
                child: new Text(S.current.Voucher,
                    textScaleFactor: 1.0,
                    style: TextStyle(
                      fontFamily: Fonts.fontfamily,
                      color: Color(0xffffffff),
                      fontSize: Fonts.fontSize_head,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                    )),
              ),
              onTap: () {
                addPayDialogView(
                    context: context,
                    type: S.current.piece,
                    payType: S.current.dialogButtonpiece);
              },
            ),
          ),
          SizedBox(
            height: list_separator_space,
          ),
          Container(
            padding: EdgeInsets.all(6),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  onTap: () {
                    Navigator.of(context).pushNamed(Routes.FOOD_SCREEN);
                  },
                  child: Column(
                    children: [
                      new Container(
                        width: MediaQuery.of(context).size.width * .22,
                        height: MediaQuery.of(context).size.width * .22,
                        decoration: new BoxDecoration(
                          color: const Color(0xff7c94b6),
                          image: new DecorationImage(
                            image: AssetImage(
                                "assets/images/fresh-tomatoes-bowl copy.png"),
                            fit: BoxFit.fill,
                          ),
                          borderRadius:
                              new BorderRadius.all(new Radius.circular(50.0)),
                          border: new Border.all(
                            color: Colors.red,
                            width: 2.0,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 6,
                      ),
                      Text(S.current.foodTitle,
                          textScaleFactor: 1.0,
                          style: TextStyle(
                            fontFamily: 'GESSTwo',
                            color: Color(0xffdb0011),
                            fontSize: Fonts.fontSize_large,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.normal,
                            letterSpacing: -0.15000000190734863,
                          ))
                    ],
                  ),
                ),
                InkWell(
                  onTap: () {
                    Navigator.of(context).pushNamed(Routes.TREATEMENT_SCREEN);
                  },
                  child: Column(
                    children: [
                      new Container(
                        width: MediaQuery.of(context).size.width * .22,
                        height: MediaQuery.of(context).size.width * .22,
                        decoration: new BoxDecoration(
                          color: const Color(0xff7c94b6),
                          image: new DecorationImage(
                            image: AssetImage(
                                "assets/images/medical-stethoscope-white-surface.png"),
                            fit: BoxFit.cover,
                          ),
                          borderRadius:
                              new BorderRadius.all(new Radius.circular(50.0)),
                          border: new Border.all(
                            color: Colors.red,
                            width: 2.0,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 6,
                      ),
                      new Text(S.current.treatementTitle,
                          textScaleFactor: 1.0,
                          style: TextStyle(
                            fontFamily: 'GESSTwo',
                            color: Color(0xffdb0011),
                            fontSize: Fonts.fontSize_large,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.normal,
                            letterSpacing: -0.15000000190734863,
                          ))
                    ],
                  ),
                ),
                InkWell(
                  onTap: () {
                    Navigator.of(context).pushNamed(Routes.CLOTHES_SCREEN);
                  },
                  child: Column(
                    children: [
                      new Container(
                        width: MediaQuery.of(context).size.width * .22,
                        height: MediaQuery.of(context).size.width * .22,
                        decoration: new BoxDecoration(
                          color: const Color(0xff7c94b6),
                          image: new DecorationImage(
                            image: AssetImage("assets/images/T-Shirt copy.png"),
                            fit: BoxFit.cover,
                          ),
                          borderRadius:
                              new BorderRadius.all(new Radius.circular(50.0)),
                          border: new Border.all(
                            color: Colors.red,
                            width: 2.0,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 6,
                      ),
                      Text(S.current.clothesBank,
                          textScaleFactor: 1.0,
                          style: TextStyle(
                            fontFamily: Fonts.fontfamily,
                            color: Color(0xffdb0011),
                            fontSize: Fonts.fontSize_large,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.normal,
                            letterSpacing: -0.15000000190734863,
                          ))
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: list_separator_space,
          ),
          Stack(
            clipBehavior: Clip.none,
            children: [
              Container(
                width: MediaQuery.of(context).size.width * 0.85,
                padding: EdgeInsets.all(outer_boundary_field_space),
                decoration: new BoxDecoration(
                    color: Color(0xfff5f6f7),
                    borderRadius: BorderRadius.circular(10)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(S.current.homeWelcomeText,
                        textScaleFactor: 1.0,
                        style: TextStyle(
                          fontFamily: Fonts.fontfamily,
                          color: Colors.black87,
                          fontSize: Fonts.fontSize_small,
                          fontWeight: FontWeight.w800,
                          fontStyle: FontStyle.normal,
                        ))
                  ],
                ),
              ),
              Positioned(
                  top: -10,
                  right: 0,
                  child: ClipOval(
                    child: Container(
                      width: 20,
                      height: 20,
                      color: Colors.red,
                      child: Icon(
                        Icons.contact_support_outlined,
                        color: Colors.white,
                      ),
                    ),
                  ))
            ],
          ),
          SizedBox(
            height: list_separator_space,
          ),
        ],
      ),
    );
  }

  Widget getSearchView() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        InkWell(
          onTap: () {
            Navigator.of(context).pushNamed(Routes.NewOrphen_Screen);
          },
          child: Container(
              width: 36,
              height: 36,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: const Color(0xffdb0011)),
              child: Icon(
                Icons.add,
                color: Colors.white,
                size: 22,
              )),
        ),
        SizedBox(
          width: 8,
        ),
        Container(
          width: MediaQuery.of(context).size.width * .8,
          height: 36,
          decoration: BoxDecoration(
            color: Color(0xfff5f5f5),
            borderRadius: BorderRadius.circular(10),
          ),
          child: TextFormField(
            autofocus: false,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(6.0),
              border: OutlineInputBorder(
                borderSide: BorderSide(color: Color(0xff4f4f4f), width: 1.5),
                borderRadius: BorderRadius.circular(10.0),
              ),
              disabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10.0),
                borderSide: BorderSide(color: Color(0xff4f4f4f), width: 1.5),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10.0),
                borderSide: BorderSide(color: Colors.red, width: 1.5),
              ),
              hintText: S.current.orphanSearch,
              hintStyle: TextStyle(
                color: Color(0xff4f4f4f),
              ),
              prefixIcon: Icon(
                Icons.search,
                color: Color(0xff4f4f4f),
              ),
            ),
            onChanged: (value) {
              showLoading(context);
              Provider.of<HomeModel>(context, listen: false).getSearchResult(
                  value.toString().toLowerCase(), onSucess: () {
                dismissLoading();
              }, onError: (error) {
                dismissLoading();
              });
              if (value.isEmpty) {
                _homeModel.clearSearch();
              }
            },
          ),
        ),
      ],
    );
  }

  List<Widget> getTabBarView() {
    return [
      Container(
        margin: EdgeInsets.symmetric(horizontal: 4),
        child: TabBar(
          unselectedLabelColor: Color(0xff9e9e9e),
          labelColor: Colors.red,
          indicatorColor: rustRed,
          unselectedLabelStyle: TextStyle(
              fontSize: Fonts.fontSize_small,
              fontWeight: FontWeight.w400,
              fontFamily: Fonts.fontfamily),
          tabs: [
            Tab(
                child: Row(
                  children: [
                    new Container(
                      child: new Icon(
                        Icons.people,
                        size: 36,
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(S.current.mostRecent,
                        textScaleFactor: 1.0,
                        style: TextStyle(
                          fontFamily: Fonts.fontfamily,
                          fontSize: Fonts.fontSize_head,
                          fontWeight: FontWeight.w800,
                          fontStyle: FontStyle.normal,
                        )),
                  ],
                )),
            Tab(
              child: Row(
                children: [
                  Container(
                    child: Image.asset(
                      "assets/icons/right.png",
                      height: 32,
                      color: Colors.red,
                    ),
                    margin: EdgeInsets.symmetric(horizontal: 4),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(S.current.beingBailed,
                      textScaleFactor: 1.0,
                      style: TextStyle(
                        fontFamily: Fonts.fontfamily,
                        fontSize: Fonts.fontSize_head,
                        fontWeight: FontWeight.w800,
                        fontStyle: FontStyle.normal,
                      )),
                ],
              ),
            )
          ],
          onTap: (index) {
            _homeModel.clearSearch();
          },
          controller: _tabController,
          indicatorSize: TabBarIndicatorSize.tab,
        ),
      ),
      SizedBox(
        height: list_separator_space,
      ),
      Consumer<HomeModel>(builder: (context, homeModel, child) {
        return Container(
            height: MediaQuery.of(context).size.height * .28,
            child: _homeModel.SearchUsers.isNotEmpty
                ? SearchListView()
                : TabBarView(
                    children: [MostRecentListView(), BeingAdoptedListView()],
                    controller: _tabController,
                  ));
      })
    ];
  }
}
