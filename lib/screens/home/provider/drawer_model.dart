import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DrawerModel with ChangeNotifier {
  Color HomedselectedColor = Colors.white;
  Color NewOrphenselectedColor = Colors.white;
  Color lastTurnsselectedColor = Colors.white;
  Color NotificationselectedColor = Colors.white;
  Color PrioritiesselectedColor = Colors.white;

  SelectHome() {
    HomedselectedColor = Color(0xff4f4f4f);
    NewOrphenselectedColor = Colors.white;
    lastTurnsselectedColor = Colors.white;
    NotificationselectedColor = Colors.white;
    PrioritiesselectedColor = Colors.white;
    notifyListeners();
  }

  SelectNewOrphen() {
    HomedselectedColor = Colors.white;
    NewOrphenselectedColor = Color(0xff4f4f4f);
    lastTurnsselectedColor = Colors.white;
    NotificationselectedColor = Colors.white;
    PrioritiesselectedColor = Colors.white;
    notifyListeners();
  }

  SelectlastTurns() {
    HomedselectedColor = Colors.white;
    NewOrphenselectedColor = Colors.white;
    lastTurnsselectedColor = Color(0xff4f4f4f);
    NotificationselectedColor = Colors.white;
    PrioritiesselectedColor = Colors.white;
    notifyListeners();
  }

  SelectNotification() {
    HomedselectedColor = Colors.white;
    NewOrphenselectedColor = Colors.white;
    lastTurnsselectedColor = Colors.white;
    NotificationselectedColor = Color(0xff4f4f4f);
    PrioritiesselectedColor = Colors.white;
    notifyListeners();
  }

  SelectPriorities() {
    HomedselectedColor = Colors.white;
    NewOrphenselectedColor = Colors.white;
    lastTurnsselectedColor = Colors.white;
    NotificationselectedColor = Colors.white;
    PrioritiesselectedColor = Color(0xff4f4f4f);
    notifyListeners();
  }
}
