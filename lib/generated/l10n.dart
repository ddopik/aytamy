// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values

class S {
  S();
  
  static S current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      S.current = S();
      
      return S.current;
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Sign Up`
  String get signUp {
    return Intl.message(
      'Sign Up',
      name: 'signUp',
      desc: '',
      args: [],
    );
  }

  /// `Login`
  String get login {
    return Intl.message(
      'Login',
      name: 'login',
      desc: '',
      args: [],
    );
  }

  /// `Invalid user name or password `
  String get invalidLogin {
    return Intl.message(
      'Invalid user name or password ',
      name: 'invalidLogin',
      desc: '',
      args: [],
    );
  }

  /// `User name`
  String get userName {
    return Intl.message(
      'User name',
      name: 'userName',
      desc: '',
      args: [],
    );
  }

  /// `Edit profile`
  String get edit_profile {
    return Intl.message(
      'Edit profile',
      name: 'edit_profile',
      desc: '',
      args: [],
    );
  }

  /// `Forgot Password`
  String get ForgotPassword {
    return Intl.message(
      'Forgot Password',
      name: 'ForgotPassword',
      desc: '',
      args: [],
    );
  }

  /// `Password Reset`
  String get resetPassword {
    return Intl.message(
      'Password Reset',
      name: 'resetPassword',
      desc: '',
      args: [],
    );
  }

  /// `Send link`
  String get sendResetPasswordLink {
    return Intl.message(
      'Send link',
      name: 'sendResetPasswordLink',
      desc: '',
      args: [],
    );
  }

  /// `Don't have an account ?`
  String get DoNotHaveAnAccount {
    return Intl.message(
      'Don\'t have an account ?',
      name: 'DoNotHaveAnAccount',
      desc: '',
      args: [],
    );
  }

  /// `close`
  String get close {
    return Intl.message(
      'close',
      name: 'close',
      desc: '',
      args: [],
    );
  }

  /// `loading`
  String get loading {
    return Intl.message(
      'loading',
      name: 'loading',
      desc: '',
      args: [],
    );
  }

  /// `My services`
  String get myServices {
    return Intl.message(
      'My services',
      name: 'myServices',
      desc: '',
      args: [],
    );
  }

  /// `Notification`
  String get notification {
    return Intl.message(
      'Notification',
      name: 'notification',
      desc: '',
      args: [],
    );
  }

  /// `Help`
  String get help {
    return Intl.message(
      'Help',
      name: 'help',
      desc: '',
      args: [],
    );
  }

  /// `Setting`
  String get setting {
    return Intl.message(
      'Setting',
      name: 'setting',
      desc: '',
      args: [],
    );
  }

  /// `Sign Out`
  String get signOut {
    return Intl.message(
      'Sign Out',
      name: 'signOut',
      desc: '',
      args: [],
    );
  }

  /// `you have added`
  String get youHaveAdded {
    return Intl.message(
      'you have added',
      name: 'youHaveAdded',
      desc: '',
      args: [],
    );
  }

  /// `service to cart`
  String get cartService {
    return Intl.message(
      'service to cart',
      name: 'cartService',
      desc: '',
      args: [],
    );
  }

  /// `email`
  String get email {
    return Intl.message(
      'email',
      name: 'email',
      desc: '',
      args: [],
    );
  }

  /// `password`
  String get password {
    return Intl.message(
      'password',
      name: 'password',
      desc: '',
      args: [],
    );
  }

  /// `Forgot password`
  String get forgotPassword {
    return Intl.message(
      'Forgot password',
      name: 'forgotPassword',
      desc: '',
      args: [],
    );
  }

  /// `Don't have an account ?`
  String get doNotHaveAccount {
    return Intl.message(
      'Don\'t have an account ?',
      name: 'doNotHaveAccount',
      desc: '',
      args: [],
    );
  }

  /// `Already have an account`
  String get alreadyHaveAccount {
    return Intl.message(
      'Already have an account',
      name: 'alreadyHaveAccount',
      desc: '',
      args: [],
    );
  }

  /// `invalid password`
  String get invalidPassword {
    return Intl.message(
      'invalid password',
      name: 'invalidPassword',
      desc: '',
      args: [],
    );
  }

  /// `invalid Email`
  String get invalidEmail {
    return Intl.message(
      'invalid Email',
      name: 'invalidEmail',
      desc: '',
      args: [],
    );
  }

  /// `First name`
  String get firstName {
    return Intl.message(
      'First name',
      name: 'firstName',
      desc: '',
      args: [],
    );
  }

  /// `Last name`
  String get lastName {
    return Intl.message(
      'Last name',
      name: 'lastName',
      desc: '',
      args: [],
    );
  }

  /// `Confirm password`
  String get confirmPassword {
    return Intl.message(
      'Confirm password',
      name: 'confirmPassword',
      desc: '',
      args: [],
    );
  }

  /// `invalid`
  String get invalid {
    return Intl.message(
      'invalid',
      name: 'invalid',
      desc: '',
      args: [],
    );
  }

  /// `password not matched`
  String get notMatchedPassword {
    return Intl.message(
      'password not matched',
      name: 'notMatchedPassword',
      desc: '',
      args: [],
    );
  }

  /// `Invalid Phone number`
  String get invalidEmptyPhoneNumber {
    return Intl.message(
      'Invalid Phone number',
      name: 'invalidEmptyPhoneNumber',
      desc: '',
      args: [],
    );
  }

  /// `Phone number`
  String get phoneNumber {
    return Intl.message(
      'Phone number',
      name: 'phoneNumber',
      desc: '',
      args: [],
    );
  }

  /// `By creating an account you are agree to our \n terms of services and privacy Policy`
  String get policyHint {
    return Intl.message(
      'By creating an account you are agree to our \n terms of services and privacy Policy',
      name: 'policyHint',
      desc: '',
      args: [],
    );
  }

  /// `Account created successfully`
  String get accountCreated {
    return Intl.message(
      'Account created successfully',
      name: 'accountCreated',
      desc: '',
      args: [],
    );
  }

  /// `Check your inbox`
  String get checkYourInbox {
    return Intl.message(
      'Check your inbox',
      name: 'checkYourInbox',
      desc: '',
      args: [],
    );
  }

  /// `We have sent you a link through\nThe email you just entered`
  String get checkYourInboxDesc {
    return Intl.message(
      'We have sent you a link through\nThe email you just entered',
      name: 'checkYourInboxDesc',
      desc: '',
      args: [],
    );
  }

  /// `Cart`
  String get cart {
    return Intl.message(
      'Cart',
      name: 'cart',
      desc: '',
      args: [],
    );
  }

  /// `EGPِ`
  String get egp {
    return Intl.message(
      'EGPِ',
      name: 'egp',
      desc: '',
      args: [],
    );
  }

  /// `you have removed`
  String get youHaveRemoved {
    return Intl.message(
      'you have removed',
      name: 'youHaveRemoved',
      desc: '',
      args: [],
    );
  }

  /// `Create new account`
  String get createNewAccount {
    return Intl.message(
      'Create new account',
      name: 'createNewAccount',
      desc: '',
      args: [],
    );
  }

  /// `Create account`
  String get createAccount {
    return Intl.message(
      'Create account',
      name: 'createAccount',
      desc: '',
      args: [],
    );
  }

  /// `Invalid user name`
  String get invalidUserName {
    return Intl.message(
      'Invalid user name',
      name: 'invalidUserName',
      desc: '',
      args: [],
    );
  }

  /// `Invalid first name`
  String get invalidFirstName {
    return Intl.message(
      'Invalid first name',
      name: 'invalidFirstName',
      desc: '',
      args: [],
    );
  }

  /// `Invalid last name`
  String get invalidLastName {
    return Intl.message(
      'Invalid last name',
      name: 'invalidLastName',
      desc: '',
      args: [],
    );
  }

  /// `Password should be more then 6 char`
  String get passwordErrorLength {
    return Intl.message(
      'Password should be more then 6 char',
      name: 'passwordErrorLength',
      desc: '',
      args: [],
    );
  }

  /// `Password not matched`
  String get passwordWordNotMatched {
    return Intl.message(
      'Password not matched',
      name: 'passwordWordNotMatched',
      desc: '',
      args: [],
    );
  }

  /// `Please check internet connection and try again.`
  String get onTryAgainMessage {
    return Intl.message(
      'Please check internet connection and try again.',
      name: 'onTryAgainMessage',
      desc: '',
      args: [],
    );
  }

  /// `No connection`
  String get noConnection {
    return Intl.message(
      'No connection',
      name: 'noConnection',
      desc: '',
      args: [],
    );
  }

  /// `Try again`
  String get tryAgain {
    return Intl.message(
      'Try again',
      name: 'tryAgain',
      desc: '',
      args: [],
    );
  }

  /// `We couldn't find any results matching your applied Type.`
  String get emptyListMessage {
    return Intl.message(
      'We couldn\'t find any results matching your applied Type.',
      name: 'emptyListMessage',
      desc: '',
      args: [],
    );
  }

  /// `No result Found`
  String get noResultFound {
    return Intl.message(
      'No result Found',
      name: 'noResultFound',
      desc: '',
      args: [],
    );
  }

  /// `Something went wrong`
  String get someThingWrong {
    return Intl.message(
      'Something went wrong',
      name: 'someThingWrong',
      desc: '',
      args: [],
    );
  }

  /// `'The application has encountered an unknown error.\n'\n 'Please try again later.'`
  String get applicationError {
    return Intl.message(
      '\'The application has encountered an unknown error.\n\'\n \'Please try again later.\'',
      name: 'applicationError',
      desc: '',
      args: [],
    );
  }

  /// `Password mustn't contain any spaces`
  String get passwordSpaceError {
    return Intl.message(
      'Password mustn\'t contain any spaces',
      name: 'passwordSpaceError',
      desc: '',
      args: [],
    );
  }

  /// `EGPِ`
  String get egpPrice {
    return Intl.message(
      'EGPِ',
      name: 'egpPrice',
      desc: '',
      args: [],
    );
  }

  /// `Yes`
  String get yes {
    return Intl.message(
      'Yes',
      name: 'yes',
      desc: '',
      args: [],
    );
  }

  /// `No`
  String get no {
    return Intl.message(
      'No',
      name: 'no',
      desc: '',
      args: [],
    );
  }

  /// `Pay`
  String get pay {
    return Intl.message(
      'Pay',
      name: 'pay',
      desc: '',
      args: [],
    );
  }

  /// `Add new address`
  String get addNewAddress {
    return Intl.message(
      'Add new address',
      name: 'addNewAddress',
      desc: '',
      args: [],
    );
  }

  /// `Address name`
  String get addressName {
    return Intl.message(
      'Address name',
      name: 'addressName',
      desc: '',
      args: [],
    );
  }

  /// `City`
  String get addressCity {
    return Intl.message(
      'City',
      name: 'addressCity',
      desc: '',
      args: [],
    );
  }

  /// `Address street`
  String get addressStreet {
    return Intl.message(
      'Address street',
      name: 'addressStreet',
      desc: '',
      args: [],
    );
  }

  /// `Building number`
  String get addressBuildingNumber {
    return Intl.message(
      'Building number',
      name: 'addressBuildingNumber',
      desc: '',
      args: [],
    );
  }

  /// `Floor number`
  String get AddressFloorNumber {
    return Intl.message(
      'Floor number',
      name: 'AddressFloorNumber',
      desc: '',
      args: [],
    );
  }

  /// `Apartment number`
  String get apartmentNumber {
    return Intl.message(
      'Apartment number',
      name: 'apartmentNumber',
      desc: '',
      args: [],
    );
  }

  /// `Apply`
  String get apply {
    return Intl.message(
      'Apply',
      name: 'apply',
      desc: '',
      args: [],
    );
  }

  /// `Payment`
  String get payment {
    return Intl.message(
      'Payment',
      name: 'payment',
      desc: '',
      args: [],
    );
  }

  /// `Saved places`
  String get savedPlaces {
    return Intl.message(
      'Saved places',
      name: 'savedPlaces',
      desc: '',
      args: [],
    );
  }

  /// `Payment method`
  String get paymentMethod {
    return Intl.message(
      'Payment method',
      name: 'paymentMethod',
      desc: '',
      args: [],
    );
  }

  /// `Choose date`
  String get chooseDate {
    return Intl.message(
      'Choose date',
      name: 'chooseDate',
      desc: '',
      args: [],
    );
  }

  /// `Pay on Delivery`
  String get payOnDelivery {
    return Intl.message(
      'Pay on Delivery',
      name: 'payOnDelivery',
      desc: '',
      args: [],
    );
  }

  /// `No saved places`
  String get noSavedPlaces {
    return Intl.message(
      'No saved places',
      name: 'noSavedPlaces',
      desc: '',
      args: [],
    );
  }

  /// `Create request`
  String get createRequest {
    return Intl.message(
      'Create request',
      name: 'createRequest',
      desc: '',
      args: [],
    );
  }

  /// `Total`
  String get total {
    return Intl.message(
      'Total',
      name: 'total',
      desc: '',
      args: [],
    );
  }

  /// `Choose visiting date`
  String get chooseCheckDate {
    return Intl.message(
      'Choose visiting date',
      name: 'chooseCheckDate',
      desc: '',
      args: [],
    );
  }

  /// `Address submitted Successfully`
  String get addressCreatedSuccessfully {
    return Intl.message(
      'Address submitted Successfully',
      name: 'addressCreatedSuccessfully',
      desc: '',
      args: [],
    );
  }

  /// `Address updated Successfully`
  String get addressUpdatedSuccessfully {
    return Intl.message(
      'Address updated Successfully',
      name: 'addressUpdatedSuccessfully',
      desc: '',
      args: [],
    );
  }

  /// `Please add address in order to submit request`
  String get pleaseAddAddress {
    return Intl.message(
      'Please add address in order to submit request',
      name: 'pleaseAddAddress',
      desc: '',
      args: [],
    );
  }

  /// `Confirm`
  String get confirm {
    return Intl.message(
      'Confirm',
      name: 'confirm',
      desc: '',
      args: [],
    );
  }

  /// `you order has been sent !`
  String get transactionSubmitted {
    return Intl.message(
      'you order has been sent !',
      name: 'transactionSubmitted',
      desc: '',
      args: [],
    );
  }

  /// `You will receive a call from our representatives to confirm your appointment  You can track your order from`
  String get transactionSubmittedDesc {
    return Intl.message(
      'You will receive a call from our representatives to confirm your appointment  You can track your order from',
      name: 'transactionSubmittedDesc',
      desc: '',
      args: [],
    );
  }

  /// `All`
  String get all {
    return Intl.message(
      'All',
      name: 'all',
      desc: '',
      args: [],
    );
  }

  /// `Pending`
  String get pending {
    return Intl.message(
      'Pending',
      name: 'pending',
      desc: '',
      args: [],
    );
  }

  /// `Confirmed`
  String get confirmed {
    return Intl.message(
      'Confirmed',
      name: 'confirmed',
      desc: '',
      args: [],
    );
  }

  /// `Canceled`
  String get canceled {
    return Intl.message(
      'Canceled',
      name: 'canceled',
      desc: '',
      args: [],
    );
  }

  /// `OnGoing`
  String get onGoing {
    return Intl.message(
      'OnGoing',
      name: 'onGoing',
      desc: '',
      args: [],
    );
  }

  /// `delivered`
  String get delivered {
    return Intl.message(
      'delivered',
      name: 'delivered',
      desc: '',
      args: [],
    );
  }

  /// `Home`
  String get main {
    return Intl.message(
      'Home',
      name: 'main',
      desc: '',
      args: [],
    );
  }

  /// `Save`
  String get save {
    return Intl.message(
      'Save',
      name: 'save',
      desc: '',
      args: [],
    );
  }

  /// `My addresses`
  String get myAddresses {
    return Intl.message(
      'My addresses',
      name: 'myAddresses',
      desc: '',
      args: [],
    );
  }

  /// `Personal info`
  String get personalInfo {
    return Intl.message(
      'Personal info',
      name: 'personalInfo',
      desc: '',
      args: [],
    );
  }

  /// `Old password`
  String get oldPassword {
    return Intl.message(
      'Old password',
      name: 'oldPassword',
      desc: '',
      args: [],
    );
  }

  /// `New password`
  String get newPassword {
    return Intl.message(
      'New password',
      name: 'newPassword',
      desc: '',
      args: [],
    );
  }

  /// `Change password`
  String get changePassword {
    return Intl.message(
      'Change password',
      name: 'changePassword',
      desc: '',
      args: [],
    );
  }

  /// `password not matched`
  String get passwordNotMatched {
    return Intl.message(
      'password not matched',
      name: 'passwordNotMatched',
      desc: '',
      args: [],
    );
  }

  /// `Change name`
  String get changeName {
    return Intl.message(
      'Change name',
      name: 'changeName',
      desc: '',
      args: [],
    );
  }

  /// `Choose new profile Photo`
  String get chooseNewProfilePhoto {
    return Intl.message(
      'Choose new profile Photo',
      name: 'chooseNewProfilePhoto',
      desc: '',
      args: [],
    );
  }

  /// `Choose from Gallery`
  String get choosePhoto {
    return Intl.message(
      'Choose from Gallery',
      name: 'choosePhoto',
      desc: '',
      args: [],
    );
  }

  /// `Capture photo`
  String get CapturePhoto {
    return Intl.message(
      'Capture photo',
      name: 'CapturePhoto',
      desc: '',
      args: [],
    );
  }

  /// `Delete photo`
  String get deletePhoto {
    return Intl.message(
      'Delete photo',
      name: 'deletePhoto',
      desc: '',
      args: [],
    );
  }

  /// `soon`
  String get soon {
    return Intl.message(
      'soon',
      name: 'soon',
      desc: '',
      args: [],
    );
  }

  /// `Explore`
  String get explore {
    return Intl.message(
      'Explore',
      name: 'explore',
      desc: '',
      args: [],
    );
  }

  /// `Orphan care homes`
  String get text_1 {
    return Intl.message(
      'Orphan care homes',
      name: 'text_1',
      desc: '',
      args: [],
    );
  }

  /// `Privacy policy\nTerms of use`
  String get copyRight {
    return Intl.message(
      'Privacy policy\nTerms of use',
      name: 'copyRight',
      desc: '',
      args: [],
    );
  }

  /// `all rights are save`
  String get copyRight_2 {
    return Intl.message(
      'all rights are save',
      name: 'copyRight_2',
      desc: '',
      args: [],
    );
  }

  /// `Sign in with Google`
  String get signUpGoogle {
    return Intl.message(
      'Sign in with Google',
      name: 'signUpGoogle',
      desc: '',
      args: [],
    );
  }

  /// `Sign in with FaceBook`
  String get signUpFaceBook {
    return Intl.message(
      'Sign in with FaceBook',
      name: 'signUpFaceBook',
      desc: '',
      args: [],
    );
  }

  /// `Join US`
  String get joinUs {
    return Intl.message(
      'Join US',
      name: 'joinUs',
      desc: '',
      args: [],
    );
  }

  /// `don't have an account`
  String get doNotHaveAnAccount {
    return Intl.message(
      'don\'t have an account',
      name: 'doNotHaveAnAccount',
      desc: '',
      args: [],
    );
  }

  /// `Enter`
  String get enter {
    return Intl.message(
      'Enter',
      name: 'enter',
      desc: '',
      args: [],
    );
  }

  /// `Register`
  String get register {
    return Intl.message(
      'Register',
      name: 'register',
      desc: '',
      args: [],
    );
  }

  /// `My Info`
  String get myInfo {
    return Intl.message(
      'My Info',
      name: 'myInfo',
      desc: '',
      args: [],
    );
  }

  /// `continue`
  String get continueText {
    return Intl.message(
      'continue',
      name: 'continueText',
      desc: '',
      args: [],
    );
  }

  /// `Birth date`
  String get birthDate {
    return Intl.message(
      'Birth date',
      name: 'birthDate',
      desc: '',
      args: [],
    );
  }

  /// `Job`
  String get job {
    return Intl.message(
      'Job',
      name: 'job',
      desc: '',
      args: [],
    );
  }

  /// `Nationality`
  String get nationality {
    return Intl.message(
      'Nationality',
      name: 'nationality',
      desc: '',
      args: [],
    );
  }

  /// `City`
  String get city {
    return Intl.message(
      'City',
      name: 'city',
      desc: '',
      args: [],
    );
  }

  /// `Latest transaction`
  String get latestTransaction {
    return Intl.message(
      'Latest transaction',
      name: 'latestTransaction',
      desc: '',
      args: [],
    );
  }

  /// `My priority`
  String get myPriority {
    return Intl.message(
      'My priority',
      name: 'myPriority',
      desc: '',
      args: [],
    );
  }

  /// `New Orphan`
  String get NewOrphan {
    return Intl.message(
      'New Orphan',
      name: 'NewOrphan',
      desc: '',
      args: [],
    );
  }

  /// `Need support`
  String get need_support {
    return Intl.message(
      'Need support',
      name: 'need_support',
      desc: '',
      args: [],
    );
  }

  /// `Do you own an institution for orphans ?`
  String get registration_info_step_one {
    return Intl.message(
      'Do you own an institution for orphans ?',
      name: 'registration_info_step_one',
      desc: '',
      args: [],
    );
  }

  /// `This is your first time on bail`
  String get registrationInfoText {
    return Intl.message(
      'This is your first time on bail',
      name: 'registrationInfoText',
      desc: '',
      args: [],
    );
  }

  /// `Who wipes the head of an orphan\nOnly God did not wipe it, he had every hair\nHasanat passed by his hand`
  String get registrationInfoText2 {
    return Intl.message(
      'Who wipes the head of an orphan\nOnly God did not wipe it, he had every hair\nHasanat passed by his hand',
      name: 'registrationInfoText2',
      desc: '',
      args: [],
    );
  }

  /// `male`
  String get male {
    return Intl.message(
      'male',
      name: 'male',
      desc: '',
      args: [],
    );
  }

  /// `female`
  String get female {
    return Intl.message(
      'female',
      name: 'female',
      desc: '',
      args: [],
    );
  }

  /// `Orphan Search`
  String get orphanSearch {
    return Intl.message(
      'Orphan Search',
      name: 'orphanSearch',
      desc: '',
      args: [],
    );
  }

  /// `Most recent`
  String get mostRecent {
    return Intl.message(
      'Most recent',
      name: 'mostRecent',
      desc: '',
      args: [],
    );
  }

  /// `Being Adopted`
  String get beingBailed {
    return Intl.message(
      'Being Adopted',
      name: 'beingBailed',
      desc: '',
      args: [],
    );
  }

  /// `نهتم بأن نوفر لك أنسب الخيارات المتاحة للكفال`
  String get type {
    return Intl.message(
      'نهتم بأن نوفر لك أنسب الخيارات المتاحة للكفال',
      name: 'type',
      desc: '',
      args: [],
    );
  }

  /// `Clothes bank`
  String get clothesTitle {
    return Intl.message(
      'Clothes bank',
      name: 'clothesTitle',
      desc: '',
      args: [],
    );
  }

  /// `Taking into account the conditions of orphans and reducing them, we seek, through the application of orphans, to provide new pieces of clothing for orphans who are registered with us through the clothing bank.`
  String get clothesSubTtile {
    return Intl.message(
      'Taking into account the conditions of orphans and reducing them, we seek, through the application of orphans, to provide new pieces of clothing for orphans who are registered with us through the clothing bank.',
      name: 'clothesSubTtile',
      desc: '',
      args: [],
    );
  }

  /// `The clothing bank is one of the orphans banks where we receive new clothes to rearrange them and send them to our orphans directly through our agents in all countries where the inclusion of their orphans is available and the verification of their orphans is available by specialists tasked with checking cases of orphans.`
  String get clothesContent {
    return Intl.message(
      'The clothing bank is one of the orphans banks where we receive new clothes to rearrange them and send them to our orphans directly through our agents in all countries where the inclusion of their orphans is available and the verification of their orphans is available by specialists tasked with checking cases of orphans.',
      name: 'clothesContent',
      desc: '',
      args: [],
    );
  }

  /// `The food bank`
  String get foodTitle {
    return Intl.message(
      'The food bank',
      name: 'foodTitle',
      desc: '',
      args: [],
    );
  }

  /// `Taking into account the conditions of orphans and reducing them, we seek, through the application of orphans, to provide commodities and food products for orphans periodically through an orphan food bank`
  String get foodSubTitle {
    return Intl.message(
      'Taking into account the conditions of orphans and reducing them, we seek, through the application of orphans, to provide commodities and food products for orphans periodically through an orphan food bank',
      name: 'foodSubTitle',
      desc: '',
      args: [],
    );
  }

  /// `The food bank is one of the orphans banks where you can donate nutritious meals and dried or fresh foods and deliver them directly to the orphan directly and quickly. \ N- The price of one meal is 10 dollars sent to one orphan who is verified to complete it, and you can increase the number of meals through the Add meal button.`
  String get foodContent {
    return Intl.message(
      'The food bank is one of the orphans banks where you can donate nutritious meals and dried or fresh foods and deliver them directly to the orphan directly and quickly. \ N- The price of one meal is 10 dollars sent to one orphan who is verified to complete it, and you can increase the number of meals through the Add meal button.',
      name: 'foodContent',
      desc: '',
      args: [],
    );
  }

  /// `Treatment bank`
  String get treatementTitle {
    return Intl.message(
      'Treatment bank',
      name: 'treatementTitle',
      desc: '',
      args: [],
    );
  }

  /// `Taking into account the conditions of orphans and reducing them, we seek, through the application of orphans, to provide packages and medicines for orphans registered with us through the treatment bank.`
  String get treatementSubTitle {
    return Intl.message(
      'Taking into account the conditions of orphans and reducing them, we seek, through the application of orphans, to provide packages and medicines for orphans registered with us through the treatment bank.',
      name: 'treatementSubTitle',
      desc: '',
      args: [],
    );
  }

  /// `The treatment bank is one of the orphans banks where we receive donations and based on the treatment lists for orphans, the packages are sent to our orphans directly through our agents in all countries where the inclusion of their orphans is available, and the verified ones are carried out by social workers in charge of verifying cases of orphans.`
  String get treatementContent {
    return Intl.message(
      'The treatment bank is one of the orphans banks where we receive donations and based on the treatment lists for orphans, the packages are sent to our orphans directly through our agents in all countries where the inclusion of their orphans is available, and the verified ones are carried out by social workers in charge of verifying cases of orphans.',
      name: 'treatementContent',
      desc: '',
      args: [],
    );
  }

  /// `Material warranty`
  String get materialWarranty {
    return Intl.message(
      'Material warranty',
      name: 'materialWarranty',
      desc: '',
      args: [],
    );
  }

  /// `Social warranty`
  String get socialWarranty {
    return Intl.message(
      'Social warranty',
      name: 'socialWarranty',
      desc: '',
      args: [],
    );
  }

  /// `donation for 1 meal by`
  String get dialogButtonmeal {
    return Intl.message(
      'donation for 1 meal by',
      name: 'dialogButtonmeal',
      desc: '',
      args: [],
    );
  }

  /// `donation for 1 piece by`
  String get dialogButtonpiece {
    return Intl.message(
      'donation for 1 piece by',
      name: 'dialogButtonpiece',
      desc: '',
      args: [],
    );
  }

  /// `donation for 1 package by`
  String get dialogButtonpackage {
    return Intl.message(
      'donation for 1 package by',
      name: 'dialogButtonpackage',
      desc: '',
      args: [],
    );
  }

  /// `dollar`
  String get currency {
    return Intl.message(
      'dollar',
      name: 'currency',
      desc: '',
      args: [],
    );
  }

  /// `meal`
  String get meal {
    return Intl.message(
      'meal',
      name: 'meal',
      desc: '',
      args: [],
    );
  }

  /// `piece`
  String get piece {
    return Intl.message(
      'piece',
      name: 'piece',
      desc: '',
      args: [],
    );
  }

  /// `package`
  String get package {
    return Intl.message(
      'package',
      name: 'package',
      desc: '',
      args: [],
    );
  }

  /// `value`
  String get value {
    return Intl.message(
      'value',
      name: 'value',
      desc: '',
      args: [],
    );
  }

  /// `donate`
  String get donateButton {
    return Intl.message(
      'donate',
      name: 'donateButton',
      desc: '',
      args: [],
    );
  }

  /// `Voucher`
  String get Voucher {
    return Intl.message(
      'Voucher',
      name: 'Voucher',
      desc: '',
      args: [],
    );
  }

  /// `orphens Team`
  String get orphensTeam {
    return Intl.message(
      'orphens Team',
      name: 'orphensTeam',
      desc: '',
      args: [],
    );
  }

  /// `The orphan platform connects orphans with the sponsor to ensure a guarantee without an intermediary. You can also follow us on social media platforms (Facebook, Twitter, Instagram, WhatsApp) to follow us`
  String get homeWelcomeText {
    return Intl.message(
      'The orphan platform connects orphans with the sponsor to ensure a guarantee without an intermediary. You can also follow us on social media platforms (Facebook, Twitter, Instagram, WhatsApp) to follow us',
      name: 'homeWelcomeText',
      desc: '',
      args: [],
    );
  }

  /// `WhatsApp message`
  String get whatsapp {
    return Intl.message(
      'WhatsApp message',
      name: 'whatsapp',
      desc: '',
      args: [],
    );
  }

  /// `phone\n Call`
  String get phoneCall {
    return Intl.message(
      'phone\n Call',
      name: 'phoneCall',
      desc: '',
      args: [],
    );
  }

  /// `orphen \nReport`
  String get orphenReport {
    return Intl.message(
      'orphen \nReport',
      name: 'orphenReport',
      desc: '',
      args: [],
    );
  }

  /// `guarantees number`
  String get guaranteesNumber {
    return Intl.message(
      'guarantees number',
      name: 'guaranteesNumber',
      desc: '',
      args: [],
    );
  }

  /// `Orphan data`
  String get orphenData {
    return Intl.message(
      'Orphan data',
      name: 'orphenData',
      desc: '',
      args: [],
    );
  }

  /// `Dream`
  String get dream {
    return Intl.message(
      'Dream',
      name: 'dream',
      desc: '',
      args: [],
    );
  }

  /// `Spare Time`
  String get spareTime {
    return Intl.message(
      'Spare Time',
      name: 'spareTime',
      desc: '',
      args: [],
    );
  }

  /// `Personal proof`
  String get personalProof {
    return Intl.message(
      'Personal proof',
      name: 'personalProof',
      desc: '',
      args: [],
    );
  }

  /// `Switch Language`
  String get SwitchLanguage {
    return Intl.message(
      'Switch Language',
      name: 'SwitchLanguage',
      desc: '',
      args: [],
    );
  }

  /// `Methods of payment and receipt`
  String get methods {
    return Intl.message(
      'Methods of payment and receipt',
      name: 'methods',
      desc: '',
      args: [],
    );
  }

  /// `Share the application with your friends`
  String get share {
    return Intl.message(
      'Share the application with your friends',
      name: 'share',
      desc: '',
      args: [],
    );
  }

  /// `Inquiries and help`
  String get Inquiries {
    return Intl.message(
      'Inquiries and help',
      name: 'Inquiries',
      desc: '',
      args: [],
    );
  }

  /// `Sponsor for an orphan`
  String get orphenSponser {
    return Intl.message(
      'Sponsor for an orphan',
      name: 'orphenSponser',
      desc: '',
      args: [],
    );
  }

  /// `orphen`
  String get orphen {
    return Intl.message(
      'orphen',
      name: 'orphen',
      desc: '',
      args: [],
    );
  }

  /// `Education`
  String get eduSys {
    return Intl.message(
      'Education',
      name: 'eduSys',
      desc: '',
      args: [],
    );
  }

  /// `Father Certificate`
  String get fatherCertificate {
    return Intl.message(
      'Father Certificate',
      name: 'fatherCertificate',
      desc: '',
      args: [],
    );
  }

  /// ` Mother Certificate`
  String get motherCertificate {
    return Intl.message(
      ' Mother Certificate',
      name: 'motherCertificate',
      desc: '',
      args: [],
    );
  }

  /// `Educational Certificate`
  String get eduCertificate {
    return Intl.message(
      'Educational Certificate',
      name: 'eduCertificate',
      desc: '',
      args: [],
    );
  }

  /// `description`
  String get description {
    return Intl.message(
      'description',
      name: 'description',
      desc: '',
      args: [],
    );
  }

  /// `Parent Phone Number`
  String get parentMobile {
    return Intl.message(
      'Parent Phone Number',
      name: 'parentMobile',
      desc: '',
      args: [],
    );
  }

  /// `Country`
  String get country {
    return Intl.message(
      'Country',
      name: 'country',
      desc: '',
      args: [],
    );
  }

  /// `Delete`
  String get delete {
    return Intl.message(
      'Delete',
      name: 'delete',
      desc: '',
      args: [],
    );
  }

  /// `New Orphen`
  String get newOrphen {
    return Intl.message(
      'New Orphen',
      name: 'newOrphen',
      desc: '',
      args: [],
    );
  }

  /// `Clothes Bank`
  String get clothesBank {
    return Intl.message(
      'Clothes Bank',
      name: 'clothesBank',
      desc: '',
      args: [],
    );
  }

  /// `Welcome to the terms of use of the orphan app\nIt is of great importance to you in that it organizes and summarizes your legal rights when you use the application.\nIt explains the rules that all users must follow.\nTherefore, please carefully read these terms, our Privacy Policy, and any other terms referenced in this document.`
  String get term_of_use_SubTitle {
    return Intl.message(
      'Welcome to the terms of use of the orphan app\nIt is of great importance to you in that it organizes and summarizes your legal rights when you use the application.\nIt explains the rules that all users must follow.\nTherefore, please carefully read these terms, our Privacy Policy, and any other terms referenced in this document.',
      name: 'term_of_use_SubTitle',
      desc: '',
      args: [],
    );
  }

  /// `Conditions for using the orphan application: At the outset, we thank you for choosing an orphan application to ensure and care for orphans. The orphans application provides specialized services that include social and interactive features to provide a helping hand and assistance to our orphans and other social content, in addition to other services that can be developed from time to time that are broadcast Via the Internet to your mobile phone, or to any other compatible device of yours through which you can access the Internet. By creating a new account, obtaining a service or a financial or social aid plan with orphans, or purchasing and / or by downloading or entering and accessing or accessing an orphan application, or using an orphan application, or our website, or using a service or An additional product provided by orphans, or in another way, by accessing or using the content, or any of the service features or functions provided by orphans, as the case may be, you have entered into a binding agreement with orphans, and you agree and accept to be bound by these terms of use. . You also acknowledge and agree to Readli's use of your personal data in accordance with the Aitam Privacy Policy.\nThese terms of use describe and explain the conditions under which the service is available to you, and supersede and replace the previously agreed terms and conditions.\nAgreeing to these terms is necessary and essential to be able to access or use the service.\nBy accepting these terms, you agree and accept that orphans will terminate this agreement, or suspend your access or access to the service at any time without prior notice in the event that you are unable to comply with any of the terms stipulated in this agreement.\nIf you do not agree to the terms (as defined below), you may not use the service, access or access it, or use any content provided by orphans, and in addition, supplementary terms of use may apply to some parts of the service; For example: conditions related to specific or specific competition rules, additional terms and conditions for additional services or other activities, or for some additional content, or in relation to separate subscription plans, or products or programs that can be accessed or provided through the service. In the event that supplementary terms and conditions are required to be applied, they will be disclosed to you in relation to the related activities or products.\nAny additional and complementary terms and conditions provided by orphans are complementary and in addition to these terms of use, and have priority in effect in the event that they conflict with the terms of use. After, in the name of the conditions.\n1- Age limit and account eligibility:\n* To be eligible to accept the conclusion of the conditions and the creation of an account, or the conclusion of a subscription plan with orphans, the following conditions must be met: You have reached at least eighteen (18) years of age, or you have legal authorization and authority to conclude this agreement, or that the registration and use be in a state Not to exceed the age of eighteen years by a parent or adult to benefit from the service legally, in accordance with the laws in force in the country in which the service is provided to you.\nYou reside in the country in which the service is available and provided to you.\nYou agree to abide by the terms.\n* You must also provide and provide orphans with correct information about yourself, and you must also provide contact information, supporting documents, correct status and a valid payment method when registering your account with orphans. And you must agree to the terms and provide the information referred to, in order to be able to access, access and use the service.\nIn the event that you do not agree to the terms, you may not use the service or its functions or use any content provided in the service.\n2- Service:\n* The orphan application provides direct communication service with people participating in the service or donation services with clothes, food and treatment to help orphans so that you, as a subscriber, can review, register, or sponsor orphans in a physical or social manner after fulfilling the conditions and approval by both parties, or otherwise. Service contents on your mobile phone, or through any other devices that have a feature to connect to the Internet through an orphans application, or any other pre-installed programs from orphans. In addition, the service includes recommendations, messages and customized features that were specially developed and amended for you in order to improve and develop your usage experience.\nOrphans may also provide, from time to time, access or access to different types of social networking forums that may or may not be available to the public, in order to develop and enrich your usage experience. Unless otherwise agreed upon in any supplementary terms, or under a subscription plan provided to you by orphans, and which you have agreed to, the service may be used, as described above, only for your personal, non-commercial use as stipulated in the terms.\n* In order for you to use the service, you must use a device that connects to the Internet that complies with the technical requirements of orphans or one of the partner companies' platforms for orphans on which our application has been installed, and you must also provide us or our business partners with your choice of the payment method accepted by orphans, as it may be. Valid from time to time. Orphan reserves the right to amend the technical requirements for using the service and change, add or delete business partners and payment methods and methods from time to time. Orphans will do their utmost to maintain your secure and privileged use.\n`
  String get term_of_use_SubContent {
    return Intl.message(
      'Conditions for using the orphan application: At the outset, we thank you for choosing an orphan application to ensure and care for orphans. The orphans application provides specialized services that include social and interactive features to provide a helping hand and assistance to our orphans and other social content, in addition to other services that can be developed from time to time that are broadcast Via the Internet to your mobile phone, or to any other compatible device of yours through which you can access the Internet. By creating a new account, obtaining a service or a financial or social aid plan with orphans, or purchasing and / or by downloading or entering and accessing or accessing an orphan application, or using an orphan application, or our website, or using a service or An additional product provided by orphans, or in another way, by accessing or using the content, or any of the service features or functions provided by orphans, as the case may be, you have entered into a binding agreement with orphans, and you agree and accept to be bound by these terms of use. . You also acknowledge and agree to Readli\'s use of your personal data in accordance with the Aitam Privacy Policy.\nThese terms of use describe and explain the conditions under which the service is available to you, and supersede and replace the previously agreed terms and conditions.\nAgreeing to these terms is necessary and essential to be able to access or use the service.\nBy accepting these terms, you agree and accept that orphans will terminate this agreement, or suspend your access or access to the service at any time without prior notice in the event that you are unable to comply with any of the terms stipulated in this agreement.\nIf you do not agree to the terms (as defined below), you may not use the service, access or access it, or use any content provided by orphans, and in addition, supplementary terms of use may apply to some parts of the service; For example: conditions related to specific or specific competition rules, additional terms and conditions for additional services or other activities, or for some additional content, or in relation to separate subscription plans, or products or programs that can be accessed or provided through the service. In the event that supplementary terms and conditions are required to be applied, they will be disclosed to you in relation to the related activities or products.\nAny additional and complementary terms and conditions provided by orphans are complementary and in addition to these terms of use, and have priority in effect in the event that they conflict with the terms of use. After, in the name of the conditions.\n1- Age limit and account eligibility:\n* To be eligible to accept the conclusion of the conditions and the creation of an account, or the conclusion of a subscription plan with orphans, the following conditions must be met: You have reached at least eighteen (18) years of age, or you have legal authorization and authority to conclude this agreement, or that the registration and use be in a state Not to exceed the age of eighteen years by a parent or adult to benefit from the service legally, in accordance with the laws in force in the country in which the service is provided to you.\nYou reside in the country in which the service is available and provided to you.\nYou agree to abide by the terms.\n* You must also provide and provide orphans with correct information about yourself, and you must also provide contact information, supporting documents, correct status and a valid payment method when registering your account with orphans. And you must agree to the terms and provide the information referred to, in order to be able to access, access and use the service.\nIn the event that you do not agree to the terms, you may not use the service or its functions or use any content provided in the service.\n2- Service:\n* The orphan application provides direct communication service with people participating in the service or donation services with clothes, food and treatment to help orphans so that you, as a subscriber, can review, register, or sponsor orphans in a physical or social manner after fulfilling the conditions and approval by both parties, or otherwise. Service contents on your mobile phone, or through any other devices that have a feature to connect to the Internet through an orphans application, or any other pre-installed programs from orphans. In addition, the service includes recommendations, messages and customized features that were specially developed and amended for you in order to improve and develop your usage experience.\nOrphans may also provide, from time to time, access or access to different types of social networking forums that may or may not be available to the public, in order to develop and enrich your usage experience. Unless otherwise agreed upon in any supplementary terms, or under a subscription plan provided to you by orphans, and which you have agreed to, the service may be used, as described above, only for your personal, non-commercial use as stipulated in the terms.\n* In order for you to use the service, you must use a device that connects to the Internet that complies with the technical requirements of orphans or one of the partner companies\' platforms for orphans on which our application has been installed, and you must also provide us or our business partners with your choice of the payment method accepted by orphans, as it may be. Valid from time to time. Orphan reserves the right to amend the technical requirements for using the service and change, add or delete business partners and payment methods and methods from time to time. Orphans will do their utmost to maintain your secure and privileged use.\n',
      name: 'term_of_use_SubContent',
      desc: '',
      args: [],
    );
  }

  /// `added successfuly`
  String get addOrphenSuccess {
    return Intl.message(
      'added successfuly',
      name: 'addOrphenSuccess',
      desc: '',
      args: [],
    );
  }

  /// `Deleted successfuly`
  String get deleteItemSuccess {
    return Intl.message(
      'Deleted successfuly',
      name: 'deleteItemSuccess',
      desc: '',
      args: [],
    );
  }

  /// `Most Recent Notifications`
  String get mostRecentNotification {
    return Intl.message(
      'Most Recent Notifications',
      name: 'mostRecentNotification',
      desc: '',
      args: [],
    );
  }

  /// `Most Recent Favorites`
  String get mostRecentLikes {
    return Intl.message(
      'Most Recent Favorites',
      name: 'mostRecentLikes',
      desc: '',
      args: [],
    );
  }

  /// `Privacy policy`
  String get privacyAndPoliceTitle {
    return Intl.message(
      'Privacy policy',
      name: 'privacyAndPoliceTitle',
      desc: '',
      args: [],
    );
  }

  /// `The orphans app takes your privacy very seriously\nSo please read the following to learn more about the privacy policy followed by the app.`
  String get privacyAndPoliceSubTitle {
    return Intl.message(
      'The orphans app takes your privacy very seriously\nSo please read the following to learn more about the privacy policy followed by the app.',
      name: 'privacyAndPoliceSubTitle',
      desc: '',
      args: [],
    );
  }

  /// `How does the orphan app use your personal information The following information explains how the orphan app deals with the personal information it collects and receives, including information related to your previous use of any of the services and contents of the app.\nPersonal information is information about you that identifies you, such as your name, address, email address, phone number, Facebook account, or bank accounts. How to activate the privacy policy on the application This policy does not apply to the dealings or practices of sites or channels that are not owned or controlled by the application of orphans, as well as to employees or people who do not work in it or under its management.\nIn the event that any institutions, bodies or sites join the application, this privacy policy will apply to these institutions, agencies and sites.\nThe application collects personal information when you register to create a new account, when you register for some services, when you use some of the existing solutions, and when you visit a page or certain pages of our partners.\nOrphan may combine or collect information about individuals with information we obtain from business partners or other companies.\nWhen you register we ask for information such as your name, email address, date of birth, gender, zip code, occupation, place of residence. For some services, we may ask you for information about your assets, your address, how you access the Internet, your contact information, your hobbies, your preferences, and a personal profile about you. When you register for the application and register for our services, you are not anonymous to us.\nOrphans collect information about your dealings with us and with some of our business partners, including information about the use of financial products and services that we offer.\nThe application receives information from your device and from your Facebook account if you use it to log in, including your email address, application information and software, device features, and the page you request on the application. The app uses the information for the following general purposes: personalizing the advertisement and content that you see and hear, fulfilling your requests for content and services, improving our services, contacting you, and conducting research.\nChildren: Those under the age of ten are allowed to register on the application, provided that they are accompanied by one of the adults in their care.\nSubscriptions: The orphan platform provides many services that work to convey the voice of an orphan to those who deserve care.\nInformation sharing and disclosure: The orphan app does not rent, sell or share your personal information with other people or non-affiliated companies except to provide the contents or services that you requested and upon obtaining your permission or under the following circumstances: Providing information to trusted partners who work on behalf of or with The application of orphans under confidentiality agreements.\nThese companies may use your personal information to help the orphan app to communicate with you about offers from the orphan app and our marketing partners. However, these companies do not have any independent right to share this information.\nResponding to subpoenas, court orders, or legal process, or to establish or exercise our legal rights or defend ourselves against legal allegations.\nWe believe that it is necessary to share information in order to investigate, prevent or take action related to illegal activities, suspicion of fraud, situations involving potential threats to the physical safety of any person, violations of the terms of use of the orphan app or as otherwise required by law.\nDuring the transfer of information about you if the ownership of the orphan app is transferred or merged with another company. In this case, the Read Me app will inform you before your information is transferred and before you become subject to a different privacy policy.\nAn orphan app that displays targeted ads based on your personal information.\nAdvertisers (including ad service companies) assume that people who interact with, see or click on their ads that target specific people to meet certain criteria, for example young people between the ages of 18-24 from a specific geographic region.\nThe orphan app does not provide any personal information to the advertiser when you interact with or view a targeted ad.\nBy interacting with or viewing a targeted ad you agree with the possibility that the advertiser will assume that you meet the targeting criteria used to display the ad.\nAdvertisers on the orphan app include financial service providers (such as banks, insurance agents, stock brokers and mortgage lenders) and non-financial companies (such as stores, airlines, and software companies). An orphan app works with vendors, partners, advertisers, and other service providers in various sectors and classes of business. For more information regarding providers of the products or services you have requested, please read our detailed reference links.\nCookies and Information: The Read Me application may store and obtain cookies from your computer or phone.`
  String get privacyAndPoliceSubContent {
    return Intl.message(
      'How does the orphan app use your personal information The following information explains how the orphan app deals with the personal information it collects and receives, including information related to your previous use of any of the services and contents of the app.\nPersonal information is information about you that identifies you, such as your name, address, email address, phone number, Facebook account, or bank accounts. How to activate the privacy policy on the application This policy does not apply to the dealings or practices of sites or channels that are not owned or controlled by the application of orphans, as well as to employees or people who do not work in it or under its management.\nIn the event that any institutions, bodies or sites join the application, this privacy policy will apply to these institutions, agencies and sites.\nThe application collects personal information when you register to create a new account, when you register for some services, when you use some of the existing solutions, and when you visit a page or certain pages of our partners.\nOrphan may combine or collect information about individuals with information we obtain from business partners or other companies.\nWhen you register we ask for information such as your name, email address, date of birth, gender, zip code, occupation, place of residence. For some services, we may ask you for information about your assets, your address, how you access the Internet, your contact information, your hobbies, your preferences, and a personal profile about you. When you register for the application and register for our services, you are not anonymous to us.\nOrphans collect information about your dealings with us and with some of our business partners, including information about the use of financial products and services that we offer.\nThe application receives information from your device and from your Facebook account if you use it to log in, including your email address, application information and software, device features, and the page you request on the application. The app uses the information for the following general purposes: personalizing the advertisement and content that you see and hear, fulfilling your requests for content and services, improving our services, contacting you, and conducting research.\nChildren: Those under the age of ten are allowed to register on the application, provided that they are accompanied by one of the adults in their care.\nSubscriptions: The orphan platform provides many services that work to convey the voice of an orphan to those who deserve care.\nInformation sharing and disclosure: The orphan app does not rent, sell or share your personal information with other people or non-affiliated companies except to provide the contents or services that you requested and upon obtaining your permission or under the following circumstances: Providing information to trusted partners who work on behalf of or with The application of orphans under confidentiality agreements.\nThese companies may use your personal information to help the orphan app to communicate with you about offers from the orphan app and our marketing partners. However, these companies do not have any independent right to share this information.\nResponding to subpoenas, court orders, or legal process, or to establish or exercise our legal rights or defend ourselves against legal allegations.\nWe believe that it is necessary to share information in order to investigate, prevent or take action related to illegal activities, suspicion of fraud, situations involving potential threats to the physical safety of any person, violations of the terms of use of the orphan app or as otherwise required by law.\nDuring the transfer of information about you if the ownership of the orphan app is transferred or merged with another company. In this case, the Read Me app will inform you before your information is transferred and before you become subject to a different privacy policy.\nAn orphan app that displays targeted ads based on your personal information.\nAdvertisers (including ad service companies) assume that people who interact with, see or click on their ads that target specific people to meet certain criteria, for example young people between the ages of 18-24 from a specific geographic region.\nThe orphan app does not provide any personal information to the advertiser when you interact with or view a targeted ad.\nBy interacting with or viewing a targeted ad you agree with the possibility that the advertiser will assume that you meet the targeting criteria used to display the ad.\nAdvertisers on the orphan app include financial service providers (such as banks, insurance agents, stock brokers and mortgage lenders) and non-financial companies (such as stores, airlines, and software companies). An orphan app works with vendors, partners, advertisers, and other service providers in various sectors and classes of business. For more information regarding providers of the products or services you have requested, please read our detailed reference links.\nCookies and Information: The Read Me application may store and obtain cookies from your computer or phone.',
      name: 'privacyAndPoliceSubContent',
      desc: '',
      args: [],
    );
  }

  /// `Aytam User`
  String get aytamUser {
    return Intl.message(
      'Aytam User',
      name: 'aytamUser',
      desc: '',
      args: [],
    );
  }

  /// `Change Language`
  String get selectLang {
    return Intl.message(
      'Change Language',
      name: 'selectLang',
      desc: '',
      args: [],
    );
  }

  /// `Transactions`
  String get transactions {
    return Intl.message(
      'Transactions',
      name: 'transactions',
      desc: '',
      args: [],
    );
  }

  /// `guarantee`
  String get orphenPay {
    return Intl.message(
      'guarantee',
      name: 'orphenPay',
      desc: '',
      args: [],
    );
  }

  /// `Remittances`
  String get payment2 {
    return Intl.message(
      'Remittances',
      name: 'payment2',
      desc: '',
      args: [],
    );
  }

  /// `Un expected Error`
  String get unExpectedError {
    return Intl.message(
      'Un expected Error',
      name: 'unExpectedError',
      desc: '',
      args: [],
    );
  }

  /// `To be more careful about the orphans orphan we seek, through the application of orphans, to provide packages and medicines for orphans registered with us through the treatment bank.\n  The treatment bank is one of the orphans banks where we receive donations and based on the treatment lists for orphans, the packages are sent to our orphans directly through our agents in all countries where the inclusion of their orphans is available, and the verified ones are carried out by social workers tasked with verifying cases of orphans`
  String get treatementBank {
    return Intl.message(
      'To be more careful about the orphans orphan we seek, through the application of orphans, to provide packages and medicines for orphans registered with us through the treatment bank.\n  The treatment bank is one of the orphans banks where we receive donations and based on the treatment lists for orphans, the packages are sent to our orphans directly through our agents in all countries where the inclusion of their orphans is available, and the verified ones are carried out by social workers tasked with verifying cases of orphans',
      name: 'treatementBank',
      desc: '',
      args: [],
    );
  }

  /// `clothes bank`
  String get clothes_bank {
    return Intl.message(
      'clothes bank',
      name: 'clothes_bank',
      desc: '',
      args: [],
    );
  }

  /// ` Taking into account the conditions of orphans and to relieve them, we seek through the application of orphans to provide new pieces of clothing for orphans registered with us through the clothing bank. `
  String get clothes_explain {
    return Intl.message(
      ' Taking into account the conditions of orphans and to relieve them, we seek through the application of orphans to provide new pieces of clothing for orphans registered with us through the clothing bank. ',
      name: 'clothes_explain',
      desc: '',
      args: [],
    );
  }

  /// `The clothing bank is one of the orphans banks where we receive new clothes to rearrange them and send them to our orphans directly through our agents in all countries where the inclusion of their orphans is available,\n and the verification of their orphans is available through specialists tasked with verifying cases of orphans.`
  String get continue_clothes_explain {
    return Intl.message(
      'The clothing bank is one of the orphans banks where we receive new clothes to rearrange them and send them to our orphans directly through our agents in all countries where the inclusion of their orphans is available,\n and the verification of their orphans is available through specialists tasked with verifying cases of orphans.',
      name: 'continue_clothes_explain',
      desc: '',
      args: [],
    );
  }

  /// `donate`
  String get donate {
    return Intl.message(
      'donate',
      name: 'donate',
      desc: '',
      args: [],
    );
  }

  /// `food bank`
  String get food_bank {
    return Intl.message(
      'food bank',
      name: 'food_bank',
      desc: '',
      args: [],
    );
  }

  /// `Taking into account the conditions of orphans and reducing them, we seek, through the application of orphans, to provide commodities and food products for orphans periodically through an orphan food bank`
  String get food_explain {
    return Intl.message(
      'Taking into account the conditions of orphans and reducing them, we seek, through the application of orphans, to provide commodities and food products for orphans periodically through an orphan food bank',
      name: 'food_explain',
      desc: '',
      args: [],
    );
  }

  /// `The food bank is one of the orphans banks where you can donate nutritious meals and dried or fresh foods and deliver them directly to the orphan directly and quickly. \n The price of one meal is 10 dollars sent to one orphan who has completed it, and you can increase the number of meals through the Add Meal button.`
  String get continue_food_explain {
    return Intl.message(
      'The food bank is one of the orphans banks where you can donate nutritious meals and dried or fresh foods and deliver them directly to the orphan directly and quickly. \n The price of one meal is 10 dollars sent to one orphan who has completed it, and you can increase the number of meals through the Add Meal button.',
      name: 'continue_food_explain',
      desc: '',
      args: [],
    );
  }

  /// `treatment bank`
  String get treatment_bank {
    return Intl.message(
      'treatment bank',
      name: 'treatment_bank',
      desc: '',
      args: [],
    );
  }

  /// `Taking into account the conditions of orphans and to relieve them, we seek through the application of orphans to provide useful information for orphans on a regular basis for orphans registered with us through the treatment bank.`
  String get treatment_explain {
    return Intl.message(
      'Taking into account the conditions of orphans and to relieve them, we seek through the application of orphans to provide useful information for orphans on a regular basis for orphans registered with us through the treatment bank.',
      name: 'treatment_explain',
      desc: '',
      args: [],
    );
  }

  /// `The treatment bank is one of the orphans banks where we receive donations and based on the treatment lists for orphans, the packages are sent to our orphans directly through our agents in all countries where the inclusion of their orphans is available and the verified person is done by social workers in charge of verifying cases of orphans.`
  String get continue_treatment_explain {
    return Intl.message(
      'The treatment bank is one of the orphans banks where we receive donations and based on the treatment lists for orphans, the packages are sent to our orphans directly through our agents in all countries where the inclusion of their orphans is available and the verified person is done by social workers in charge of verifying cases of orphans.',
      name: 'continue_treatment_explain',
      desc: '',
      args: [],
    );
  }

  /// `term of use`
  String get term_of_use {
    return Intl.message(
      'term of use',
      name: 'term_of_use',
      desc: '',
      args: [],
    );
  }

  /// `Welcome to the terms of use of the orphan app\nIt is of great importance to you in that it organizes and summarizes your legal rights when you use the application.\nIt explains the rules that all users must follow.\nTherefore, please carefully read these terms, our Privacy Policy, and any other terms referenced in this document.`
  String get term_of_use_explain {
    return Intl.message(
      'Welcome to the terms of use of the orphan app\nIt is of great importance to you in that it organizes and summarizes your legal rights when you use the application.\nIt explains the rules that all users must follow.\nTherefore, please carefully read these terms, our Privacy Policy, and any other terms referenced in this document.',
      name: 'term_of_use_explain',
      desc: '',
      args: [],
    );
  }

  /// `Conditions for using the orphan application: At the outset, we thank you for choosing an orphan application to ensure and care for orphans. The orphans application provides specialized services that include social and interactive features to provide a helping hand and assistance to our orphans and other social content, in addition to other services that can be developed from time to time that are broadcast Via the Internet to your mobile phone, or to any other compatible device of yours through which you can access the Internet. By creating a new account, obtaining a service or a financial or social aid plan with orphans, or purchasing and / or by downloading or entering and accessing or accessing an orphan application, or using an orphan application, or our website, or using a service or An additional product provided by orphans, or in another way, by accessing or using the content, or any of the service features or functions provided by orphans, as the case may be, you have entered into a binding agreement with orphans, and you agree and accept to be bound by these terms of use. . You also acknowledge and agree to Readli's use of your personal data in accordance with the Aitam Privacy Policy.\nThese terms of use describe and explain the conditions under which the service is available to you, and supersede and replace the previously agreed terms and conditions.\nAgreeing to these terms is necessary and essential to be able to access or use the service.\nBy accepting these terms, you agree and accept that orphans will terminate this agreement, or suspend your access or access to the service at any time without prior notice in the event that you are unable to comply with any of the terms stipulated in this agreement.\nIf you do not agree to the terms (as defined below), you may not use the service, access or access it, or use any content provided by orphans, and in addition, supplementary terms of use may apply to some parts of the service; For example: conditions related to specific or specific competition rules, additional terms and conditions for additional services or other activities, or for some additional content, or in relation to separate subscription plans, or products or programs that can be accessed or provided through the service. In the event that supplementary terms and conditions are required to be applied, they will be disclosed to you in relation to the related activities or products.\nAny additional and complementary terms and conditions provided by orphans are complementary and in addition to these terms of use, and have priority in effect in the event that they conflict with the terms of use. After, in the name of the conditions.\n1- Age limit and account eligibility:\n* To be eligible to accept the conclusion of the conditions and the creation of an account, or the conclusion of a subscription plan with orphans, the following conditions must be met: You have reached at least eighteen (18) years of age, or you have legal authorization and authority to conclude this agreement, or that the registration and use be in a state Not to exceed the age of eighteen years by a parent or adult to benefit from the service legally, in accordance with the laws in force in the country in which the service is provided to you.\nYou reside in the country in which the service is available and provided to you.\nYou agree to abide by the terms.\n* You must also provide and provide orphans with correct information about yourself, and you must also provide contact information, supporting documents, correct status and a valid payment method when registering your account with orphans. And you must agree to the terms and provide the information referred to, in order to be able to access, access and use the service.\nIn the event that you do not agree to the terms, you may not use the service or its functions or use any content provided in the service.\n2- Service:\n* The orphan application provides direct communication service with people participating in the service or donation services with clothes, food and treatment to help orphans so that you, as a subscriber, can review, register, or sponsor orphans in a physical or social manner after fulfilling the conditions and approval by both parties, or otherwise. Service contents on your mobile phone, or through any other devices that have a feature to connect to the Internet through an orphans application, or any other pre-installed programs from orphans. In addition, the service includes recommendations, messages and customized features that were specially developed and amended for you in order to improve and develop your usage experience.\nOrphans may also provide, from time to time, access or access to different types of social networking forums that may or may not be available to the public, in order to develop and enrich your usage experience. Unless otherwise agreed upon in any supplementary terms, or under a subscription plan provided to you by orphans, and which you have agreed to, the service may be used, as described above, only for your personal, non-commercial use as stipulated in the terms.\n* In order for you to use the service, you must use a device that connects to the Internet that complies with the technical requirements of orphans or one of the partner companies' platforms for orphans on which our application has been installed, and you must also provide us or our business partners with your choice of the payment method accepted by orphans, as it may be. Valid from time to time. Orphan reserves the right to amend the technical requirements for using the service and change, add or delete business partners and payment methods and methods from time to time. Orphans will do their utmost to maintain your secure and privileged use.\n`
  String get term_of_use_explain2 {
    return Intl.message(
      'Conditions for using the orphan application: At the outset, we thank you for choosing an orphan application to ensure and care for orphans. The orphans application provides specialized services that include social and interactive features to provide a helping hand and assistance to our orphans and other social content, in addition to other services that can be developed from time to time that are broadcast Via the Internet to your mobile phone, or to any other compatible device of yours through which you can access the Internet. By creating a new account, obtaining a service or a financial or social aid plan with orphans, or purchasing and / or by downloading or entering and accessing or accessing an orphan application, or using an orphan application, or our website, or using a service or An additional product provided by orphans, or in another way, by accessing or using the content, or any of the service features or functions provided by orphans, as the case may be, you have entered into a binding agreement with orphans, and you agree and accept to be bound by these terms of use. . You also acknowledge and agree to Readli\'s use of your personal data in accordance with the Aitam Privacy Policy.\nThese terms of use describe and explain the conditions under which the service is available to you, and supersede and replace the previously agreed terms and conditions.\nAgreeing to these terms is necessary and essential to be able to access or use the service.\nBy accepting these terms, you agree and accept that orphans will terminate this agreement, or suspend your access or access to the service at any time without prior notice in the event that you are unable to comply with any of the terms stipulated in this agreement.\nIf you do not agree to the terms (as defined below), you may not use the service, access or access it, or use any content provided by orphans, and in addition, supplementary terms of use may apply to some parts of the service; For example: conditions related to specific or specific competition rules, additional terms and conditions for additional services or other activities, or for some additional content, or in relation to separate subscription plans, or products or programs that can be accessed or provided through the service. In the event that supplementary terms and conditions are required to be applied, they will be disclosed to you in relation to the related activities or products.\nAny additional and complementary terms and conditions provided by orphans are complementary and in addition to these terms of use, and have priority in effect in the event that they conflict with the terms of use. After, in the name of the conditions.\n1- Age limit and account eligibility:\n* To be eligible to accept the conclusion of the conditions and the creation of an account, or the conclusion of a subscription plan with orphans, the following conditions must be met: You have reached at least eighteen (18) years of age, or you have legal authorization and authority to conclude this agreement, or that the registration and use be in a state Not to exceed the age of eighteen years by a parent or adult to benefit from the service legally, in accordance with the laws in force in the country in which the service is provided to you.\nYou reside in the country in which the service is available and provided to you.\nYou agree to abide by the terms.\n* You must also provide and provide orphans with correct information about yourself, and you must also provide contact information, supporting documents, correct status and a valid payment method when registering your account with orphans. And you must agree to the terms and provide the information referred to, in order to be able to access, access and use the service.\nIn the event that you do not agree to the terms, you may not use the service or its functions or use any content provided in the service.\n2- Service:\n* The orphan application provides direct communication service with people participating in the service or donation services with clothes, food and treatment to help orphans so that you, as a subscriber, can review, register, or sponsor orphans in a physical or social manner after fulfilling the conditions and approval by both parties, or otherwise. Service contents on your mobile phone, or through any other devices that have a feature to connect to the Internet through an orphans application, or any other pre-installed programs from orphans. In addition, the service includes recommendations, messages and customized features that were specially developed and amended for you in order to improve and develop your usage experience.\nOrphans may also provide, from time to time, access or access to different types of social networking forums that may or may not be available to the public, in order to develop and enrich your usage experience. Unless otherwise agreed upon in any supplementary terms, or under a subscription plan provided to you by orphans, and which you have agreed to, the service may be used, as described above, only for your personal, non-commercial use as stipulated in the terms.\n* In order for you to use the service, you must use a device that connects to the Internet that complies with the technical requirements of orphans or one of the partner companies\' platforms for orphans on which our application has been installed, and you must also provide us or our business partners with your choice of the payment method accepted by orphans, as it may be. Valid from time to time. Orphan reserves the right to amend the technical requirements for using the service and change, add or delete business partners and payment methods and methods from time to time. Orphans will do their utmost to maintain your secure and privileged use.\n',
      name: 'term_of_use_explain2',
      desc: '',
      args: [],
    );
  }

  /// `privacy policy`
  String get privacy_policy {
    return Intl.message(
      'privacy policy',
      name: 'privacy_policy',
      desc: '',
      args: [],
    );
  }

  /// `The orphans app takes your privacy very seriously\nSo please read the following to learn more about the privacy policy followed by the app.`
  String get privacy_policy_explain {
    return Intl.message(
      'The orphans app takes your privacy very seriously\nSo please read the following to learn more about the privacy policy followed by the app.',
      name: 'privacy_policy_explain',
      desc: '',
      args: [],
    );
  }

  /// `How does the orphan app use your personal information The following information explains how the orphan app deals with the personal information it collects and receives, including information related to your previous use of any of the services and contents of the app.\nPersonal information is information about you that identifies you, such as your name, address, email address, phone number, Facebook account, or bank accounts. How to activate the privacy policy on the application This policy does not apply to the dealings or practices of sites or channels that are not owned or controlled by the application of orphans, as well as to employees or people who do not work in it or under its management.\nIn the event that any institutions, bodies or sites join the application, this privacy policy will apply to these institutions, agencies and sites.\nThe application collects personal information when you register to create a new account, when you register for some services, when you use some of the existing solutions, and when you visit a page or certain pages of our partners.\nOrphan may combine or collect information about individuals with information we obtain from business partners or other companies.\nWhen you register we ask for information such as your name, email address, date of birth, gender, zip code, occupation, place of residence. For some services, we may ask you for information about your assets, your address, how you access the Internet, your contact information, your hobbies, your preferences, and a personal profile about you. When you register for the application and register for our services, you are not anonymous to us.\nThe application receives information from your device and from your Facebook account if you use it to log in, including your email address, application information and software, device features, and the page you request on the application. The app uses the information for the following general purposes: personalizing the advertisement and content that you see and hear, fulfilling your requests for content and services, improving our services, contacting you, and conducting research.\nChildren: Those under the age of ten are allowed to register on the application, provided that they are accompanied by one of the adults in their care.\nSubscriptions: The orphan platform provides many services that work to convey the voice of an orphan to those who deserve care.\nInformation sharing and disclosure: The orphan app does not rent, sell or share your personal information with other people or non-affiliated companies except to provide the contents or services that you requested and upon obtaining your permission or under the following circumstances: Providing information to trusted partners who work on behalf of or with The application of orphans under confidentiality agreements.\nThese companies may use your personal information to help the orphan app to communicate with you about offers from the orphan app and our marketing partners. However, these companies do not have any independent right to share this information.\nResponding to subpoenas, court orders, or legal proceedings, or to establish or exercise our legal rights or defend ourselves against legal allegations.\nWe believe that it is necessary to share information in order to investigate, prevent or take action related to illegal activities, suspicion of fraud, situations involving potential threats to the physical integrity of any person, violations of the terms of use of the orphan app or as otherwise required by law.\nDuring the transfer of information about you if the ownership of the orphan app is transferred or merged with another company. In this case, the Read Me app will inform you before your information is transferred and before you become subject to a different privacy policy.\nAn orphan app that displays targeted ads based on your personal information.\nAdvertisers (including ad service companies) assume that people who interact with, see or click on their ads that target specific people to meet certain criteria, for example young people between the ages of 18-24 from a specific geographic region.\nThe orphan app does not provide any personal information to the advertiser when you interact with or view a targeted ad.\nBy interacting with or viewing a targeted ad you agree with the possibility that the advertiser will assume that you meet the targeting criteria used to display the ad.\nAdvertisers on the orphan app include financial service providers (such as banks, insurance agents, stock brokers and mortgage lenders) and non-financial companies (such as stores, airlines, and software companies). An orphan app works with vendors, partners, advertisers, and other service providers in various sectors and classes of business. For more information regarding providers of the products or services you have requested, please read our detailed reference links.\nCookies and Information: The Read Me application may store and obtain cookies from your computer or phone.\n`
  String get privacy_policy_explain2 {
    return Intl.message(
      'How does the orphan app use your personal information The following information explains how the orphan app deals with the personal information it collects and receives, including information related to your previous use of any of the services and contents of the app.\nPersonal information is information about you that identifies you, such as your name, address, email address, phone number, Facebook account, or bank accounts. How to activate the privacy policy on the application This policy does not apply to the dealings or practices of sites or channels that are not owned or controlled by the application of orphans, as well as to employees or people who do not work in it or under its management.\nIn the event that any institutions, bodies or sites join the application, this privacy policy will apply to these institutions, agencies and sites.\nThe application collects personal information when you register to create a new account, when you register for some services, when you use some of the existing solutions, and when you visit a page or certain pages of our partners.\nOrphan may combine or collect information about individuals with information we obtain from business partners or other companies.\nWhen you register we ask for information such as your name, email address, date of birth, gender, zip code, occupation, place of residence. For some services, we may ask you for information about your assets, your address, how you access the Internet, your contact information, your hobbies, your preferences, and a personal profile about you. When you register for the application and register for our services, you are not anonymous to us.\nThe application receives information from your device and from your Facebook account if you use it to log in, including your email address, application information and software, device features, and the page you request on the application. The app uses the information for the following general purposes: personalizing the advertisement and content that you see and hear, fulfilling your requests for content and services, improving our services, contacting you, and conducting research.\nChildren: Those under the age of ten are allowed to register on the application, provided that they are accompanied by one of the adults in their care.\nSubscriptions: The orphan platform provides many services that work to convey the voice of an orphan to those who deserve care.\nInformation sharing and disclosure: The orphan app does not rent, sell or share your personal information with other people or non-affiliated companies except to provide the contents or services that you requested and upon obtaining your permission or under the following circumstances: Providing information to trusted partners who work on behalf of or with The application of orphans under confidentiality agreements.\nThese companies may use your personal information to help the orphan app to communicate with you about offers from the orphan app and our marketing partners. However, these companies do not have any independent right to share this information.\nResponding to subpoenas, court orders, or legal proceedings, or to establish or exercise our legal rights or defend ourselves against legal allegations.\nWe believe that it is necessary to share information in order to investigate, prevent or take action related to illegal activities, suspicion of fraud, situations involving potential threats to the physical integrity of any person, violations of the terms of use of the orphan app or as otherwise required by law.\nDuring the transfer of information about you if the ownership of the orphan app is transferred or merged with another company. In this case, the Read Me app will inform you before your information is transferred and before you become subject to a different privacy policy.\nAn orphan app that displays targeted ads based on your personal information.\nAdvertisers (including ad service companies) assume that people who interact with, see or click on their ads that target specific people to meet certain criteria, for example young people between the ages of 18-24 from a specific geographic region.\nThe orphan app does not provide any personal information to the advertiser when you interact with or view a targeted ad.\nBy interacting with or viewing a targeted ad you agree with the possibility that the advertiser will assume that you meet the targeting criteria used to display the ad.\nAdvertisers on the orphan app include financial service providers (such as banks, insurance agents, stock brokers and mortgage lenders) and non-financial companies (such as stores, airlines, and software companies). An orphan app works with vendors, partners, advertisers, and other service providers in various sectors and classes of business. For more information regarding providers of the products or services you have requested, please read our detailed reference links.\nCookies and Information: The Read Me application may store and obtain cookies from your computer or phone.\n',
      name: 'privacy_policy_explain2',
      desc: '',
      args: [],
    );
  }

  /// `orphan team`
  String get orphan_team {
    return Intl.message(
      'orphan team',
      name: 'orphan_team',
      desc: '',
      args: [],
    );
  }

  /// `The orphans platform connects orphans with the sponsor to ensure a guarantee without an intermediary. You can also follow us on social media platforms (Facebook, Twitter, Instagram, WhatsApp) to follow us.`
  String get orphan_team_explain {
    return Intl.message(
      'The orphans platform connects orphans with the sponsor to ensure a guarantee without an intermediary. You can also follow us on social media platforms (Facebook, Twitter, Instagram, WhatsApp) to follow us.',
      name: 'orphan_team_explain',
      desc: '',
      args: [],
    );
  }

  /// `adopte`
  String get adopte {
    return Intl.message(
      'adopte',
      name: 'adopte',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'ar'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}