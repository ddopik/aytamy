import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
// import 'package:google_fonts/google_fonts.dart';

/// Google fonts constant setting: https://fonts.google.com/

class Fonts {
  static const double fontSize_head = 21;
  static const double fontSize_large = 18;
  static const double fontSize_meduim = 16;
  static const double fontSize_small = 14;



  static const double itemFontSize = 14;
  static const String fontfamily = 'GESSTwo';
}

TextTheme textTheme(theme, String language) {
  switch (language) {
    case 'vi':
      return GoogleFonts.montserratTextTheme(theme);
    case 'ar':
      return GoogleFonts.ralewayTextTheme(theme);
    default:
      return GoogleFonts.ralewayTextTheme(theme);
  }
}

TextTheme HeadlineTheme(theme, [language = 'en']) {
  switch (language) {
    case 'vi':
      return GoogleFonts.montserratTextTheme(theme);
    case 'ar':
      return GoogleFonts.ralewayTextTheme(theme);
    default:
      return GoogleFonts.ralewayTextTheme(theme);
  }
}
